//
//  AppDelegate.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 02/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import GoogleMaps
import Firebase
import UserNotifications
import Stripe

@UIApplicationMain
 class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {

    var window:UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
         IQKeyboardManager.shared.enable = true
        L102Localizer.DoTheMagic()
               FirebaseApp.configure()
               GMSServices.provideAPIKey("AIzaSyAf5Us7xQgnm0hggbsj86lrwzKSCykgErE")
        
        UNUserNotificationCenter.current().delegate = self
       GoogleManager.sharedManager.googleSetup(googleKey:"646149973707-q6a4gv20vfa0hr40sk8cnls09aa2564l.apps.googleusercontent.com")
       
        STPAPIClient.shared().publishableKey = "pk_test_cRsz5lM2lXBvDrTqONfbYhkX"
        
        registerForRemoteNotification()
        Router.shared.setInitialVC()
        return true
    }
    
    func reloadApp () {
          Router.shared.setInitialVC()
      }
    
    //MARK: - Method to request send Notification permission
    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
            DispatchQueue.main.async {
                           if error == nil{
                               
                               UIApplication.shared.registerForRemoteNotifications()
                           }
                       }
                   }
               }
               else {
                   DispatchQueue.main.async {
                       UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
                       UIApplication.shared.registerForRemoteNotifications()
                   }
               }
           }
           
           //MARK: - Notification Delegate Method
           func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
               let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
               print("push token")
               print(token)
             
               print(deviceToken.description)
               if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                   print(uuid)
               }
               UserDefaults.standard.setDeviceToken(token: token)
               UserDefaults.standard.synchronize()
           }
       
       func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
           print(error.localizedDescription)
       }
           
           func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

               print("Handle push from foreground\(notification.request.content.userInfo)")
               let dict = notification.request.content.userInfo["aps"] as! NSDictionary
               print(dict)
              //let d : [String : Any] = dict["alert"] as! [String : Any]
               let badge = dict["badge"] as? Int
               UIApplication.shared.applicationIconBadgeNumber = badge ?? 0
               
               completionHandler([.sound, .alert, .badge])
               
           }
           
           func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
               print("Recived: \(userInfo)")
               var isApplicationActive:Bool {
                   get {return UIApplication.shared.applicationState == .active}
               }
               if (isApplicationActive) {
                   print("App is Active")
                  // Router.shared.moveAccordingly(pushData:userInfo as! [String:Any])
                   
               }
               completionHandler(.newData)
           }
           
           func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
              
               print("Handle push from background or closed\(response.notification.request.content.userInfo)")
               
               let dict = response.notification.request.content.userInfo as NSDictionary
               
               print(dict["aps"] as! NSDictionary)
               //Router.shared.moveAccordingly(pushData:response.notification.request.content.userInfo as! [String:Any])
               print("My Background")

           }


   
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "VGFruits")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
               
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

