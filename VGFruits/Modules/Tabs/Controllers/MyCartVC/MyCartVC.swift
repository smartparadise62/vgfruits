//
//  MyCartVC.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 12/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit
import Stripe


class MyCartVC: UIViewController {
    
    //MARK: - IBOutlets
    
    
    @IBOutlet weak var lblDiscount: UILabel!
    
    @IBOutlet weak var lblDiscountedPrice: UILabel!
    
    @IBOutlet weak var btnCopuonlbl: UIButton!
    @IBOutlet weak var viewScroll: UIScrollView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var tblCart: UITableView!{
        didSet{
            tblCart.registerXIB(CellIdentifiers.ItemsCell.rawValue)
        }
    }
    
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var lblCheckoutPrice: UILabel!
    @IBOutlet weak var lblDeliveryPrice: UILabel!
    @IBOutlet weak var lblGrandTotal: UILabel!
    @IBOutlet weak var emptyView: UIView!
    
    var tblDS:TableViewDataSource?
    var shopId = -1
    var addressID = 0
    var productIds = ""
    var quantities = ""
    var totalPayableAmount:Double = 0
  
    var intentSecret = ""
    var discount = 0
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        //getDefaultAddress()
        setupView()
        NotificationCenter.default.addObserver(self, selector: #selector(discountApplied), name: Notification.Name("DiscountAvailed"), object: nil)
        lblDiscount.isHidden = true
        lblDiscountedPrice.isHidden = true
        lblAddress.text = /UserPreference.shared.data?.address
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        getDefaultAddress()
        loadCart()
    }
    
    @objc func discountApplied(_ sender:Notification){
        if let data = sender.userInfo as? [String:Any] {
            let discount = data["discount"] as? Int
            let name = data["name"] as? String
            if self.totalPayableAmount - Double(/discount) > 0{
                self.discount = /discount
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                               self.totalPayableAmount -= Double(/discount)
                               self.lblGrandTotal.text = "€ \(self.totalPayableAmount)"
                               self.lblCheckoutPrice.text = "€ \(self.totalPayableAmount )"
                               self.btnCopuonlbl.setTitle(/name, for: .normal)
                               self.lblDiscount.isHidden = false
                    self.lblDiscountedPrice.isHidden = false
                    self.lblDiscountedPrice.text = "€ \(/discount)"
                               
                           }
            }
           
        }
    }
    
    
    func getDefaultAddress(){
        UserEP.getDefaultAddress.request(loader: false, success: { (res) in
            guard let data = res as? DefaultAddress else { return }
            self.addressID = /data.data?.id
            self.lblAddress.text = /data.data?.address
        }) { (error) in
            print(/error)
        }
    }
    
    func loadCart(){
        CartManager.shared.getCart { (productList, shopId,stripeId)  in
            if productList.count > 0 {
                self.shopId = shopId
                self.configureTable(items:productList)
                self.viewScroll.isHidden = false
                self.viewBottom.isHidden = false
                self.emptyView.isHidden = true
                var price = 0.0
                self.productIds = ""
                self.quantities = ""
                for item in productList{
                    let amt = /item.productPrice
                    let quantity = Double(/item.productQuantity)
                    price += (amt * quantity)
                    self.productIds += "\(/item.productId)"
                    self.quantities += "\(/item.productQuantity)"
                    if item != productList.last{
                        self.productIds += ","
                        self.quantities += ","
                    }
                }
                self.lblTotalPrice.text = "€ \(price)"
                self.lblDeliveryPrice.text = "€ 0"
                self.lblGrandTotal.text = "€ \(price)"
                self.lblCheckoutPrice.text = "€ \(price )"
                self.totalPayableAmount = price
                if self.discount > 0 {
                    self.lblDiscount.isHidden = false
                    self.lblDiscountedPrice.text = "€ \(self.discount)"
                    self.totalPayableAmount -= Double(self.discount)
                    self.lblGrandTotal.text = "€ \(self.totalPayableAmount)"
                    self.lblCheckoutPrice.text = "€ \(self.totalPayableAmount )"
                }
            }else{
                self.emptyView.isHidden = false
                self.viewScroll.isHidden = true
                self.viewBottom.isHidden = true
                self.discount = 0
                self.btnCopuonlbl.setTitle("NA", for: .normal)
            }
            
        }
    }
    
    func setupView(){
        Utility.dropShadow(mView: viewBottom, radius: 4, color: .lightGray, size: CGSize(width: 0, height: -1))
    }
    
    func configureTable(items:[CartProduct]){
        tblHeight.constant = CGFloat(150 * items.count)
        tblDS = TableViewDataSource(items: items, tableView: tblCart, cellIdentifier: CellIdentifiers.ItemsCell.rawValue, configureCellBlock: { (cell, item, index) in
            
            if let mCell = cell as? ItemCell{
                mCell.shopId = self.shopId
                mCell.item = item
                mCell.isFromCart = true
                mCell.btnDelete.isHidden = true
                mCell.viewStepper.isHidden = false
                mCell.btnAddToCart.isHidden = true
                mCell.viewBorder.isHidden = false
                
                mCell.item = item
                CartManager.shared.getCart { (cartList, id,stripeId) in
                    mCell.cartItems  = cartList
                    
                }
                
                mCell.changed = {
                    self.loadCart()
                }
            }
            
        }, aRowSelectedListener: { (index, item) in
            
        }, willDisplayCell: { (index, cell, item) in
            
        }, viewforHeaderInSection: nil, scrollToTop: nil)
        
        tblCart.dataSource = tblDS
        tblCart.delegate = tblDS
        tblCart.reloadData()
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnActionMenu(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("OpenSideBar"), object: nil)
    }
    
    @IBAction func btnActionSubmit(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("JoinRoom"), object: nil, userInfo: ["userId":self.shopId])
        
        UserEP.getStripeToken(amount: Int(totalPayableAmount*100), order_id: Int(Date().timeIntervalSince1970), shop_id: self.shopId, application_fee_amount: 10 ,currency:"USD").request(loader:true,success: {(res) in
            guard let data = res as? StripeSecret else { return }
            guard let vc = R.storyboard.sideBar.addPaymentMethodVC() else {return}
            vc.secret = /data.data?.secret
            vc.stripeId = /data.data?.stripe_id
            vc.addressID = self.addressID
            vc.productIds = self.productIds
            vc.shopId = self.shopId
            vc.quantities = self.quantities
            Router.shared.pushVC(vc: vc)
            
        }){(error) in
            Toast.shared.showAlert(type: .apiFailure, message: /error)
        }
        
    }
    
    @IBAction func btnActionChangePaymentMethod(_ sender: Any) {
        guard let vc = R.storyboard.sideBar.paymentMethodsVC() else { return }
        Router.shared.pushVC(vc: vc)
    }
    
    @IBAction func btnActionAddPromoCode(_ sender: Any) {
        guard let vc = R.storyboard.details.couponsVC() else { return }
        Router.shared.pushVC(vc: vc)
    }
    
    
    @IBAction func btnActionChangeAddress(_ sender: Any) {
//        let vc = SelectLocationVC(nibName: String(describing: SelectLocationVC.self), bundle: nil)
//        vc.delegate = self
//        self.present(vc, animated: true, completion: nil)
        guard let vc = R.storyboard.details.selectAddressVC() else { return}
        Router.shared.pushVC(vc: vc)
        
    }
}

extension MyCartVC:SelectLocationDelegate{
   
    
    func locationSelected(address: String, city: String, state: String, country: String, zipcode: String, lat: Double, long: Double) {
        self.lblAddress.text = address
        //        self.lat = lat
        //        self.long = long
        
    }
    
}
