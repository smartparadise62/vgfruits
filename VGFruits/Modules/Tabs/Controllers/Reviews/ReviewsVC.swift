//
//  ReviewsVC.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 05/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class ReviewsVC: UIViewController {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var tblReview: UITableView!{
        didSet{
            tblReview.registerXIB(CellIdentifiers.ReviewCell.rawValue)
        }
    }
    
    
    var dsReview:TableViewDataSource?
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        configureReviewTable()
    }
    
    func configureReviewTable(){
         dsReview = TableViewDataSource(items: [1,1,1,1,1,1,1], tableView: tblReview, cellIdentifier: CellIdentifiers.ReviewCell.rawValue, configureCellBlock: { (cell, item, index) in
             guard let mCell = cell as? OrderCell else { return }
             mCell.item = item
         }, aRowSelectedListener: { (index, item) in
             
         }, willDisplayCell: nil, viewforHeaderInSection: nil, scrollToTop: nil)
         tblReview.delegate = dsReview
         tblReview.dataSource = dsReview
         tblReview.reloadData()
     }
    
    //MARK: - IBAction Methods
    @IBAction func btnActionMenu(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("OpenSideBar"), object: nil)
    }
    

}
