//
//  HomeVC.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 05/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit
import PullToRefreshKit

class HomeVC: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var viewFilter: UIView!
    
     @IBOutlet weak var lblMaxDistance: UILabel!
    
    @IBOutlet weak var btnHideFiltr: UIButton!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var tblSearch: UITableView!

    var tblDS:HomeTableDataSource?
    var searchDS:SearchDataSource?
    var categories = [Categories]()
    var banners = [Banners]()
    var shops = [ShopData]()
    var searchData = [SearchData]()

    
    var lat:Double = 0.0
    var long:Double = 0.0
    var shouldLoad = false
    var isSearchActive = false
    var offset = 1
    var selectedCategory:Int? = nil
    var radius = 40
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.dropShadow(mView: viewSearch, radius: 4, color: .lightGray, size: CGSize(width: 0.5, height: 0.5))
        shouldLoad = true
        btnHideFiltr.isHidden = true
        viewFilter.isHidden = true
        tblSearch.isHidden = true
        tfSearch.delegate = self
        viewFilter.layer.cornerRadius = 15
        viewFilter.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        Utility.dropShadow(mView: viewFilter, radius: 3, color: .lightGray, size: CGSize(width: -1, height: -1))
        footerRefresh()
        NotificationCenter.default.addObserver(self, selector: #selector(categoryTapped), name: Notification.Name("CategorySelected"), object: nil)
        viewFilter.transform = viewFilter.transform.translatedBy(x: 0, y: 500)
        
         NotificationCenter.default.addObserver(self, selector: #selector(anotherShopAdding), name: Notification.Name("AnotherRequest"), object: nil)
    }
    
    @objc func anotherShopAdding(_ notification:Notification){
           if let data = notification.userInfo as? [String:Any] {
               let addingShopId = data["addingShopId"] as? Int
               let shopId = data["shopId"] as? Int
               let product = data["product"] as? CartProduct
               let controller = Router.shared.topController()!
               
               controller.showAlert(message: "deleteAlreadyAdded".localize, title: "VGFruits", actionTitle: "Ok".localize, ok: {
                   NotificationCenter.default.post(name:Notification.Name(rawValue: "DeleteCart"), object: nil, userInfo: ["shopId":/shopId])
                   
                   DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                       
                       let product = CartProduct(productId: /product?.productId, productName: /product?.productName, productCategory: /product?.productCategory, productDescription: /product?.productDescription, productPrice: Double(/product?.productPrice), productQuantity: /product?.productQuantity, productImage: /product?.productImage, createdAt: Date())
                       CartManager.shared.addProductToCart(product: product, addingShopId: /addingShopId, stripeId: "")
                       DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                           self.tblSearch.reloadData()
                       }
                       
                   }
               }) {
                   
               }
           }
       }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let data = UserPreference.shared.data
        lblAddress.text = /data?.address
        self.lat = /data?.latitude
        self.long = /data?.longitude
        imgProfile.setImageKF(/data?.profile, placeholder: UIImage(named:"personn"))
        if !isSearchActive{
            offset = 1
            getHome()
            getShop(shouldLoad: false)
        }
        
        getAddress()
       
    }
    
    @objc func categoryTapped(_ sender:Notification){
        if let data = sender.userInfo as? [String:Any]{
            let id = data["id"] as? Int
            selectedCategory = id
//            if let catId = selectedCategory{
//                searchItemByCatId(catId:catId)
//            }else{
                getShop(shouldLoad: true)
          //  }
        }
    }
    
    func footerRefresh() {
        
        let footer = self.tblMain.setUpFooterRefresh { [weak self] in
            if /self?.isSearchActive{
                
            }else{
                self?.offset += 1
                self?.getShop(shouldLoad:false)
            }
                
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                self?.tblMain.endFooterRefreshing()
            }
        }.SetUp { (footer) in
            footer.setText("", mode: .pullToRefresh)
            footer.setText("Loading...".localize, mode: .refreshing)
            footer.setText("".localize, mode: .noMoreData)
        }
        footer.refreshMode = .scroll
        
    }
    
    func getHome(){
        UserEP.getHome(latitude: "\(self.lat)", longitude: "\(self.long)").request(loader: shouldLoad, success: { (res) in
            guard let data = res as? HomeModel else { return }
            Utility.shared.saveCategories(categories: data.data?.categories ?? [])
            let category = Categories()
            category.id = nil
            category.name = "All"
            var cArray = data.data?.categories ?? []
            cArray.insert(category, at: 0)
            self.categories = cArray
            self.banners = data.data?.banners ?? []
            //self.shops = data.data?.topRatedShops ?? []
            print(self.shops.count)
            self.shouldLoad = false
            self.configureTable()
        }) { (error) in
            Toast.shared.showAlert(type: .apiFailure, message: /error)
        }
    }
    
    
    func getShop(shouldLoad:Bool){
        UserEP.getShops(offset: offset, limit: 20, lat: "\(lat)", long: "\(long)", categoryId: selectedCategory, search: nil, distance: radius).request(loader: shouldLoad, success: { (res) in
            guard let data = res as? ShopModel else {
                if self.offset != 1{
                    self.offset -= 1
                    
                }
                return
            }
            if /data.data?.result?.count == 0{
                if self.offset != 1{
                                   self.offset -= 1
                                   
                               }
                return
            }
            self.shops = self.offset == 1 ?  data.data?.result ?? [] : self.shops + (data.data?.result ?? [])
            self.configureTable()
            
        }) { (error) in
            Toast.shared.showAlert(type: .apiFailure, message: /error)
        }
    }

    func configureTable(){
        tblDS = HomeTableDataSource(items: shops, tableView: tblMain, cellIdentifier: CellIdentifiers.ShopCell.rawValue, configureCellBlock: { (cell, item, index) in
            if let mCell = cell as? ShopCell{
                mCell.item = item
            }
            if let mCell = cell as? BannerCollectionCell{
                mCell.item = self.banners
            }
            if let mCell = cell as? CategoryCVCell{
                mCell.item = self.categories
                mCell.didSelectCategory = { (id) in
                    self.selectedCategory = id
                    self.offset = 1
                    self.getShop(shouldLoad: true)
                }
            }

        }, aRowSelectedListener: { (index, item) in
            
            if index.row > 1{
                guard let vc = R.storyboard.details.shopDetailsVC() else { return }
                vc.data = item as? ShopData
                Router.shared.pushVC(vc: vc)
            }
            
        }, willDisplayCell: { (index, cell, item) in
            
        }, viewforHeaderInSection: nil, scrollToTop: nil)
        
        tblMain.dataSource = tblDS
        tblMain.delegate = tblDS
        tblMain.reloadData()
    }
    
    func configureSearchTable(){
          searchDS = SearchDataSource(items: searchData, tableView: tblSearch, cellIdentifier: CellIdentifiers.SearchItemCell.rawValue, configureCellBlock: { (cell, item, index) in
//              if let mCell = cell as? ShopCell{
//                  mCell.item = item
//              }
//              if let mCell = cell as? SearchItemCell{
//                  mCell.item = item
//              }
              

          }, aRowSelectedListener: { (index, item) in
              guard let vc = R.storyboard.details.shopDetailsVC() else { return }
                    vc.searchData = item as? SearchData
              Router.shared.pushVC(vc: vc)
              
          }, willDisplayCell: { (index, cell, item) in
            
            if let mCell = cell as? SearchItemCell{
                guard let data = item as? SearchData else { return }
                if /data.type == 2{
                    mCell.shopId = /data.user_id
                   // mCell.stripeId = /data.stripeId
                    mCell.item = item
                    CartManager.shared.getCart { (cartList, id,stripeId)  in
                        mCell.cartItems  = cartList
                 }
                }
               
               
            }
            
            if let mCell = cell as? ShopCell{
                mCell.item = item
            }
              
          }, viewforHeaderInSection: nil, scrollToTop: nil)
          
          tblSearch.dataSource = searchDS
          tblSearch.delegate = searchDS
          tblSearch.reloadData()
      }

    
    //MARK: - IBAction Methods
    
    @IBAction func btnActionApply(_ sender: Any) {
        self.offset = 1
        self.getShop(shouldLoad: true)
        btnHideFiltr.isHidden = true
               UIView.animate(withDuration: 0.3, animations: {
                   self.viewFilter.transform = self.viewFilter.transform.translatedBy(x: 0, y: 500)
               }) { (finished) in
                   self.viewFilter.isHidden = true
               }
    }
    
    
    @IBAction func sliderChanged(_ sender: Any) {
        let value = Int((sender as! UISlider).value)
        radius = value
        lblMaxDistance.text = "\(value) " + "km".localize
       
        
    }
    
    @IBAction func btnActionFilter(_ sender: Any) {
        viewFilter.isHidden = false
        btnHideFiltr.isHidden = false
        UIView.animate(withDuration: 0.3) {
            self.viewFilter.transform = self.viewFilter.transform.translatedBy(x: 0, y: -500)
        }
        
    }
    @IBAction func btnActionHideFilter(_ sender: Any) {
        
        btnHideFiltr.isHidden = true
        UIView.animate(withDuration: 0.3, animations: {
            self.viewFilter.transform = self.viewFilter.transform.translatedBy(x: 0, y: 500)
        }) { (finished) in
            self.viewFilter.isHidden = true
        }
       
    }
   
    
    @IBAction func btnActionMenu(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("OpenSideBar"), object: nil)
    }
    
    @IBAction func btnActionAddres(_ sender: Any) {
//        let vc = SelectLocationVC(nibName: String(describing: SelectLocationVC.self), bundle: nil)
//        vc.delegate = self
//        self.present(vc, animated: true, completion: nil)
        guard let vc = R.storyboard.details.selectAddressVC() else { return }
        Router.shared.pushVC(vc: vc)
    }
    
    func getAddress(){
        UserEP.getAddress.request(loader: false, success: { (res) in
            guard let data = res as? AddressModel else { return }
            if data.data?.result?.count == 0{
                UserEP.addAddress(address: /UserPreference.shared.data?.address, street_address: "12", city: "City", postCode: "123456", latitude: "\(/UserPreference.shared.data?.latitude)", longitude: "\(/UserPreference.shared.data?.longitude)", is_default: 1).request(loader: false, success: { (res) in
                    
                }) { (error) in
                    
                }
            }else{
                self.getDefaultAddress()
            }
        }) { (error) in
            Toast.shared.showAlert(type: .apiFailure, message: /error)
        }
    }
    func getDefaultAddress(){
        UserEP.getDefaultAddress.request(loader: false, success: { (res) in
            guard let data = res as? DefaultAddress else { return }
            //self.addressID = /data.data?.id
            self.lblAddress.text = /data.data?.address
        }) { (error) in
            print(/error)
        }
    }
    
    
}

extension HomeVC:SelectLocationDelegate,UITextFieldDelegate{
    
    func locationSelected(address: String, city: String, state: String, country: String, zipcode: String, lat: Double, long: Double) {
        self.lblAddress.text = address
        self.lat = lat
        self.long = long
        shouldLoad = true
        getHome()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let currentText = textField.text as NSString? else {
            return false
        }
        let updatedText = currentText.replacingCharacters(in: range, with: string)
       
        if updatedText.count > 0{
            tblMain.isHidden = true
            tblSearch.isHidden = false
                searchShop(text:updatedText)
            isSearchActive = true
            }else if updatedText.count == 0{
            isSearchActive = false
            tblMain.isHidden = false
            tblSearch.isHidden = true
            getHome()
            }
       
        return true
    }
    
    func searchShop(text:String){
        UserEP.searchHome(offset: 1, limit: 100, lat: "\(lat)", long: "\(long)", search: text, category: nil, distance: radius).request(loader: false, success: { (res) in
            guard let data = res as? SearchModel else { return }
            self.searchData = data.data ?? []
            self.configureSearchTable()
        }) { (error) in
            Toast.shared.showAlert(type: .apiFailure, message: /error)
        }
        
//        UserEP.getShops(offset:1,limit:50,lat: "\(lat)", long: "\(long)", categoryId: nil, search: text, distance: radius).request(loader: false, success: { (res) in
//            guard let data = res as? ShopModel else { return }
//            self.shops = data.data?.result ?? []
//            self.configureTable()
//        }) { (error) in
//            Toast.shared.showAlert(type: .apiFailure, message: /error)
//        }
    }
    

}
