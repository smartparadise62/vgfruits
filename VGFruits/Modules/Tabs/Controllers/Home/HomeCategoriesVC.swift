//
//  HomeCategoriesVC.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 11/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class HomeCategoriesVC: UIViewController {

  //MARK: - IBOutlets
    
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var cvCat: UICollectionView!{
        didSet{
            cvCat.registerXIB(CellIdentifiers.CategoryCell.rawValue)
        }
    }
    
    @IBOutlet weak var tfSearch: UITextField!
    
    
    var selectedIndex = -1
    var catDS:CollectionDataSource?

    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tfSearch.delegate = self
        let categories = Utility.shared.getCategories() ?? []
        configureCollection(categories:categories)
        Utility.dropShadow(mView: viewSearch, radius: 4, color: .lightGray, size: CGSize(width: 0.5, height: 0.5))
    }
    
    func configureCollection(categories:[Categories]){
        
        let width = UIScreen.main.bounds.size.width / 2
        catDS = CollectionDataSource(_items: categories, _identifier: CellIdentifiers.CategoryCell.rawValue, _collectionView: cvCat, _size: CGSize(width: width, height: width), _edgeInsets: nil, _lineSpacing: nil, _interItemSpacing: nil)
        catDS?.configureCell = {  (cell,item,index) in
            guard let mCell = cell as? CategoryCell else { return }
            mCell.item = item
            if self.selectedIndex == index.row{
                mCell.viewBack.backgroundColor = #colorLiteral(red: 0.76, green: 0.28, blue: 0.2, alpha: 1)
                mCell.lblTitle.textColor = .white
                mCell.lblSub.textColor = .white
                
            }else{
                mCell.viewBack.backgroundColor = .white
                mCell.lblTitle.textColor = .black
                mCell.lblSub.textColor = .lightGray
                
            }
            
        }
        catDS?.didSelectItem = {
            (index,item) in
            guard let data = item as? Categories else { return }
            self.selectedIndex = index.row
            self.cvCat.reloadData()
            Loader.shared.start()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                NotificationCenter.default.post(name: Notification.Name("CategorySelected"), object: nil, userInfo: ["id":/data.id])
                Router.shared.popFromInitialNav()
            }
            
        }
        
        cvCat.delegate = catDS
        cvCat.dataSource = catDS
        cvCat.reloadData()
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnActionBack(_ sender: Any) {
        Router.shared.popFromInitialNav()
       }
}

extension HomeCategoriesVC:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let currentText = textField.text as NSString? else {
            return false
        }
        let updatedText = currentText.replacingCharacters(in: range, with: string)
       
        if updatedText.count > 0{
                
                       
                searchCategories(text:updatedText)
            }else if updatedText.count == 0{
                
                let categories = Utility.shared.getCategories() ?? []
                configureCollection(categories:categories)
            }
       
        return true
    }
    
    func searchCategories(text:String){
        UserEP.getCategory(offset: 1, limit: 100, search: text).request(loader: false, success: { (res) in
            guard let data = res as? CategoryModel else { return }
            let categories = data.data?.result ?? []
            self.configureCollection(categories: categories)
        }) { (error) in
            Toast.shared.showAlert(type: .apiFailure, message: /error)
        }
    }
}
