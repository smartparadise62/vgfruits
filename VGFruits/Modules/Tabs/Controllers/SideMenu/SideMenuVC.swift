//
//  SideMenuVC.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 05/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class SideMenuVC: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var tblMenu: UITableView!{
        didSet{
            tblMenu.registerXIB(CellIdentifiers.SideMenuCell.rawValue)
        }
    }
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    //MARK:Properties
    var menuDataSource:TableViewDataSource?
    
    //["icon":#imageLiteral(resourceName: "bookmark-group"),"name":BarItems.bookmark.rawValue]
    //["icon":#imageLiteral(resourceName: "Help&support"),"name":BarItems.help.rawValue],
    //,["icon":#imageLiteral(resourceName: "Payment-img"),"name":BarItems.paymentOptions.rawValue]
    var sideArray = [["icon":#imageLiteral(resourceName: "home-side.png"),"name":BarItems.home.rawValue.localize],["icon":#imageLiteral(resourceName: "Offers-group"),"name":BarItems.offers.rawValue.localize],["icon":#imageLiteral(resourceName: "History-group"),"name":BarItems.history.rawValue.localize],["icon":#imageLiteral(resourceName: "Order-img-group"),"name":BarItems.setting.rawValue.localize],["icon":#imageLiteral(resourceName: "Privacy-side"),"name":BarItems.privacyPolicy.rawValue.localize],["icon":#imageLiteral(resourceName: "Terms-side"),"name":BarItems.aboutUs.rawValue.localize],["icon":#imageLiteral(resourceName: "Help-side.png"),"name":BarItems.logout.rawValue.localize]]
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTable()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let data = UserPreference.shared.data
        lblName.text = /data?.first_name + " " + /data?.last_name
        lblAddress.text = /data?.address
        imgProfile.setImageKF(/data?.profile, placeholder: UIImage(named:"personn"))
    }
    
    
    func configureTable(){
        menuDataSource = TableViewDataSource(items: sideArray, tableView: tblMenu, cellIdentifier: CellIdentifiers.SideMenuCell.rawValue, configureCellBlock: { (cell, item, index) in
            guard let mCell = cell as? SideMenuCell else { return }
            mCell.item = item
        }, aRowSelectedListener: { (index, item) in
            self.handleSelection(index:index.row)
        }, willDisplayCell: nil, viewforHeaderInSection: nil, scrollToTop: nil)
        tblMenu.delegate = menuDataSource
        tblMenu.dataSource = menuDataSource
        tblMenu.reloadData()
    }
    
    func handleSelection(index:Int){
            //fireCloseNotification()
            NotificationCenter.default.post(name: Notification.Name(rawValue: "MenuTapped"), object: nil, userInfo: ["index":index])
    }

    func fireCloseNotification(){
           NotificationCenter.default.post(name: Notification.Name("CloseDrawer"), object: nil)
       }
    
    @IBAction func btnActionProfile(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MenuTapped"), object: nil, userInfo: ["index":-5])
    }
    
    
    
}
