//
//  EditProfileVC.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 12/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift

class EditProfileVC: UIViewController {

    @IBOutlet weak var lastWidth: NSLayoutConstraint!
    @IBOutlet weak var widthFirst: NSLayoutConstraint!
    //MARK: - IBOutlets
    
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var tfFirstName: ACFloatingTextfield!
    @IBOutlet weak var tfLastName: ACFloatingTextfield!
    
    @IBOutlet weak var tfPhone: ACFloatingTextfield!
    @IBOutlet weak var tfAddress: ACFloatingTextfield!
    
    @IBOutlet weak var tfCode: ACFloatingTextfield!
    @IBOutlet weak var phoneWhite: UIView!
    @IBOutlet weak var addressWhite: UIView!
    @IBOutlet weak var lastWhite: UIView!
    @IBOutlet weak var firstWhite: UIView!
    
    var selectedPicture:UIImage?
    var lat = ""
    var long = ""
    
    //MARK: - Life Cycle Methods
      override func viewDidLoad() {
          super.viewDidLoad()
          setupView()
        if L102Language.currentAppleLanguage() == "fr"{
                   widthFirst.constant = 55
                   lastWidth.constant =  90
               }
      }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let data = UserPreference.shared.data
        lblName.text = /data?.first_name + " " + /data?.last_name
        lblAddress.text = /data?.address
        imgProfile.setImageKF(/data?.profile, placeholder:#imageLiteral(resourceName: "personn") )
        
        lblPhone.text = "\(/data?.phone_code)" + /data?.phone
        tfFirstName.text = /data?.first_name
        tfLastName.text = /data?.last_name
        
        tfCode.text = "\(/data?.phone_code)"
        tfAddress.text = /data?.address
        lat = "\(/data?.latitude)"
        long = "\(/data?.longitude)"
        
    }
      
    
    
    
    func setupView(){
        Utility.dropShadow(mView: viewTop, radius: 4, color: .lightGray, size: CGSize(width: 1, height: 1))
        
        tfFirstName.delegate = self
        tfLastName.delegate = self
        tfPhone.delegate = self
        tfAddress.delegate = self
      }
    
    func validate()->String{
        if  /tfFirstName.text?.isEmpty || /tfLastName.text?.isEmpty ||  /tfAddress.text?.isEmpty  {
            return AlertMessage.REQUIRED_EMPTY
        }
//        else if !Utility.isValidPhone(value: /tfPhone.text){
//            return AlertMessage.INVALID_PHONE
//        }
        return AlertMessage.EMPTY_SSM
    }
      
      
    //MARK: - IBAction Methods
    @IBAction func btnActionBack(_ sender: Any) {
        Router.shared.popFromInitialNav()
    }
    
    @IBAction func btnActionImage(_ sender: Any) {
        DispatchQueue.main.async {
                          AttachmentHandler.shared.showAttachmentActionSheet(vc: self)
                          AttachmentHandler.shared.imagePickedBlock = { [weak self] item in
                              self?.selectedPicture = item
                           self?.imgProfile.image = item
                             
                          }
                      }
    }
    
    @IBAction func btnActionRemoveAccount(_ sender: Any) {
        
    }
    
    @IBAction func btnActionUpdate(_ sender: Any) {
        let valid = validate()
        if valid == "" {
            UserEP.editProfile(firstName: /tfFirstName.text, lastName: /tfLastName.text, address: /tfAddress.text, lat: self.lat, long: self.long, profile: selectedPicture).request(loader: true, success: { (res) in
                guard let data = res as? Login else { return }
                UserPreference.shared.data = data.data
                Toast.shared.showAlert(type: .success, message: /data.message)
                Router.shared.popFromInitialNav()
            }) { (error) in
                Toast.shared.showAlert(type: .apiFailure, message: /error)
            }
        }
    }
    
    @IBAction func btnActionAddress(_ sender: Any) {
//        let vc = SelectLocationVC(nibName: String(describing: SelectLocationVC.self), bundle: nil)
//                     vc.delegate = self
//                     self.present(vc, animated: true, completion: nil)
        
        guard let vc = R.storyboard.details.addNewAddress() else { return}
             vc.delegate = self
             vc.isForAdding = false
             self.present(vc, animated: true, completion: nil)
    }
    
}
extension EditProfileVC:UITextFieldDelegate,SelectLocationDelegate{



func locationSelected(address: String, city: String, state: String, country: String, zipcode: String, lat: Double, long: Double) {
    self.tfAddress.text = address
    addressWhite.isHidden = false
    self.lat = "\(lat)"
    self.long = "\(long)"
}
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tfFirstName{
            firstWhite.isHidden = false
        }else if textField == tfLastName{
            lastWhite.isHidden = false
        }else if textField == tfPhone{
            phoneWhite.isHidden = false
        }else if textField == tfAddress{
            addressWhite.isHidden = false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tfFirstName{
            if /textField.text?.isEmpty{
                firstWhite.isHidden = true
            }
        }else if textField == tfLastName{
            if /textField.text?.isEmpty{
                lastWhite.isHidden = true
            }
        }else if textField == tfPhone{
            if /textField.text?.isEmpty{
                phoneWhite.isHidden = true
            }
        }else if textField == tfAddress{
            if /textField.text?.isEmpty{
                addressWhite.isHidden = true
            }
        }
    }
}
