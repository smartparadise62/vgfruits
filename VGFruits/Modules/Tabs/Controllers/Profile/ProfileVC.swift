//
//  ProfileVC.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 05/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {

     
    //MARK: - IBOutlets
    
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
    }
    
    func setupView(){
        Utility.dropShadow(mView: viewTop, radius: 4, color: .lightGray, size: CGSize(width: 1, height: 1))
        let data = UserPreference.shared.data
        lblAddress.text = /data?.address
        lblName.text = /data?.first_name + " " + /data?.last_name
        lblPhone.text = "\(/data?.phone_code)" + /data?.phone
        imgProfile.setImageKF(/data?.profile, placeholder: UIImage(named:"personn"))
    }
    
    
    //MARK: - IBAction Methods
    @IBAction func btnActionMenu(_ sender: Any) {
        Router.shared.popFromInitialNav()
       }

    @IBAction func btnActionAddReview(_ sender: Any) {
    }
    
    @IBAction func btnActionEditProfile(_ sender: Any) {
        guard let vc = R.storyboard.tabs.editProfileVC() else { return }
        Router.shared.pushVC(vc: vc)
    }
    
    @IBAction func btnActionAddPhoto(_ sender: Any) {
        
    }
    
}
