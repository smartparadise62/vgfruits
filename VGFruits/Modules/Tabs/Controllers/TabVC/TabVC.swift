//
//  TabVC.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 05/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit
import KYDrawerController

class TabVC: UITabBarController {
    
    
    var bottomBar:BottomTab?

    override func viewDidLoad() {
        super.viewDidLoad()
        if Device.IS_IPHONE_X{
            bottomBar = BottomTab(frame: CGRect(x: 0, y: UIScreen.main.bounds.size.height - 100, width: UIScreen.main.bounds.size.width
                , height: 100))
        }else{
            bottomBar = BottomTab(frame: CGRect(x: 0, y: UIScreen.main.bounds.size.height - 80, width: UIScreen.main.bounds.size.width
                          , height: 80))
        }
        
        self.view.addSubview(bottomBar!)
        self.view.bringSubviewToFront(bottomBar!)
        
        
        bottomBar?.homeTapped = {
            self.selectedIndex = 0
            self.removeProductView()
        }
        bottomBar?.orderTapped = {
            
            self.selectedIndex = 2
            self.removeProductView()
        }
        
        
        bottomBar?.notificationTapped = {
            self.selectedIndex = 1
            self.removeProductView()
            
        }
        bottomBar?.cartTapped = {
            self.selectedIndex = 3
            self.removeProductView()
        }
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(openSideMenu), name: Notification.Name("OpenSideBar"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(menuTapped), name: Notification.Name(rawValue: "MenuTapped"), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToCart), name: Notification.Name("GoToCart"), object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(moveToOrder), name: Notification.Name("MoveToOrders"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToMenu), name: Notification.Name("MoveToMenu"), object: nil)
        
        CartManager.shared.getCart { (items, id,stripe) in
                           NotificationCenter.default.post(name: Notification.Name("CartUpdated"), object: nil, userInfo: ["count":items.count])
                       }
        
        
        
    }
    var productView:AddProductView?
    func addProductView(){
        let headerHeight = UIScreen.main.bounds.size.height * 0.12
        productView = AddProductView(frame: CGRect(x: 0, y: headerHeight, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height - headerHeight))
        self.view.addSubview(productView!)
        self.view.bringSubviewToFront(productView!)
    }
    
    func removeProductView(){
        if let view = productView{
            view.removeFromSuperview()
        }
    }
    
    
    @objc func openSideMenu(_ notification:Notification){
        if let vc = self.navigationController?.parent as? KYDrawerController{
            vc.setDrawerState(.opened, animated: true)
        }
    }
    
    @objc func menuTapped(_ notification:Notification){
        guard let dict = notification.userInfo as? [String:Any] else { return }
        if let item = dict["index"] as? Int{
            handleMenuSelection(item:item)
        }
    }
    
    @objc func moveToCart(_ notification:Notification){
        NotificationCenter.default.post(name: Notification.Name("TabTapped"), object: nil, userInfo: ["index":3])
        selectedIndex = 3
    }
    @objc func moveToOrder(_ notification:Notification){
        NotificationCenter.default.post(name: Notification.Name("TabTapped"), object: nil, userInfo: ["index":2])
        selectedIndex = 2
    }
    
     @objc func moveToMenu(_ notification:Notification){
        NotificationCenter.default.post(name: Notification.Name("TabTapped"), object: nil, userInfo: ["index":0])
           selectedIndex = 0
       }
    
    @objc func closeDrawer(){
        if let vc = self.navigationController?.parent as? KYDrawerController{
                        vc.setDrawerState(.closed, animated: true)
                    }
    }
    
    func handleMenuSelection(item:Int){
        
        switch item{
        case 0:
            NotificationCenter.default.post(name: Notification.Name("TabTapped"), object: nil, userInfo: ["index":0])
            selectedIndex = 0
            
        case -9:
            guard let vc = R.storyboard.sideBar.paymentMethodsVC() else { return }
            Router.shared.pushVC(vc: vc)
            
        case 1:
            guard let vc = R.storyboard.details.couponsVC() else { return }
            vc.fromSide = true
            Router.shared.pushVC(vc: vc)
        
        case 2:
            guard let vc = R.storyboard.sideBar.historyVC() else { return }
            Router.shared.pushVC(vc: vc)
        case 3:
            guard let vc = R.storyboard.sideBar.settingsVC() else { return }
            Router.shared.pushVC(vc: vc)
            
        case 4:
            
            guard let vc = R.storyboard.sideBar.privacyVC() else { return }
                    
            Router.shared.pushVC(vc: vc)
            
        case 5:
                guard let vc = R.storyboard.sideBar.aboutUsVC() else { return }
                       
                Router.shared.pushVC(vc: vc)
            
        case 6:
            logout()

        case -5:
            guard let vc = R.storyboard.tabs.profileVC() else { return }
            Router.shared.pushVC(vc: vc)
        default:
            break
        }
        
        if let vc = self.navigationController?.parent as? KYDrawerController{
            vc.setDrawerState(.closed, animated: true)
        }
        
    }
    var isCalled = false
    func logout(){
        if !isCalled{
            isCalled = true
            UserEP.logout.request(loader: true, success: { (res) in
                       Router.shared.setLoginAsRoot()
                   }) { (error) in
                       Toast.shared.showAlert(type: .apiFailure, message: /error)
                   }
        }
       
    }
    
    

}
