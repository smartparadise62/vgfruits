//
//  CartManager.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 25/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit
import CoreData

class CartManager {
    
   //MARK: - Properties
    static let shared = CartManager()
    
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(removeCart), name: Notification.Name("DeleteCart"), object: nil)
    }
    
    //MARK: - Method to add Product to Cart
    func addProductToCart(product:CartProduct,addingShopId:Int,stripeId:String){
        
        getCart { (list, shopId,stripeId) in
            if list.count > 0{
//                if shopId != addingShopId{
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: "AnotherRequest"), object: nil, userInfo: ["shopId":shopId,"addingShopId":addingShopId,"product":product])
//                }else{
                    self.updateCart(product: product, shopId: shopId, shouldRemove: false)
 //               }
            }else{
                self.createCart(product:product, shopId: addingShopId,shopStripeId:stripeId)
            }
        }
    }
    
    func removeProductFromCart(product:CartProduct,addingShopId:Int,stripeId:String){
        getCart { (list, shopId,stripeId) in
                   if list.count > 0{
                       if shopId != addingShopId{
                           NotificationCenter.default.post(name: Notification.Name(rawValue: "AnotherRequest"), object: nil)
                       }else{
                        self.updateCart(product: product, shopId: shopId,shouldRemove:true)
                       }
                   }else{
                       self.createCart(product:product, shopId: addingShopId,shopStripeId: stripeId)
                   }
               }
    }
    
    //MARK: - Method to Create Cart
    func createCart(product:CartProduct,shopId:Int,shopStripeId:String){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let cartEntity = NSEntityDescription.entity(forEntityName: "Cart", in: context)!
        let productEntity = NSManagedObject(entity: cartEntity, insertInto: context)
        let model = CartProductList(value: [product])
        productEntity.setValue(model, forKey: "products")
        productEntity.setValue(shopId, forKey: "shopId")
        productEntity.setValue(shopStripeId, forKey: "shopStripeId")
        
        do{
            try context.save()
            self.getCart { (items, id,stripeId) in
                NotificationCenter.default.post(name: Notification.Name("CartUpdated"), object: nil, userInfo: ["count":items.count])
            }
        }catch let error{
            print(error.localizedDescription)
        }
    }
    
    //MARK: - Method to Update the Cart
    func updateCart(product:CartProduct,shopId:Int,shouldRemove:Bool){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Cart")
        fetchRequest.predicate = NSPredicate(format: "shopId = %d", shopId)
        do{
            let result = try context.fetch(fetchRequest)
            let object = result.first as? NSManagedObject
            var productsList = (object?.value(forKey: "products") as? CartProductList)?.cartProducts ?? []
            
            let mProduct = productsList.filter({$0.productId == product.productId})
            let alreadyPresentProduct = mProduct.first
            if mProduct.count > 0{
                let mArray = productsList as NSArray
                let index = mArray.index(of: alreadyPresentProduct)
                if shouldRemove{
                    
                    if alreadyPresentProduct?.productQuantity == 1{
                        productsList.remove(at: index)
                    }else{
                        alreadyPresentProduct?.productQuantity = (/alreadyPresentProduct?.productQuantity) - 1
                        productsList[index] = alreadyPresentProduct!
                    }
                    
                }else{
                     alreadyPresentProduct?.productQuantity = (/alreadyPresentProduct?.productQuantity) + 1
                    productsList[index] = alreadyPresentProduct!
                }
                
            }else{
                productsList.append(product)
            }
            if productsList.count == 0{
                deleteCart(shopId:shopId)
            }else{
                let model = CartProductList(value: productsList)
                object?.setValue(model, forKey: "products")
                do{
                    try context.save()
                    self.getCart { (items, id,stripeID) in
                        NotificationCenter.default.post(name: Notification.Name("CartUpdated"), object: nil, userInfo: ["count":items.count])
                    }
                }catch let error{
                    print(error)
                }
            }
        }catch let error{
            print(error.localizedDescription)
        }
    }
    
    //MARK: - Method to Delete the cart
    @objc func removeCart(_ notification:Notification){
        if let data = notification.userInfo as? [String:Any]{
            deleteCart(shopId: data["shopId"] as? Int ?? 0)
        }
        
    }
    
    func deleteCart(shopId:Int){
       let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSManagedObject>(entityName: "Cart")
        request.predicate = NSPredicate(format: "shopId = %d", shopId)
        do{
            let result = try context.fetch(request)
            let object = result.first as! NSManagedObject
            context.delete(object)
            do{
                try context.save()
                self.getCart { (items, id,stripeId) in
                    NotificationCenter.default.post(name: Notification.Name("CartUpdated"), object: nil, userInfo: ["count":items.count])
                }
            }catch{
                print(error.localizedDescription)
            }
        }catch let error{
            print(error.localizedDescription)
        }
    }

    //Method to get cart
    func getCart(finished:@escaping(_ list:[CartProduct],_ shopId:Int,_ shopStripeId:String)->()){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSManagedObject>(entityName: "Cart")
        request.returnsObjectsAsFaults = false
        do{
            let cart = try context.fetch(request)
            let data = cart.first
            let list  = (data?.value(forKey: "products") as? CartProductList)?.cartProducts ?? []
            let shopId = data?.value(forKey: "shopId") as? Int ?? 0
            let shopStripeId = data?.value(forKey: "shopStripeId") as? String ?? ""
            finished(list,shopId,shopStripeId)
          
        }catch let error as NSError{
            print(error.localizedDescription)
        }
    }
    
        
}


