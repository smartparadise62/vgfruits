//
//  CartModel.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 25/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import Foundation


class CartProductList:NSObject,NSCoding {
    
    var cartProducts:[CartProduct]?
    
    init(value:[CartProduct]){
        cartProducts = value
    }
    
   
    func encode(with aCoder: NSCoder) {
        aCoder.encode(cartProducts, forKey: "CartProductList")
    }
    
    required init?(coder aDecoder: NSCoder) {
        cartProducts = aDecoder.decodeObject(forKey: "CartProductList") as? [CartProduct]
    }
}

class CartProduct:NSObject,NSCoding{
    var productId:Int?
    var productName:String?
    var productCategory:String?
    var productDescription:String?
    var productPrice:Double?
    var productQuantity:Int?
    var productImage:String?
    var createdAt:Date?
    
    init(productId:Int,productName:String,productCategory:String,productDescription:String,productPrice:Double,productQuantity:Int,productImage:String,createdAt:Date){
        self.productId = productId
        self.productName = productName
        self.productCategory = productCategory
        self.productDescription = productDescription
        self.productPrice = productPrice
        self.productQuantity = productQuantity
        self.productImage = productImage
        self.createdAt = createdAt
     }
     

     func encode(with aCoder: NSCoder) {
        aCoder.encode(productId, forKey: "productId")
        aCoder.encode(productName,forKey: "productName")
        aCoder.encode(productName,forKey: "productCategory")
        aCoder.encode(productName,forKey: "productDescription")
        aCoder.encode(productPrice,forKey: "productPrice")
        aCoder.encode(productQuantity, forKey: "productQuantity")
        aCoder.encode(productImage, forKey: "productImage")
        aCoder.encode(createdAt,forKey: "createdAt")
     }
     
     required init?(coder aDecoder: NSCoder) {
         productId = aDecoder.decodeObject(forKey: "productId") as? Int
        productName = aDecoder.decodeObject(forKey: "productName") as? String
        productCategory = aDecoder.decodeObject(forKey: "productCategory") as? String
        productDescription = aDecoder.decodeObject(forKey: "productDescription") as? String
        productPrice = aDecoder.decodeObject(forKey: "productPrice") as? Double
        productQuantity = aDecoder.decodeObject(forKey: "productQuantity") as? Int
        productImage = aDecoder.decodeObject(forKey: "productImage") as? String
        createdAt = aDecoder.decodeObject(forKey: "createdAt") as? Date
     }
}
