//
//  PlacedOrder.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 26/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import Foundation

class PlacedOrderModel:Codable{
    var success:Bool?
    var code:Int?
    var message:String?
    var data:[PlacedOrder]?
    }

class PlacedOrder:Codable{
    var quantity:Int?
    var address_details:AddressData?
    var product_details:[Product]?
    var modified:Int?
    var order_date:Int?
    var user_id:Int?
    var coupon_id:Int?
    var service_fees:Int?
    var price:Int?
    var product_id:String?
    var taxes:Int?
    var discout:Int?
    var created:Int?
    var order_id:Int?
    var status:Int?
    var shop_id:String?
}


      
class StripeSecret:Codable{
    var success :Bool?
    var code : Int?
    var message : String?
    var data:Secret?
}

class Secret:Codable{
    var secret:String?
    var stripe_id : String?
    
}
