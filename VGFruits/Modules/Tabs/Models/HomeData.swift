//
//  HomeData.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 19/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import Foundation


class HomeModel:Codable{
    var success:Bool?
    var code:Int?
    var message:String?
    var data:HomeData?
}

class HomeData:Codable{
    var categories:[Categories]?
    var banners:[Banners]?
    var newShops:[ShopData]?
    var topRatedShops:[ShopData]?
}

class Banners:Codable{
    var status:Int?
    var id:Int?
    var created:Int?
    var image:String?
    var modified:Int?
}
 

class ShopData:Codable{
      var status : Int?
      var profile : String?
      var is_online :Int?
      var phone_code : Int?
      var accept_order : Int?
      var total_distance : Int?
      var service_fees : Int?
      var latitude : Double?
      var last_name : String?
      var is_free : Int?
      var id : Int?
      var taxes : Int?
      var email : String?
      var longitude : Double?
      var phone : String?
      var user_type : Int?
      var delivery_charges : Int?
      var min_order : Int?
      var first_name : String?
      var rating : Double?
      var address : String?
    var stripeId : String?
    
    }
