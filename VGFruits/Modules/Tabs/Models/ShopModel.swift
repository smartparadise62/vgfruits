//
//  ShopModel.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 11/07/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import Foundation


class ShopModel:Codable{
    var success:Bool?
    var code:Int?
    var message:String?
    var data:ShopResult?
}

class ShopResult:Codable{
    var result:[ShopData]?
    var pagination:Pagination?
}
