//
//  SearchModel.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 27/07/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import Foundation

class SearchModel:Codable{
    var success: Bool?
    var code: Int?
    var message: String?
    var data:[SearchData]?
    
}

class SearchData:Codable{
    var id:Int?
    var user_id:Int?
    var category_id:Int?
    var sub_category_id:Int?
    var is_feature:Int?
    var name:String?
    var price:Int?
    var image:String?
    var stock:Int?
    var description:String?
    var status:Int?
    var rating:Int?
    var created:Int?
    var modified:Int?
    var category_name:String?
    var is_fav:Int?
    var shop_name:String?
    var address:String?
    var first_name:String?
    var last_name:String?
    var profile:String?
    var service_fees:Int?
    var delivery_charges:Int?
    var taxes:Int?
    var type:Int?
    
    var min_order:Int?
    var accept_order:Int?
   
    var is_free:Int?
    var is_online:Int?
    var email:String?
    var phone:String?
    var phone_code:Int?
    
    var user_type:Int?
    var latitude:Double?
    var longitude:Double?
  
    var total_distance:Int?
}
