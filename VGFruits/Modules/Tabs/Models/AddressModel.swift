//
//  AddressModel.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 26/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import Foundation


class AddressModel:Codable{
    var success:Bool?
    var code:Int?
    var message:String?
    var data:AddressResult?
}

class AddressResult:Codable{
    var result:[AddressData]?
    var pagination:Pagination?
}

class AddressData:Codable{
    var id:Int?
    var post_code:String?
    var modified : Int?
    var address_line_two : String?
    var longitude : Double?
    var user_id : Int?
    var latitude : Double?
    var address : String?
    var city : String?
    var created : Int?
    var is_default : Int?
    var street_address : String?
}

class DefaultAddress:Codable{
    var success:Bool?
    var code:Int?
    var message:String?
    var data:AddressData?
}
