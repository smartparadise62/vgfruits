//
//  OrderModel.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 27/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import Foundation

class OrderModel:Codable{
    var success:Bool?
    var code:Int?
    var message:String?
    var data:OrderResult?
}

class OrderResult:Codable{
    var result:[OrderList]?
    var pagination:Pagination?
}

class OrderList:Codable{
    var product_details:[ProductDetails]?
    var last_name:String?
    var accept_order:Int?
    var order_otp:Int?
    var id:Int?
    var taxes:Int?
    var shop_lng:Double?
    var estimate_time:String?
    var reject_message:String?
    var address_id:Int?
    var created:Int?
    var profile:String?
    var address:String?
    var modified:Int?
    var address_details:AddressData?
    var order_date:Int?
    var coupon_details:Int?
    var driver_info:Int?
    var order_status:Int?
    var delivery_fees:Int?
    var shop_id:Int?
    var discout:Int?
    var shop_phone_code:Int?
    var phone_code:Int?
    var email:String?
    var service_fees:Int?
    var payment_status:Int?
    var longitude:Double?
    var shop_name:String?
    var quantity:Int?
    var shop_phone:String?
    var price:Int?
    var product_id:String?
    var shop_address:String?
    var first_name:String?
    var shop_lat:Double?
    var shop_email:String?
    var user_id:Int?
    var driver_id:Int?
    var coupon_id:Int?
    var min_order:Int?
    var shop_profile:String?
    var payment_details:PaymentDetails?
    var phone:String?
    var latitude:Double?
}

class PaymentDetails:Codable{
   
             var amount : Int?
             var currency : String?
             var stripe_id : String?
             var source_id : String?
             var payment_method_id : String?
             var stripe_description : String?
           
}

class ProductDetails:Codable{
    var id:Int?
    var rating:Int?
    var modified:Int?
    var is_feature:Int?
    var description:String?
    var category_id:Int?
    var qyt:Int?
    var stock:Int?
    var user_id:Int?
    var price:Int?
    var image:String?
    var created:Int?
    var totalPrice:Int?
    var name:String?
    var status:Int?
}
