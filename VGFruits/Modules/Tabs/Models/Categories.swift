//
//  Categories.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 19/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import Foundation



class CategoryModel:Codable{
    var success:Bool?
    var code:Int?
    var message:String?
    var data:CategoryResult?
}

class CategoryResult:Codable{
    var result:[Categories]?
    var pagination:Pagination?
}

class Categories:Codable{
    var status:Int?
    var id:Int?
    var image:String?
    var created:Int?
    var app_category_id:Int?
    var modified:Int?
    var name:String?
    var application_category_name:String?
    
    
    init(){
        
    }
}

class Pagination:Codable{
    var limit:Int?
    var currentPage:Int?
    var totalPage:Int?
    var totalRecord:Int?
}
