//
//  BottomTab.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 04/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class BottomTab: UIView {
    
    
    //MARK: - IBOutlets
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var backView: UIView!
   // @IBOutlet weak var homeWidth: NSLayoutConstraint!
    @IBOutlet weak var homeActiveView: UIView!
    @IBOutlet weak var homeInactiveView: UIView!
   // @IBOutlet weak var ordersWidth: NSLayoutConstraint!
    @IBOutlet weak var orderInacvieView: UIView!
    @IBOutlet weak var orderActiveView: UIView!
 //   @IBOutlet weak var addProductWidth: NSLayoutConstraint!
 //   @IBOutlet weak var profileWIdth: NSLayoutConstraint!
    @IBOutlet weak var profileActiveView: UIView!
    @IBOutlet weak var profileInactiveView: UIView!
   // @IBOutlet weak var reviewsWIdth: NSLayoutConstraint!
    @IBOutlet weak var reviewActiveView: UIView!
    @IBOutlet weak var reviewInactiveView: UIView!
    
    @IBOutlet weak var iconHome: UIImageView!
    
    @IBOutlet weak var iconNotification: UIImageView!
    
    @IBOutlet weak var iconOrder: UIImageView!
    
    @IBOutlet weak var lblItemCount: UILabel!
    
    //MARK: - Properties
    var homeTapped:(()->())?
    var orderTapped:(()->())?
    
    var notificationTapped:(()->())?
    var cartTapped:(()->())?
    
    //MARK: - View Initialization Meethods
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    
    func commonInit(){
        Bundle.main.loadNibNamed("BottomTab", owner: self, options: nil)
        self.contentView.frame = self.bounds
        self.contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        self.addSubview(contentView)
        Utility.dropShadow(mView: backView, radius: 4, color: .lightGray, size: CGSize(width: 1, height: -1))
        setActiveView(index: 3)
        lblItemCount.isHidden = true
        setActiveIcons(index:0)
        NotificationCenter.default.addObserver(self, selector: #selector(itemCountUpdated), name: Notification.Name("CartUpdated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(tabTapped), name: Notification.Name("TabTapped"), object: nil)
        
    }
    
    @objc func tabTapped(_ sender:Notification){
        if let data = sender.userInfo as? [String:Any]{
            let index = data["index"] as? Int
            setActiveIcons(index: /index)
        }
    }
    
    @objc func itemCountUpdated(_ notification:Notification){
        guard let item = notification.userInfo as? [String:Any] else { return }
        let count = item["count"] as? Int
        if /count > 0 {
            lblItemCount.text = "\(/count)"
            lblItemCount.isHidden = false
        }else {
            lblItemCount.isHidden = true
        }
        
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnActionHome(_ sender: Any) {
   //     setActiveView(index: 0)
        setActiveIcons(index: 0)
        homeTapped?()
    }
    
    @IBAction func btnActionOrders(_ sender: Any) {
      //  setActiveView(index: 1)
        setActiveIcons(index: 1)
        notificationTapped?()
    }
    
   
    
    
    @IBAction func btnActionProfile(_ sender: Any) {
    //setActiveView(index: 2)
        setActiveIcons(index: 2)
        orderTapped?()
    }
    
    
    @IBAction func btnActionReviews(_ sender: Any) {
        //setActiveView(index: 3)
        //setActiveView(index: 3)
        cartTapped?()
    }
    
    func setActiveIcons(index:Int){
        iconHome.image = #imageLiteral(resourceName: "pin-gray.png")
        iconNotification.image = #imageLiteral(resourceName: "bell-icon-1")
        iconOrder.image = #imageLiteral(resourceName: "bag-icon.png")
        
        switch index{
        case 0:
            iconHome.image = #imageLiteral(resourceName: "Pin-selected.png")
        case 1:
            iconNotification.image = #imageLiteral(resourceName: "bell-selected.png")
        case 2:
            iconOrder.image = #imageLiteral(resourceName: "bag-green.png")
        default:
            break
        }
    }
    
    func setActiveView(index:Int){
       
        let inactiveWidth = (UIScreen.main.bounds.size.width - 110) / 4
        
      //  UIView.animate(withDuration: 0.1) {
            self.homeActiveView.isHidden = true
            self.orderActiveView.isHidden = true
            self.profileActiveView.isHidden = true
            self.reviewActiveView.isHidden = true
            
            self.homeInactiveView.isHidden = false
            self.orderInacvieView.isHidden = false
            self.profileInactiveView.isHidden = false
            self.reviewInactiveView.isHidden = false
            
//            self.homeWidth.constant = inactiveWidth
//            self.ordersWidth.constant = inactiveWidth
//            self.addProductWidth.constant = inactiveWidth
//            self.profileWIdth.constant = inactiveWidth
//            self.reviewsWIdth.constant = inactiveWidth
            
            self.layoutIfNeeded()
    //    }
        
        
        switch index{
        case 0:
            UIView.animate(withDuration: 0.1) {
                self.homeActiveView.isHidden = false
                self.homeInactiveView.isHidden = true
                //self.homeWidth.constant = 90
                self.layoutIfNeeded()
            }
            case 1:
            UIView.animate(withDuration: 0.1) {
                self.orderActiveView.isHidden = false
                self.orderInacvieView.isHidden = true
               // self.ordersWidth.constant = 90
                self.layoutIfNeeded()
            }
            
            case 2:
            UIView.animate(withDuration: 0.1) {
                self.profileActiveView.isHidden = false
                self.profileInactiveView.isHidden = true
             //   self.profileWIdth.constant = 90
                self.layoutIfNeeded()
            }
            case 3:
            UIView.animate(withDuration: 0.1) {
                self.reviewActiveView.isHidden = false
                self.reviewInactiveView.isHidden = true
             //   self.reviewsWIdth.constant = 90
                self.layoutIfNeeded()
            }
        default:
            break
        }
        
        
     }
    
}
