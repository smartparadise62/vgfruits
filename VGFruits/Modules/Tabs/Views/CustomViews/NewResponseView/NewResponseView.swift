//
//  OrderResponseView.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 09/07/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class NewResponseView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var lblOrderId: UILabel!
    
    @IBOutlet weak var imgIcon: UIImageView!
    
    @IBOutlet weak var orderStatus: UILabel!
    
    @IBOutlet weak var heightRate: NSLayoutConstraint!
    @IBOutlet weak var btnRate: UIButton!
    
    var data:OrderResponse?
    
    
    init(frame: CGRect,data:OrderResponse) {
        super.init(frame: frame)
        self.data = data
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    func commonInit(){
        Bundle.main.loadNibNamed(String(describing: NewResponseView.self), owner: self, options: nil)
   
    self.contentView.frame = self.bounds
    self.contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
    self.addSubview(contentView)
        btnRate.isHidden = true
        heightRate.constant = 0
        lblOrderId.text = "Order Id: \(/data?.orderID)"
        if /data?.orderStatus == 1{
            imgIcon.image = #imageLiteral(resourceName: "Tick-yellow.png")
            orderStatus.text = "Your order has been accepted"
        }else if /data?.orderStatus == 2{
            imgIcon.image = #imageLiteral(resourceName: "close-1")
            orderStatus.text = "Your order has been cancelled due to \(/data?.rejectMessage)"
        }else if /data?.orderStatus == 4{
            imgIcon.image = #imageLiteral(resourceName: "Tick-yellow.png")
            orderStatus.text = "Your order has been completed"
            btnRate.isHidden = false
            heightRate.constant = 35
        }
    }
    
    
    @IBAction func btnActionOk(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    
    @IBAction func btnActionRate(_ sender: Any) {
        guard let vc = R.storyboard.details.addRatingVC() else { return }
        vc.responseData = self.data
        Router.shared.pushVC(vc: vc)
        self.removeFromSuperview()
    }
    
}
