//
//  AddProductView.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 05/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift

class AddProductView: UIView {

    //MARK: - IBOutlets
    @IBOutlet weak var tfPrice: ACFloatingTextfield!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var whiteProductName: UIView!
    @IBOutlet weak var tfProductName: ACFloatingTextfield!
    @IBOutlet weak var whiteCategory: UIView!
    @IBOutlet weak var tfCategory: ACFloatingTextfield!
    @IBOutlet weak var whitePrice: UIView!
    @IBOutlet weak var tfDescription: ACFloatingTextfield!
    @IBOutlet weak var whiteQuantity: UIView!
    @IBOutlet weak var tfQuantity: ACFloatingTextfield!
    @IBOutlet weak var whiteDesc: UIView!
    
    
    //MARK: - View Initialization Methods
    override init(frame: CGRect) {
          super.init(frame:frame)
          commonInit()
      }
      
      required init?(coder: NSCoder) {
          super.init(coder: coder)
          commonInit()
      }
      
      
      func commonInit(){
          Bundle.main.loadNibNamed("AddProductView", owner: self, options: nil)
          self.contentView.frame = self.bounds
          self.contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
          self.addSubview(contentView)
        backView.layer.cornerRadius = 15
        backView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        Utility.dropShadow(mView: backView, radius: 4, color: .lightGray, size: CGSize(width: 1, height: -0.5))
          setupView()
      }
    
    func setupView(){
        tfCategory.delegate = self
        tfProductName.delegate = self
        tfPrice.delegate = self
        tfQuantity.delegate = self
        tfDescription.delegate = self
        whiteProductName.isHidden = true
        whitePrice.isHidden = true
         whiteQuantity.isHidden = true
         whiteDesc.isHidden = true
         whiteCategory.isHidden = true
        
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnActionAddImage(_ sender: Any) {
        
    }
    
    @IBAction func btnActionAdd(_ sender: Any) {
        
    }
    
    @IBAction func btnActionCategory(_ sender: Any) {
        
    }
}

extension AddProductView:UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tfProductName{
            whiteProductName.isHidden = false
        }else if textField == tfPrice{
            whitePrice.isHidden = false
        }else if textField == tfQuantity{
            whiteQuantity.isHidden = false
        }else if textField == tfDescription{
            whiteDesc.isHidden = false
        }else if textField == tfCategory {
            whiteCategory.isHidden = false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tfProductName{
            if /textField.text?.isEmpty{
                whiteProductName.isHidden = true
            }
        }else if textField == tfPrice{
            if /textField.text?.isEmpty{
                whitePrice.isHidden = true
            }
        }else if textField == tfQuantity{
            if /textField.text?.isEmpty{
                whiteQuantity.isHidden = true
            }
        }else if textField == tfDescription{
            if /textField.text?.isEmpty{
                whiteDesc.isHidden = true
            }
        }else if textField == tfCategory{
            if /textField.text?.isEmpty{
                whiteCategory.isHidden = true
            }
        }
    }
}
