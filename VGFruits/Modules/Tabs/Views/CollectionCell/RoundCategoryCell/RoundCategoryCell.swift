//
//  RoundCategoryCell.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 11/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class RoundCategoryCell: UICollectionViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var imgCat: UIImageView!
    @IBOutlet weak var lblCat: UILabel!
    
    //MARK: Properties
    var item:Any?{
        didSet{
            guard let data = item as? Categories else { return }
            if data.id == nil{
                imgCat.image = #imageLiteral(resourceName: "fruit.png")
            }else{
                imgCat.setImageKF(/data.image, placeholder: UIImage(named:"placeholder"))
            }
            lblCat.text = /data.name
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
