//
//  FilterCell.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 11/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit
import Cosmos


class FilterCell: UICollectionViewCell {

    @IBOutlet weak var viewRate: CosmosView!
    @IBOutlet weak var lblFilter: UILabel!
    @IBOutlet weak var viewBack: UIView!
    
    
    var item:Any?{
        didSet{
            guard let data = item as? String else { return }
            if data == "Over 4.5"{
                viewRate.isHidden = false
            }else{
                viewRate.isHidden = true
            }
            lblFilter.text = data
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
