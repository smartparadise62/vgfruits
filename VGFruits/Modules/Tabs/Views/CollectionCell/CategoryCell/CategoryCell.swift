//
//  CategoryCell.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 05/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var lblSub: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!{
        didSet{
            viewHeight.constant = UIScreen.main.bounds.size.width / 2.5
        }
    }
    
    @IBOutlet weak var imgCat: UIImageView!
    
    @IBOutlet weak var viewWidth: NSLayoutConstraint!{
        didSet{
            viewWidth.constant = UIScreen.main.bounds.size.width / 2.5
        }
    }
    
    @IBOutlet weak var viewBack: UIView!
    
    var item:Any?{
        didSet{
            guard let data = item as? Categories else { return }
            imgCat.setImageKF(/data.image, placeholder: UIImage(named:"placeholder"))
            lblTitle.text = /data.name
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Utility.dropShadow(mView: viewBack, radius: 4, color: .lightGray, size: CGSize(width: 1, height: 1))
    }

}
