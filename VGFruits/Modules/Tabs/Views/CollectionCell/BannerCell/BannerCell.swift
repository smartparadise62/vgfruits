//
//  BannerCell.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 11/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class BannerCell: UICollectionViewCell {

    @IBOutlet weak var imgHeight: NSLayoutConstraint!{
        didSet{
            imgHeight.constant = 135
        }
    }
    
    @IBOutlet weak var imgBanner: UIImageView!
    
    @IBOutlet weak var imgWidth: NSLayoutConstraint!{
        didSet{
            imgWidth.constant = UIScreen.main.bounds.size.width / 1.5
        }
    }
    var item:Any?{
        didSet{
            guard let data = item as? Banners else { return }
            imgBanner.setImageKF(/data.image, placeholder: UIImage(named: "placeholder"))
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

}
