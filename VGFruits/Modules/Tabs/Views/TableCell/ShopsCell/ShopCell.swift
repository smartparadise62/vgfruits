//
//  ShopCell.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 11/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit
import CoreLocation

class ShopCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var imgshop: UIImageView!
    @IBOutlet weak var lblShopName: UILabel!
    @IBOutlet weak var lblShopCat: UILabel!
    @IBOutlet weak var lblDeliveryStatus: UILabel!
    
    @IBOutlet weak var lblETA: UILabel!
    
    //MARK: - Properties
    var item:Any?{
        didSet{
            if let data = item as? ShopData  {
                imgshop.setImageKF(/data.profile, placeholder: UIImage(named: "placeholder"))
                 lblShopName.text = /data.first_name + " " + /data.last_name + "/" + "\(/data.rating)"
            
                 lblETA.text = Utility.eta(lat: /data.latitude, long: /data.longitude)
                if /data.delivery_charges == 0{
                    lblDeliveryStatus.text = "free".localize
                }else {
                    lblDeliveryStatus.text = "DeliveryCharges".localize + "€ \(/data.delivery_charges)"
                }
            }
            
            if let data = item as? SearchData{
                imgshop.setImageKF(/data.profile, placeholder: UIImage(named: "placeholder"))
                lblShopName.text = /data.first_name + " " + /data.last_name + "/" + "\(/data.rating)"
                               
                lblETA.text = Utility.eta(lat: /data.latitude, long: /data.longitude)
                if /data.delivery_charges == 0{
                                   lblDeliveryStatus.text = "free".localize
                               }else {
                                   lblDeliveryStatus.text = "DeliveryCharges".localize + "€ \(/data.delivery_charges)"
                               }
            }
            
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
    
    

   
    
}
