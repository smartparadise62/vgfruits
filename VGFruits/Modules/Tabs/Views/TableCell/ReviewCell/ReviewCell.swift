//
//  ReviewCell.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 05/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class ReviewCell: UITableViewCell {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var viewBack: UIView!
    
    //MARK: - Properties
    var item:Any?{
        didSet{
            
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        Utility.dropShadow(mView: viewBack, radius: 4, color: .lightGray, size: CGSize(width: 1, height: 1))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
