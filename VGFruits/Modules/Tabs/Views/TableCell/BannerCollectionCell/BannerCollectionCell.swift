//
//  BannerCollectionCell.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 11/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class BannerCollectionCell: UITableViewCell {
    
    
    //MARK: - IBOutlets
    @IBOutlet weak var cvBanner: UICollectionView!{
        didSet{
            cvBanner.registerXIB(CellIdentifiers.BannerCell.rawValue)
        }
    }
    
    @IBOutlet weak var pageControls: UIPageControl!

    //MARK: - Properties
    var item:Any?{
        didSet{
            guard let item = item as? [Banners] else { return }
            configureCollection(item:item)
        }
    }
    var cvDataSource:CollectionDataSource?
    
    
    //MARK: - Cell Initialization
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCollection(item:[Banners]){
          
        cvDataSource = CollectionDataSource(_items: item, _identifier: CellIdentifiers.BannerCell.rawValue, _collectionView: cvBanner, _size: CGSize(width: UIScreen.main.bounds.size.width / 1.5, height: 140), _edgeInsets: nil, _lineSpacing: nil, _interItemSpacing: nil)
          cvDataSource?.configureCell = {  (cell,item,index) in
              guard let mCell = cell as? BannerCell else { return }
              mCell.item = item
              
          }
          
          cvDataSource?.didSelectItem = {
              (index,item) in
             
          }
          
          cvBanner.delegate = cvDataSource
          cvBanner.dataSource = cvDataSource
          cvBanner.reloadData()
      }
}
