//
//  CategoryCVCell.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 11/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class CategoryCVCell: UITableViewCell {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var cvCategories: UICollectionView!{
        didSet{
            cvCategories.registerXIB(CellIdentifiers.RoundCategoryCell.rawValue)
        }
    }
    
    //MARK: - Properties

    var item:Any?{
        didSet{
            guard let data = item as? [Categories] else { return }
            configureCollection(items:data)
        }
    }
    
    var cvDataSource:CollectionDataSource?
    var selectedIndex = 0
    
    var didSelectCategory:((_ id:Int?)->())?
    
    //MARK: - Cell Initialization Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        NotificationCenter.default.addObserver(self, selector: #selector(categoryTapped), name: Notification.Name("CategorySelected"), object: nil)
    }
    
    @objc func categoryTapped(_ sender:Notification){
        if let data = sender.userInfo as? [String:Any]{
            let id = data["id"] as? Int
            self.selectedIndex = /id
            self.cvCategories.reloadData()
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //MARK: - IBAction Methods
    @IBAction func btnActionViewAll(_ sender: Any) {
        guard let vc = R.storyboard.tabs.homeCategoriesVC() else { return }
        Router.shared.pushVC(vc: vc)
    }
    
    func configureCollection(items:[Categories]){
        
        cvDataSource = CollectionDataSource(_items: items, _identifier: CellIdentifiers.RoundCategoryCell.rawValue, _collectionView: cvCategories, _size: CGSize(width: 90, height: 114), _edgeInsets: nil, _lineSpacing: nil, _interItemSpacing: nil)
        cvDataSource?.configureCell = {  (cell,item,index) in
            guard let mCell = cell as? RoundCategoryCell else { return }
            
            mCell.item = item
            guard let data = item as? Categories else { return }
            if /data.id == self.selectedIndex{
                mCell.viewBack.backgroundColor = #colorLiteral(red: 0.3527078629, green: 0.6145247817, blue: 0.6196863055, alpha: 1)
                
            }else{
                mCell.viewBack.backgroundColor = .white
            }
            
        }
        
        cvDataSource?.didSelectItem = {
            (index,item) in
            guard let data = item as? Categories else { return }
            self.selectedIndex = /data.id
            self.cvCategories.reloadData()
            self.didSelectCategory?(data.id)
        }
        
        cvCategories.delegate = cvDataSource
        cvCategories.dataSource = cvDataSource
        cvCategories.reloadData()
    }
    
}
