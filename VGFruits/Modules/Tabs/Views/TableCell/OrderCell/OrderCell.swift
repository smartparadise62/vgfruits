//
//  OrderCell.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 05/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var heightTableItems: NSLayoutConstraint!
    @IBOutlet weak var btnStatus: UIButton!
    
    @IBOutlet weak var lblConfirmationCode: UILabel!
    
    @IBOutlet weak var tblItems: UITableView!{
        didSet{
            tblItems.registerXIB(CellIdentifiers.TrackItemCell.rawValue)
        }
    }
    @IBOutlet weak var viewBack: UIView!
    
    //MARK: - Properties
    var item:Any?{
        didSet{
            guard let data = item as? OrderList else { return }
            heightTableItems.constant = CGFloat(/data.product_details?.count * 44)
            
            
            configureTable(items: data.product_details ?? [])
           
            lblConfirmationCode.text = "confirmationCode".localize + " \(/data.order_otp)"
            imgProduct.setImageKF(/data.shop_profile, placeholder: UIImage(named: "placeholder"))
            lblOrderId.text = "Order".localize + " \(/data.id)"
            var totalProductPrice = 0
                       for item in data.product_details ?? []{
                           let qty = /item.qyt
                           let price = /item.price
                           totalProductPrice += (qty * price)
                       }
            
            lblTotal.text = "€ \(/totalProductPrice)"
            lblTime.text = Utility.getTimeFromEpoch(time: Double(/data.created), format: "dd MMM YYYY 'at' hh:mm a")
            if /data.order_status == 0{
                btnStatus.setTitle("Pending Approval".localize, for: .normal)
            }else if /data.order_status == 1{
                btnStatus.setTitle("Order Accepted".localize, for: .normal)
            }else if /data.order_status == 2{
                btnStatus.setTitle("Order Cancelled".localize, for: .normal)
            }else if /data.order_status == 4{
                btnStatus.setTitle("Completed".localize, for: .normal)
            }
        }
    }
    
    
    var tblDS:TableViewDataSource?
    override func awakeFromNib() {
        super.awakeFromNib()
        Utility.dropShadow(mView: viewBack, radius: 4, color: .lightGray, size: CGSize(width: 1, height: 1))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func btnActionReorder(_ sender: Any) {
        if let data = item as? OrderList{
            let vc = Router.shared.topController()!
                   vc.showAlert(message: "Do you want to repeat this order?", title: "VGFruits", actionTitle: "Ok", ok: {
                    self.reorder()
                   }) {
                    
            }
        }
       
    }
    
    func reorder(){
        let data = self.item as? OrderList
        NotificationCenter.default.post(name: Notification.Name("JoinRoom"), object: nil, userInfo: ["userId":/data?.shop_id])
              
        UserEP.getStripeToken(amount: /data?.price * 100, order_id: /data?.id, shop_id: /data?.shop_id, application_fee_amount: 10 ,currency:"USD").request(loader:true,success: {(res) in
                  guard let mData = res as? StripeSecret else { return }
                  guard let vc = R.storyboard.sideBar.addPaymentMethodVC() else {return}
                  vc.secret = /mData.data?.secret
                  vc.stripeId = /mData.data?.stripe_id
                  vc.addressID = /data?.address_id
            vc.reorderOrderId = /data?.id
            vc.isReorder = true
                  Router.shared.pushVC(vc: vc)
                  
              }){(error) in
                  Toast.shared.showAlert(type: .apiFailure, message: /error)
              }
              
    }
    
    @IBAction func btnActionPaymentComplete(_ sender: Any) {
        
    }
    
    func configureTable(items:[ProductDetails]){
         tblDS = TableViewDataSource(items: items, tableView: tblItems, cellIdentifier: CellIdentifiers.TrackItemCell.rawValue, configureCellBlock: { (cell, item, index) in
             guard let mCell = cell as? TrackItemCell else { return }
             mCell.item = item
         }, aRowSelectedListener: { (index, item) in
             
         }, willDisplayCell: nil, viewforHeaderInSection: nil, scrollToTop: nil)
         tblItems.delegate = tblDS
         tblItems.dataSource = tblDS
         tblItems.reloadData()
     }
}
