//
//  FilterCVCell.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 11/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class FilterCVCell: UITableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var cvFilter: UICollectionView!{
        didSet{
            cvFilter.registerXIB(CellIdentifiers.FilterCell.rawValue)
        }
    }
    
    //MARK: - Properties
    var item:Any?{
        didSet{
            configureCollection()
        }
    }
    
    var selectedIndex = 0
    var cvDataSource:CollectionDataSource?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configureCollection(){
        
        cvDataSource = CollectionDataSource(_items: ["All","Over 4.5","Cost","Discount","Offers"], _identifier: CellIdentifiers.FilterCell.rawValue, _collectionView: cvFilter, _size: CGSize(width: 120, height: 34), _edgeInsets: nil, _lineSpacing: nil, _interItemSpacing: nil)
        cvDataSource?.configureCell = {  (cell,item,index) in
            guard let mCell = cell as? FilterCell else { return }
            mCell.item = item
            if self.selectedIndex == index.row{
                mCell.viewBack.backgroundColor = #colorLiteral(red: 0.3527078629, green: 0.6145247817, blue: 0.6196863055, alpha: 1)
                mCell.lblFilter.textColor = .white
            }else{
                mCell.viewBack.backgroundColor = .white
                mCell.lblFilter.textColor = .lightGray
            }
            
        }
        
        cvDataSource?.didSelectItem = {
            (index,item) in
            self.selectedIndex = index.row
            self.cvFilter.reloadData()
        }
        
        cvFilter.delegate = cvDataSource
        cvFilter.dataSource = cvDataSource
        cvFilter.reloadData()
    }
    
}
