//
//  SideMenuCell.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 05/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var lblMenu: UILabel!
    @IBOutlet weak var lblLogout: UILabel!
    
    //Properties
    var item:Any?{
        didSet{
            guard let data = item as? [String:Any] else { return }
            if data["name"] as? String == BarItems.logout.rawValue{
                imgMenu.isHidden = true
                lblMenu.isHidden = true
                lblLogout.isHidden = false
            }else{
                imgMenu.image = data["icon"] as? UIImage
                lblMenu.text = data["name"] as? String
                lblLogout.isHidden = true
            imgMenu.isHidden = false
            lblMenu.isHidden = false
           }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
