//
//  OnboardingCell.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 02/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class OnboardingCell: UICollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var imgOnboarding: UIImageView!
    
    @IBOutlet weak var vWIdth: NSLayoutConstraint!{
        didSet{
            vWIdth.constant = UIScreen.main.bounds.size.width - 120
        }
    }
    
    @IBOutlet weak var vHeight: NSLayoutConstraint!{
        didSet{
            vHeight.constant = 300
        }
    }
    //MARK: - Properties
    var item:Any?{
        didSet{
            imgOnboarding.image = item as? UIImage
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
