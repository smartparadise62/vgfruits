//
//  OnboardingVC.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 02/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class OnboardingVC: UIViewController {

    //MARK: - IBOutlets
      @IBOutlet weak var cvOnboarding: UICollectionView!{
          didSet{
              cvOnboarding.registerXIB(CellIdentifiers.OnboardingCell.rawValue)
          }
      }
      @IBOutlet weak var dot1: UIImageView!
      @IBOutlet weak var dot2: UIImageView!
      @IBOutlet weak var dot3: UIImageView!
     
    @IBOutlet weak var viewBackMargin: NSLayoutConstraint!
    
    @IBOutlet weak var btnWidth: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
      @IBOutlet weak var lblSubtitle: UILabel!
      @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var stackDots: UIStackView!
    
    @IBOutlet weak var viewBack: UIView!
    //MARK: - Properties
      var dataSource:CollectionDataSource?
      var images = [#imageLiteral(resourceName: "fruits-img-1.png"),#imageLiteral(resourceName: "salad-img.png"),#imageLiteral(resourceName: "juice-artboard-icon.png")]
      var titleArray = ["tutorial1".localize,"tutorial2".localize,"tutorial3".localize]
    
      var subtitleArray = ["sub1".localize,"sub2".localize,"sub3".localize]
      var currentIndex = 0
      var darkDot = #imageLiteral(resourceName: "circle-green-img.png")
      var lightDot = #imageLiteral(resourceName: "ligt-green.png")
      
      
      //MARK: - Life Cycle Methods
      override func viewDidLoad() {
          super.viewDidLoad()
        
        if Device.IS_IPHONE_X{
            viewBackMargin.constant = -100
        }
        
        btnNext.setTitle("next".localize, for: .normal)
        setDots(index: currentIndex)
        lblTitle.text = self.titleArray[currentIndex]
        lblSubtitle.text = self.subtitleArray[currentIndex]
      
        viewBack.layer.cornerRadius = viewBack.frame.size.width/2
        viewBack.layer.masksToBounds = true
      }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configureCollection()
        
    }
      
      func configureCollection(){
          dataSource = CollectionDataSource(_items: images, _identifier: CellIdentifiers.OnboardingCell.rawValue, _collectionView: cvOnboarding, _size: CGSize(width: UIScreen.main.bounds.size.width, height: cvOnboarding.frame.size.height), _edgeInsets: nil, _lineSpacing: nil, _interItemSpacing: nil)
          
          dataSource?.configureCell = { (cell,item,index) in
              guard let mCell = cell as? OnboardingCell else { return }

              mCell.item = item
          }
          
          dataSource?.didScroll = {
              [weak self] in
              let index = Int((self?.cvOnboarding.contentOffset.x ?? 0.0)/UIScreen.main.bounds.size.width)
              self?.lblTitle.text = self?.titleArray[index]
            self?.lblSubtitle.text = self?.subtitleArray[index]
              self?.currentIndex = index
              self?.setDots(index: index)
              if self?.currentIndex == 2{
                self?.stackDots.isHidden = true
                UIView.animate(withDuration: 0.2) {
                    self?.btnWidth.constant = UIScreen.main.bounds.size.width - 60
                    self?.view.layoutIfNeeded()
                }
                  self?.btnNext.setTitle("getStart".localize, for: .normal)
              }else{
                self?.stackDots.isHidden = false
                               UIView.animate(withDuration: 0.2) {
                                   self?.btnWidth.constant = 150
                                   self?.view.layoutIfNeeded()
                               }
                  self?.btnNext.setTitle("next".localize, for: .normal)
              }
          }
      }
      
      
      //MARK:IBAction Methods
      @IBAction func btnActionNext(_ sender: Any) {
          if currentIndex == 2{
            guard let vc = R.storyboard.authentication.loginVC() else { return }
            Router.shared.pushVC(vc: vc)
          }else{
              currentIndex += 1
              cvOnboarding.scrollRectToVisible(CGRect(x: 0, y: 0, width: CGFloat(currentIndex + 1) * UIScreen.main.bounds.size.width, height: cvOnboarding.frame.size.height), animated: true)
          }
      }
    
      func setDots(index:Int){
          dot1.image = lightDot
          dot2.image = lightDot
          dot3.image = lightDot
          
          switch index{
          case 0:
              dot1.image = darkDot
          case 1:
              dot2.image = darkDot
          case 2:
              dot3.image = darkDot
          
          default:
              break
          }
      }

}
