//
//  SignupVC.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 04/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import FirebaseAuth

class SignupVC: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tfFirstName: ACFloatingTextfield!
    @IBOutlet weak var tfCountryCode: ACFloatingTextfield!
    @IBOutlet weak var tfLastName: ACFloatingTextfield!
    @IBOutlet weak var tfEmail: ACFloatingTextfield!
    @IBOutlet weak var tfPassword: ACFloatingTextfield!
    @IBOutlet weak var whiteFirst: UIView!
    @IBOutlet weak var whiteLast: UIView!
    @IBOutlet weak var whiteEmail: UIView!
    @IBOutlet weak var whitePass: UIView!
    @IBOutlet weak var imgVwShop: UIImageView!
  
    @IBOutlet weak var tfAddress: ACFloatingTextfield!
    @IBOutlet weak var addressWhite: UIView!
    
    @IBOutlet weak var tfPhone: ACFloatingTextfield!
    
    @IBOutlet weak var phoneWhite: UIView!
    
    @IBOutlet weak var nameWidth: NSLayoutConstraint!
       
       @IBOutlet weak var lastWidth: NSLayoutConstraint!
       
       @IBOutlet weak var emailWidth: NSLayoutConstraint!
       
       @IBOutlet weak var phoneWIdth: NSLayoutConstraint!
       
       @IBOutlet weak var passwordWidth: NSLayoutConstraint!
    
    var selectedPicture:UIImage?
    
    var lat:Double = 0.0
    var long:Double = 0.0
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        if L102Language.currentAppleLanguage() == "fr"{
                   nameWidth.constant = nameWidth.constant - 15
                   lastWidth.constant = lastWidth.constant + 30
            emailWidth.constant = emailWidth.constant + 16
            phoneWIdth.constant = phoneWIdth.constant + 45
            passwordWidth.constant = passwordWidth.constant + 30
               }
        
    }
    
    func setupView(){
        whiteEmail.isHidden = true
        whitePass.isHidden = true
        whiteFirst.isHidden = true
        whiteLast.isHidden = true
        addressWhite.isHidden = true
        phoneWhite.isHidden = true
        tfFirstName.delegate = self
        tfLastName.delegate = self
        tfPassword.delegate = self
        tfEmail.delegate = self
        tfAddress.delegate = self
        tfPhone.delegate = self
    }
    
    
    
    //MARK: - IBAction Methods
    
    @IBAction func btnActionImage(_ sender: Any) {
        DispatchQueue.main.async {
                   AttachmentHandler.shared.showAttachmentActionSheet(vc: self)
                   AttachmentHandler.shared.imagePickedBlock = { [weak self] item in
                       self?.selectedPicture = item
                    self?.imgVwShop.image = item
                      
                   }
               }
    }
    
    
    @IBAction func btnActionAddress(_ sender: Any) {
        guard let vc = R.storyboard.details.addNewAddress() else { return}
        vc.delegate = self
        vc.isForAdding = false
        self.present(vc, animated: true, completion: nil)
//        let vc = SelectLocationVC(nibName: String(describing: SelectLocationVC.self), bundle: nil)
//               vc.delegate = self
//               self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func btnActionBack(_ sender: Any) {
        Router.shared.popFromInitialNav()
    }
    
    @IBAction func btnActionCountryCode(_ sender: Any) {
        guard let vc = R.storyboard.authentication.countryCodeSearchViewController() else { return }
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    
    
    @IBAction func btnActionSignup(_ sender: Any) {
        let valid = validate()
        if valid == ""{
            
           let signupData = SignupModel(firstName: /tfFirstName.text, lastName: /tfLastName.text, email: /tfEmail.text, countryCode: /tfCountryCode.text, phoneNum: /tfPhone.text, address: /tfAddress.text, password: /tfPassword.text, lat: String(lat), long: String(long))
            sendOTP(num:/tfCountryCode.text + /tfPhone.text, dialCode: /tfCountryCode.text, phoneNumberOnly: Int(/tfPhone.text) ?? 0, data: signupData)
        }else{
            Toast.shared.showAlert(type: .validationFailure, message: /valid)
        }
    }
    
    
    ///Send Otp
    func sendOTP(num:String,dialCode:String,phoneNumberOnly:Int,data:SignupModel){
        DispatchQueue.main.async {
            Loader.shared.start()
        }
           PhoneAuthProvider.provider().verifyPhoneNumber(num, uiDelegate: nil) { (verificationID, error) in
            DispatchQueue.main.async {
                     Loader.shared.stop()
                 }
               if let error = error {
                   print(error.localizedDescription)
                   Toast.shared.showAlert(type: .apiFailure, message: error.localizedDescription)
                   return
                   
               }else{
                   UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                   guard let vc = R.storyboard.authentication.verifyEmailVC() else {return}
                   vc.CountryCode = dialCode
                   vc.phoneNumber = phoneNumberOnly
                   vc.signupData = data
                
                   vc.fromScreen = .signup
                   self.pushVC(vc)
               }
           }
       }
    
    func validate()->String?{
           if  /tfFirstName.text?.isEmpty || /tfLastName.text?.isEmpty || /tfEmail.text?.isEmpty || /tfAddress.text?.isEmpty || /tfPhone.text?.isEmpty || /tfPassword.text?.isEmpty{
                         return AlertMessage.REQUIRED_EMPTY
           }else if !Utility.isValidEmail(testStr: /tfEmail.text){
               return AlertMessage.INVALID_EMAIL
           }else if !Utility.isValidPhone(value: /tfPhone.text){
               return AlertMessage.INVALID_PHONE
           }
           return AlertMessage.EMPTY_SSM
       }
    
}

extension SignupVC:UITextFieldDelegate,CountryCodeSearchDelegate,SelectLocationDelegate{
    
    func didTap(onCode detail: [AnyHashable : Any]!) {
          self.tfCountryCode.text =  (detail["dial_code"] as! String)
      }
    
    func locationSelected(address: String, city: String, state: String, country: String, zipcode: String, lat: Double, long: Double) {
                self.tfAddress.text = address
          addressWhite.isHidden = false
          self.lat = lat
          self.long = long
      }
      
   
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tfEmail{
            whiteEmail.isHidden = false
        }else if textField == tfPassword{
            whitePass.isHidden = false
        }else if textField == tfFirstName{
            whiteFirst.isHidden = false
        }else if textField == tfLastName{
            whiteLast.isHidden = false
        }else if textField == tfPhone{
            phoneWhite.isHidden = false
        }else if textField == tfAddress{
            addressWhite.isHidden = false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tfEmail{
            if /textField.text?.isEmpty{
                whiteEmail.isHidden = true
            }
        }else if textField == tfPassword{
            if /textField.text?.isEmpty{
                whitePass.isHidden = true
            }
        }else if textField == tfFirstName{
            if /textField.text?.isEmpty{
                whiteFirst.isHidden = true
            }
        }else if textField == tfLastName{
            if /textField.text?.isEmpty{
                whiteLast.isHidden = true
            }
        }else if textField == tfPhone{
            if /textField.text?.isEmpty{
                phoneWhite.isHidden = true
            }
        }else if textField == tfAddress{
            if /textField.text?.isEmpty{
                addressWhite.isHidden = true
            }
        }
    }
}
