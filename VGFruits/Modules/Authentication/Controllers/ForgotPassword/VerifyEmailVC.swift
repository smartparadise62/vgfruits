//
//  VerifyEmailVC.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 04/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit
import FirebaseAuth

class VerifyEmailVC: UIViewController {
    
    @IBOutlet weak var tf1: MyTextField!
    
    @IBOutlet weak var tf2: MyTextField!
    @IBOutlet weak var tf3: MyTextField!
    @IBOutlet weak var tf4: MyTextField!
    @IBOutlet weak var tf5: MyTextField!
    
    @IBOutlet weak var tf6: MyTextField!
    
    @IBOutlet weak var btnDone: UIButton!
    
    //Variables
       enum FromScreen {
           case forgotPssword
           case signup
       }
     var isSocialSign = false
       
       var phoneNumber:Int?
       var CountryCode:String?
       var fromScreen:FromScreen? = .signup
       var signupData:SignupModel?
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        tf1.delegate = self
        tf2.delegate = self
         tf3.delegate = self
         tf4.delegate = self
         tf5.delegate = self
         tf6.delegate = self
        
        if fromScreen == .forgotPssword{
            btnDone.setTitle("resetPassword".localize, for: .normal)
        }else{
            btnDone.setTitle("submit".localize, for: .normal)
        }
    }
    
    
    
    func validate()->String{
        if /tf1.text?.isEmpty || /tf2.text?.isEmpty || /tf3.text?.isEmpty || /tf4.text?.isEmpty || /tf5.text?.isEmpty || /tf6.text?.isEmpty{
            return AlertMessage.EMPTY_OTP
        }
        return ""
    }
    
    @IBAction func btnActionResend(_ sender: Any) {
        sendOTP(num:(CountryCode ?? "") + "\(phoneNumber ?? 0)" )
    }
    
    @IBAction func btnActionBack(_ sender: Any) {
        Router.shared.popFromInitialNav()
    }
    @IBAction func btnActionDone(_ sender: Any) {
        verifyOtpNumber()
    }
    
    //MARK:- Apis
     func verifyOtpNumber() {
       
       let valid = validate()
       if valid != ""{
         Toast.shared.showAlert(type: .apiFailure, message: valid)
       }else{
         DispatchQueue.main.async {
                  Loader.shared.start()
              }
         var verificationCode = (tf1.text ?? "") + (tf2.text ?? "") + (tf3.text ?? "")
         verificationCode.append(tf4.text!)
         verificationCode.append(tf5.text!)
         verificationCode.append(tf6.text!)
         
         let verificationID = UserDefaults.standard.string(forKey: "authVerificationID") ?? ""
         let credential = PhoneAuthProvider.provider().credential(
           withVerificationID: verificationID,
           verificationCode: verificationCode)
         
         
           Auth.auth().signIn(with: credential) { (authResult, error) in
               DispatchQueue.main.async {
                        Loader.shared.stop()
                    }
               if let error = error {
                   print(error)
                   Toast.shared.showAlert(type: .apiFailure, message: error.localizedDescription )
                   return
               }
               
               ///Append Data before proceeding
               
               if self.fromScreen == .forgotPssword{
//                   guard let vc = R.storyboard.authentication.updatePasswordViewController() else {return}
//
//                   vc.phoneNumber = self.phoneNumber
//                   vc.dialCode = self.CountryCode
//                   self.pushVC(vc)
                   
               }else{
                if self.isSocialSign{
                    self.socialSignup()
                }else{
                    self.signup()
                }
                   
//                   guard let vc = R.storyboard.authentication.uploadVideoBoomerangViewController() else {return}
//                   vc.selectedPageControlIndex = self.selectedPageControlIndex + 1
//                   vc.signUpDataDictionary = self.signUpDataDictionary
                  // self.pushVC(vc)
               }
           }
       }
       }
    
    func signup(){
        guard let model = signupData else { return }
        let token = UserDefaults.standard.getDeviceToken() ?? "12345"
        UserEP.signup(firstName: /model.firstName, lastName: /model.lastName, email: /model.email, password: /model.password, phone: /model.phoneNum, country_code: /model.countryCode, address: /model.address, latitude: /model.lat, longitude: /model.long, deviceType: "2", deviceToken: token, userType: "1").request(loader: true, success: { (res) in
        guard let data = res as? Login else {return}
        UserPreference.shared.data = data.data
        UserPreference.shared.setLoggedIn(true)
        Router.shared.setDrawer()
        }) { (error) in
            Toast.shared.showAlert(type: .apiFailure, message: /error)
        }
    }
    
    func socialSignup(){
        guard let data = signupData else { return }
        let token = UserDefaults.standard.getDeviceToken() ?? "12345"
               UserEP.socialLogin(socialId: /data.socialId, token: /data.socialToken, socialType: /data.socialType, firstName: /data.firstName, email: /data.email, phone: /data.phoneNum, phoneCode: /data.countryCode, lastName: /data.lastName, latitude: /data.lat, longitude: /data.long, address: /data.address, deviceType: "2", deviceToken: token).request(loader: true, success: { (res) in
                   guard let data = res as? Login else {return}
                   UserPreference.shared.data = data.data
                   UserPreference.shared.setLoggedIn(true)
                   Router.shared.setDrawer()
               }) { (error) in
                   Toast.shared.showAlert(type: .apiFailure, message: /error)
               }
    }
    
    func sendOTP(num:String){
      
      PhoneAuthProvider.provider().verifyPhoneNumber(num, uiDelegate: nil) { (verificationID, error) in
        if let error = error {
         
          print(error.localizedDescription)
          Toast.shared.showAlert(type: .apiFailure, message: error.localizedDescription)
          return
          
        }else{
         
          UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
        }
      }
    }
}
extension VerifyEmailVC:UITextFieldDelegate,MyTextFieldDelegate{
    
    func textFieldDidDelete(textField: MyTextField) {
          switch textField{
          case tf1:
              print("atfirst")
          
          case tf2:
              
              tf1.becomeFirstResponder()
          case tf3:
              
              tf2.becomeFirstResponder()
          case tf4:
              
              tf3.becomeFirstResponder()
            case tf5:
            
            tf4.becomeFirstResponder()
            case tf6:
            
            tf5.becomeFirstResponder()
         
          default:
              print("")
          }
      }
      
      func textFieldDidBeginEditing(_ textField: UITextField) {
          textField.text = ""
      }
      

      
      func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          if let char = string.cString(using: String.Encoding.utf8) {
              let isBackSpace = strcmp(char, "\\b")
              if (isBackSpace == -92) {
                  switch textField {
                  case tf1:
                     print("atfirst")
                  //  secondTF.becomeFirstResponder()
                  case tf2:
                      
                      tf1.becomeFirstResponder()
                  case tf3:
                      
                      tf2.becomeFirstResponder()
                  case tf4:
                      
                      tf3.becomeFirstResponder()
                case tf5:
                              
                    tf4.becomeFirstResponder()
                case tf6:
                              
                    tf5.becomeFirstResponder()
                           
                  default:
                      print("")
                  }
              }else{
                  let typeCastToStrng = textField.text as NSString?
                  let mString = typeCastToStrng?.replacingCharacters(in: range, with: string)
                  changedCharacter(text:mString!,textField:textField)
              }
          }
          return true
      }
      
      func changedCharacter(text:String,textField:UITextField){
          let textCount = text.count
         
          if textCount >= 1{
              switch textField {
              case tf1:
                  tf1.text = text
                  tf2.becomeFirstResponder()
              
              case tf2:
                  tf2.text = text
                  tf3.becomeFirstResponder()
              
              case tf3:
                  tf3.text = text
                  tf4.becomeFirstResponder()
              
              case tf4:
                  tf4.text = text
                  tf5.becomeFirstResponder()
              
              case tf5:
                tf5.text = text
                tf6.becomeFirstResponder()
            
              case tf6:
                tf6.text = text
                tf6.resignFirstResponder()
              
              default:
                  print("")
              }
          }
      }
    
}
