//
//  ForgotPassVC.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 04/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift


class ForgotPassVC: UIViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var whiteEmail: UIView!
    @IBOutlet weak var tfEmail: ACFloatingTextfield!
      @IBOutlet weak var emailWidth: NSLayoutConstraint!
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
      if L102Language.currentAppleLanguage() == "fr"{
                    emailWidth.constant = emailWidth.constant + 16
           }
    }
    
    func setupView(){
        whiteEmail.isHidden = true
        tfEmail.delegate = self
    }
    

    //MARK: - IBAction methods
    @IBAction func btnActionBack(_ sender: Any) {
        Router.shared.popFromInitialNav()
    }
    
    @IBAction func btnActionReset(_ sender: Any) {
        let valid = validate()
        if valid == ""{
            UserEP.forgotPassword(email: /tfEmail.text).request(loader: true, success: { (res) in
                guard let data = res as? AnyData else { return }
                Toast.shared.showAlert(type: .success, message: /data.message)
                Router.shared.popFromInitialNav()
            }) { (error) in
                Toast.shared.showAlert(type: .apiFailure, message: /error)
            }
        }else{
            Toast.shared.showAlert(type: .apiFailure, message: /valid)
        }
       // guard let vc = R.storyboard.authentication.verifyEmailVC() else { return }
       // Router.shared.pushVC(vc: vc)
    }
    
    func validate() -> String{
        if /tfEmail.text?.isEmpty{
            return AlertMessage.REQUIRED_EMPTY
        }else if !Utility.isValidEmail(testStr: /tfEmail.text){
            return AlertMessage.INVALID_EMAIL
        }
        return ""
    }
    
}

extension ForgotPassVC:UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tfEmail{
            whiteEmail.isHidden = false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tfEmail{
            if /textField.text?.isEmpty{
                whiteEmail.isHidden = true
            }
        }
    }
}
