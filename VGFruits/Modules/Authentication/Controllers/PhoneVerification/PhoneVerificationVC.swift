//
//  PhoneVerificationVC.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 10/07/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import FirebaseAuth


class PhoneVerificationVC: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tfPhone: ACFloatingTextfield!
    @IBOutlet weak var tfCountryCode: ACFloatingTextfield!
    @IBOutlet weak var phoneWhite: UIView!
    
    //MARK: - Properties
    var data:SignupModel?

    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView(){
          phoneWhite.isHidden = true
          tfPhone.delegate = self
      }
    
    
    //MARK: - IBAction Methods
    @IBAction func btnActionBack(_ sender: Any) {
           Router.shared.popFromInitialNav()
       }
       
       @IBAction func btnActionCountryCode(_ sender: Any) {
           guard let vc = R.storyboard.authentication.countryCodeSearchViewController() else { return }
           vc.delegate = self
           self.present(vc, animated: true, completion: nil)
       }
    
    @IBAction func btnActionSend(_ sender: Any) {
           let valid = validate()
           if valid == ""{
            data?.phoneNum = /self.tfPhone.text
            data?.countryCode = /self.tfCountryCode.text
            sendOTP(num:/tfCountryCode.text + /tfPhone.text, dialCode: /tfCountryCode.text, phoneNumberOnly: Int(/tfPhone.text) ?? 0, data: self.data!)
           }else{
               Toast.shared.showAlert(type: .validationFailure, message: /valid)
           }
       }
       
       
       ///Send Otp
       func sendOTP(num:String,dialCode:String,phoneNumberOnly:Int,data:SignupModel){
           DispatchQueue.main.async {
               Loader.shared.start()
           }
              PhoneAuthProvider.provider().verifyPhoneNumber(num, uiDelegate: nil) { (verificationID, error) in
               DispatchQueue.main.async {
                        Loader.shared.stop()
                    }
                  if let error = error {
                      print(error.localizedDescription)
                      Toast.shared.showAlert(type: .apiFailure, message: error.localizedDescription)
                      return
                      
                  }else{
                      UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                      guard let vc = R.storyboard.authentication.verifyEmailVC() else {return}
                      vc.CountryCode = dialCode
                      vc.phoneNumber = phoneNumberOnly
                      vc.signupData = data
                      vc.isSocialSign = true
                      vc.fromScreen = .signup
                      self.pushVC(vc)
                  }
              }
          }
       
       func validate()->String?{
              if   /tfPhone.text?.isEmpty {
                            return AlertMessage.REQUIRED_EMPTY
              }else if !Utility.isValidPhone(value: /tfPhone.text){
                  return AlertMessage.INVALID_PHONE
              }
              return AlertMessage.EMPTY_SSM
        }
}

extension PhoneVerificationVC:UITextFieldDelegate,CountryCodeSearchDelegate{


        func didTap(onCode detail: [AnyHashable : Any]!) {
            self.tfCountryCode.text =  (detail["dial_code"] as! String)
        }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == tfPhone{
            phoneWhite.isHidden = false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
      if textField == tfPhone{
            if /textField.text?.isEmpty{
                phoneWhite.isHidden = true
            }
        }
    }
}
