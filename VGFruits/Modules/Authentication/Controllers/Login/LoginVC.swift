//
//  LoginVC.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 04/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit
import CoreLocation
import ACFloatingTextfield_Swift

class LoginVC: UIViewController,CLLocationManagerDelegate {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tfEmail: ACFloatingTextfield!
    
    @IBOutlet weak var whitePass: UIView!
    @IBOutlet weak var whiteEmail: UIView!
    @IBOutlet weak var tfPass: ACFloatingTextfield!
    
    @IBOutlet weak var imgRememberME: UIImageView!
    
    @IBOutlet weak var passwordWidth: NSLayoutConstraint!
       
       @IBOutlet weak var emailWidth: NSLayoutConstraint!
    
    
    
    var isRemembered = false
     var locationManager:CLLocationManager!
    var address = ""
      
    var latitude = 0.0
    var longitude = 0.0
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        if L102Language.currentAppleLanguage() == "fr"{
            passwordWidth.constant = passwordWidth.constant + 24
            emailWidth.constant = emailWidth.constant + 16
        }
    }
    
    func setupView(){
        whiteEmail.isHidden = true
        whitePass.isHidden = true
        tfPass.delegate = self
        tfEmail.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
            if let manager = locationManager{
                manager.stopUpdatingLocation()
            }
            
        }
    
    //MARK: - IBAction Methods
    @IBAction func btnActionForgot(_ sender: Any) {
        guard let vc = R.storyboard.authentication.forgotPassVC() else { return}
           Router.shared.pushVC(vc: vc)
    }
    
    @IBAction func btnActionRemember(_ sender: Any) {
        isRemembered = !isRemembered
        if isRemembered{
            imgRememberME.image = #imageLiteral(resourceName: "Remeber-icon.png")
        }else{
            imgRememberME.image = #imageLiteral(resourceName: "unselect-toggle-1")
        }
    }
    
    @IBAction func btnActionLogin(_ sender: Any) {
        let valid = validate()
        if valid == ""{
            let token = UserDefaults.standard.getDeviceToken() ?? "12345"
            UserEP.login(email: /tfEmail.text, password: /tfPass.text, deviceType: "2", deviceToken: token).request(loader: true, success: { (res) in
                guard let data = res as? Login else {return}
                UserPreference.shared.data = data.data
                UserPreference.shared.setLoggedIn(true)
                Router.shared.setDrawer()
            }) { (error) in
                Toast.shared.showAlert(type: .apiFailure, message: /error)
            }
        }else{
            Toast.shared.showAlert(type: .validationFailure, message: /valid)
        }
    }
    
    @IBAction func btnActionSignup(_ sender: Any) {
        guard let vc = R.storyboard.authentication.signupVC() else { return}
        Router.shared.pushVC(vc: vc)
    }
    
    
    @IBAction func btnActionGoogle(_ sender: Any) {
      GoogleManager.sharedManager.configureLoginManager(sender: self) { (google) in
            let signupData = SignupModel(firstName: /google.fullName, lastName: "", email: /google.email, countryCode: "", phoneNum: "", address: self.address, picture: nil, password: "", lat: "\(self.latitude)", long: "\(self.longitude)", socialId: /google.userId, socialToken: /google.userId, socialType: 2)
            self.checkSocialLogin(socialData:signupData)
        }
    }
    
    
    @IBAction func btnActionFB(_ sender: Any) {
       FacebookManager.sharedManager.configureLoginManager(sender: self) { (fbData) in
                   let signupData = SignupModel(firstName: /fbData.firstName, lastName: /fbData.lastName, email: /fbData.email, countryCode: "", phoneNum: "", address: self.address, picture: nil, password: "", lat: "\(self.latitude)", long: "\(self.longitude)", socialId: /fbData.fbId, socialToken: /fbData.fbId, socialType: 1)
                   self.checkSocialLogin(socialData:signupData)
               }
    }
    
    func checkSocialLogin(socialData:SignupModel){
        UserEP.checkSocialExist(socialId: /socialData.socialId).request(loader: true, success: { (res) in
            guard let data = res as? SocialCheck else { return }
            if /data.data?.isRegister == 0{
                self.socialSignup(data:socialData)
            }else{
                self.socialLogin(data:socialData)
            }
        }) { (error) in
            Toast.shared.showAlert(type: .apiFailure, message: /error)
        }
    }
    
    func socialSignup(data:SignupModel){
      guard let vc = R.storyboard.authentication.phoneVerificationVC() else { return }
      vc.data = data
      Router.shared.pushVC(vc: vc)
    }
    
    func socialLogin(data:SignupModel){
      let token = UserDefaults.standard.getDeviceToken() ?? "12345"
      UserEP.socialLoginIn(socialId: /data.socialId, token: /data.socialToken, socialType: /data.socialType, email: /data.email,latitude: /data.lat, longitude: /data.long, address: /data.address, deviceType: "2", deviceToken: token).request(loader: true, success: { (res) in
          guard let data = res as? Login else {return}
          UserPreference.shared.data = data.data
          UserPreference.shared.setLoggedIn(true)
          Router.shared.setDrawer()
      }) { (error) in
          Toast.shared.showAlert(type: .apiFailure, message: /error)
      }
    }
    
    func validate()->String?{
              if  /tfEmail.text?.isEmpty || /tfPass.text?.isEmpty{
                  return AlertMessage.REQUIRED_EMPTY
              }else if !Utility.isValidEmail(testStr: /tfEmail.text){
                  return AlertMessage.INVALID_EMAIL
            }
              return AlertMessage.EMPTY_SSM
          }
    
}

extension LoginVC:UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tfEmail{
            whiteEmail.isHidden = false
        }else if textField == tfPass{
            whitePass.isHidden = false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tfEmail{
            if /textField.text?.isEmpty{
                whiteEmail.isHidden = true
            }
        }else if textField == tfPass{
            if /textField.text?.isEmpty{
                whitePass.isHidden = true
            }
        }
    }
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        
        let lon: Double = Double("\(pdblLongitude)")!
        
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                DispatchQueue.main.async {
                    
                    if (error != nil)
                    {
                        print("reverse geodcode fail: \(error!.localizedDescription)")
                    }
                     guard let pm = placemarks as? [CLPlacemark] else {return}
                    
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        
                        var addressString : String = ""
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                            // self.cityTF.text = pm.
                            
                        }
                        if pm.thoroughfare != nil {
                            //  addressString = addressString + pm.thoroughfare! + ", "
                        }
                        
                        if pm.locality != nil {
                            //self.city = pm.locality ?? ""
                            addressString = addressString + pm.locality! + ", "
                        }
                        if pm.administrativeArea != nil{
                           // self.state = pm.administrativeArea ?? ""
                            addressString = addressString + (pm.administrativeArea ?? "")
                        }
                        if pm.country != nil {
                          //  self.country = pm.country ?? ""
                            //   addressString = addressString + pm.country! + " "
                        }
                        if pm.postalCode != nil {
                          //  self.zip = pm.postalCode ?? ""
                            // addressString = addressString + pm.postalCode! + " "
                            
                        }
                        self.address = addressString
                        //self.lblAddress.text = self.address
                        
                    }
                }
                
        })
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {return}
        latitude = location.coordinate.latitude
        longitude = location.coordinate.longitude
        getAddressFromLatLon(pdblLatitude: latitude, withLongitude: longitude)
        locationManager.stopUpdatingLocation()
    }
}
