//
//  Signup.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 18/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import Foundation
import UIKit

struct SignupModel{
    
    var firstName:String?
    var lastName:String?
    var email:String?
    var countryCode:String?
    var phoneNum:String?
    var address:String?
    var picture:UIImage?
    var password:String?
    var lat:String?
    var long:String?
    var socialId:String?
    var socialToken:String?
    var socialType:Int?
}

class AnyData:Codable{
   
      var success : Bool?
      var code : Int?
      var message : String?
    }

