//
//  LoginData.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 18/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import Foundation

class Login:Codable{
    var success:Bool?
    var code:Int?
    var message:String?
    var data:LoginData?
    var error_message:String?

}

class LoginData:Codable{
    var authorization_key:String?
    var status : Int?
    var licence : String?
    var profile : String?
    var dob : String?
    var opening_hours : String?
    var is_online : Int?
    var phone_code : Int?
    var accept_order : Int?
    var language : String?
    var notification_on : Int?
    var service_fees : Int?
    var latitude : Double?
    var last_name : String?
    var is_free : Int?
    var id : Int?
    var taxes : Int?
    var email : String?
    var longitude : Double?
    var phone : String?
    var user_type : Int?
    var delivery_charges : Int?
    var app_category_id : Int?
    var min_order : Int?
    var card_informations : String?
    var order_notification : Int?
    var first_name : String?
    var strip_id : String?
    var address : String?
  
}

class SocialCheck:Codable{
    var success:Bool?
    var code:Int?
    var message:String?
    var data:CheckedData?
}

class CheckedData:Codable{
    var isRegister:Int?
}

