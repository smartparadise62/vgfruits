//
//  NotificationsVC.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 08/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit
import KYDrawerController

class NotificationsVC: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var tblNotifications: UITableView!{
        didSet{
            tblNotifications.registerXIB(CellIdentifiers.NotificationCell.rawValue)
        }
    }
    
    
    //MARK: - Properties
    var notificationList = [NotificationData]()
    var tblDataSource:TableViewDataSource?
    var shouldLoad = true
    var offset = 1
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        footerRefresh()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        offset = 1
        getData()
    }
    
    
    
    func footerRefresh() {
        
        let footer = self.tblNotifications.setUpFooterRefresh { [weak self] in
            
                self?.offset += 1
               self?.getData()
            
                
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                self?.tblNotifications.endFooterRefreshing()
            }
        }.SetUp { (footer) in
            footer.setText("", mode: .pullToRefresh)
            footer.setText("Loading...".localize, mode: .refreshing)
            footer.setText("".localize, mode: .noMoreData)
        }
        footer.refreshMode = .scroll
        
    }
    
    func getData(){
        UserEP.getNotifications(offset:offset,limit:20).request(loader: shouldLoad, success: { (res) in
            guard let data = res as? NotificationModel else {
                if self.offset != 1{
                    self.offset -= 1
                }
                return
                
            }
            if /data.data?.result?.count == 0{
                 if self.offset != 1{
                        self.offset -= 1
                        return
                }
                
            }
            self.notificationList = self.offset == 1 ? data.data?.result ?? [] : self.notificationList + (data.data?.result ?? [])
            self.configureTable()
            self.shouldLoad = false
        }) { (error) in
            Toast.shared.showAlert(type: .apiFailure, message: /error)
        }
    }
 
    func configureTable(){
        tblDataSource = TableViewDataSource(items: self.notificationList, tableView: tblNotifications, cellIdentifier: CellIdentifiers.NotificationCell.rawValue, configureCellBlock: { (cell, item, index) in
               guard let mCell = cell as? NotificationCell else { return }
               mCell.item = item
           }, aRowSelectedListener: { (index, item) in
               
           }, willDisplayCell: nil, viewforHeaderInSection: nil, scrollToTop: nil)
           tblNotifications.delegate = tblDataSource
           tblNotifications.dataSource = tblDataSource
           tblNotifications.reloadData()
       }
  
    @IBAction func btnActionMenu(_ sender: Any) {

        NotificationCenter.default.post(name: Notification.Name("OpenSideBar"), object: nil)
    }
    
}
