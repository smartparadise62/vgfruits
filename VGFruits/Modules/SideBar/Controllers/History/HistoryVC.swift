//
//  HistoryVC.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 03/07/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class HistoryVC: UIViewController {

      //MARK: - IBOutlets
      @IBOutlet weak var tblOrders: UITableView!{
          didSet{
              tblOrders.registerXIB(CellIdentifiers.OrderCell.rawValue)
          }
      }
      
      var orderList = [OrderList]()
      var tblDS:TableViewDataSource?
      var shouldLoad = true
    var offset = 1
      //MARK: - Life Cycle Methods
      override func viewDidLoad() {
          super.viewDidLoad()
          headerRefresh()
          footerRefresh()
        getOrders()
      }
      
      override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          
      }
    
    func footerRefresh() {
           
           let footer = self.tblOrders.setUpFooterRefresh { [weak self] in
               
               self?.offset += 1
               self?.getOrders()
                   
               DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                   self?.tblOrders.endFooterRefreshing()
               }
           }.SetUp { (footer) in
               footer.setText("", mode: .pullToRefresh)
               footer.setText("Loading...".localize, mode: .refreshing)
               footer.setText("".localize, mode: .noMoreData)
           }
           footer.refreshMode = .scroll
       }
       
       func headerRefresh() {
           let refresh = tblOrders?.setUpHeaderRefresh { [weak self] in
               
               if(/self?.orderList.count != 0) {
                   self?.orderList.removeAll()
                   self?.offset = 1
                   self?.getOrders()
              }
           DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                   self?.tblOrders?.endHeaderRefreshing()}
           }.SetUp({ (header) in
               header.setText("", mode: .pullToRefresh)
               header.setText("Loading...".localize, mode: .refreshing)
               header.setText("".localize, mode: .refreshSuccess)
           })
       }
      
      func getOrders(){
        UserEP.getOrders(status:4,offset:offset,limit:20).request(loader: shouldLoad, success: { (res) in
            guard let data = res as? OrderModel else {
                if self.offset != 1{
                                                  self.offset -= 1
                                                  
                                              }
                return }
            if /data.data?.result?.count == 0{
                 if self.offset != 1{
                                                  self.offset -= 1
                                                  return
                                              }
                
            }
            self.orderList  = self.offset == 1 ? data.data?.result ?? [] : self.orderList + (data.data?.result ?? [])
              self.configureTable()
              self.shouldLoad = false
          }) { (error) in
              Toast.shared.showAlert(type: .apiFailure, message: /error)
          }
      }
      
      func configureTable(){
          tblDS = TableViewDataSource(items: self.orderList, tableView: tblOrders, cellIdentifier: CellIdentifiers.OrderCell.rawValue, configureCellBlock: { (cell, item, index) in
              guard let mCell = cell as? OrderCell else { return }
              mCell.item = item
          }, aRowSelectedListener: { (index, item) in
              guard let vc = R.storyboard.details.trackOrderVC() else { return }
              vc.data = item as? OrderList
              Router.shared.pushVC(vc: vc)
          }, willDisplayCell: nil, viewforHeaderInSection: nil, scrollToTop: nil)
          tblOrders.delegate = tblDS
          tblOrders.dataSource = tblDS
          tblOrders.reloadData()
      }
      
     
      
      //MARK: - IBAction Methods
      @IBAction func btnActionMenu(_ sender: Any) {
        Router.shared.popFromInitialNav()
    }
}
