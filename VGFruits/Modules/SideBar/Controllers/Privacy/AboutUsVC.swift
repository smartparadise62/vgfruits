//
//  AboutUsVC.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 12/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class AboutUsVC: UIViewController {

    //MARK: - IBOutlets
    
    
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    //MARK: - IBAction Methods
    @IBAction func btnActionBack(_ sender: Any) {
        Router.shared.popFromInitialNav()
    }
    

}
