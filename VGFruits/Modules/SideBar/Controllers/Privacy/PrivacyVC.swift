//
//  PrivacyVC.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 12/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class PrivacyVC: UIViewController {

    
    //MARK: - Life Cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        //getInfo()
        
    }
    
    func getInfo(){
        UserEP.getAppInfo.request(loader: true, success: { (res) in
            
        }) { (error) in
            Toast.shared.showAlert(type: .apiFailure, message: /error)
        }
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnActionBack(_ sender: Any) {
        Router.shared.popFromInitialNav()
        
    }
    

}
