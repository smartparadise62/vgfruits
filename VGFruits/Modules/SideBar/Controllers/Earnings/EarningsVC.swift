//
//  EarningsVC.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 08/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit
import KYDrawerController

class EarningsVC: UIViewController {
    
 @IBOutlet weak var tblEarning: UITableView!{
     didSet{
         tblEarning.registerXIB(CellIdentifiers.EarningCell.rawValue)
     }
 }
    @IBOutlet weak var viewTop: UIView!
    
     var tblDataSource:TableViewDataSource?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utility.dropShadow(mView: viewTop, radius: 4, color: .lightGray, size: CGSize(width: 1, height: 1))
NotificationCenter.default.addObserver(self, selector: #selector(closeDrawer), name: Notification.Name("CloseDrawer"), object: nil)
       configureTable()
    }
    
    
    @objc func closeDrawer(){
          if let vc = self.navigationController?.parent as? KYDrawerController{
                          vc.setDrawerState(.closed, animated: true)
                      }
      }
    func configureTable(){
              tblDataSource = TableViewDataSource(items: [1,1,1,1,1,1,1], tableView: tblEarning, cellIdentifier: CellIdentifiers.EarningCell.rawValue, configureCellBlock: { (cell, item, index) in
                  guard let mCell = cell as? EarningCell else { return }
                  mCell.item = item
              }, aRowSelectedListener: { (index, item) in
                  
              }, willDisplayCell: nil, viewforHeaderInSection: nil, scrollToTop: nil)
              tblEarning.delegate = tblDataSource
              tblEarning.dataSource = tblDataSource
              tblEarning.reloadData()
          }
   
    @IBAction func btnActionMenu(_ sender: Any) {
        
        if let vc = self.navigationController?.parent as? KYDrawerController{
                         vc.setDrawerState(.opened, animated: true)
                     }
    }
    
}
