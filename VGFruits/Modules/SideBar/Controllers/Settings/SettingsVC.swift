//
//  SettingsVC.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 08/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit
import KYDrawerController


class SettingsVC: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var switchNotification: UISwitch!
    
    @IBOutlet weak var lblLang: UILabel!
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        let data = UserPreference.shared.data
       
        let enable = /data?.notification_on == 1 ? true : false
        switchNotification.setOn(enable, animated: true)
        
             if L102Language.currentAppleLanguage() == "fr"{
                 lblLang.text = "French"
             }else{
                 lblLang.text = "English"
             }
    }
    
   
    @IBAction func switchNotiChanged(_ sender: Any) {
        let shouldEnable = switchNotification.isOn
        let val = shouldEnable ? 1:0
        
        UserEP.setNotificationStatus(enable: val).request(loader: true, success: { (res) in
            guard let data = res as? Login else { return }
            UserPreference.shared.data = data.data
        }) { (error) in
            Toast.shared.showAlert(type: .apiFailure, message: /error)
        }
    }
    
    @IBAction func btnActionMenu(_ sender: Any) {
        Router.shared.popFromInitialNav()
        
    }
    
    @IBAction func btnActionEditLang(_ sender: Any) {
        let alert = UIAlertController(title: "changeLanguage".localize, message: nil, preferredStyle: .actionSheet)
             let eng = UIAlertAction(title: "English", style: .default) { (action) in
                 L102Language.setAppleLAnguageTo(lang: "en")
                 (UIApplication.shared.delegate as! AppDelegate).reloadApp()
             }
             let french = UIAlertAction(title: "French",style: .default) { (action) in
                 L102Language.setAppleLAnguageTo(lang: "fr")
                 (UIApplication.shared.delegate as! AppDelegate).reloadApp()
             }
             
             let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
             
             alert.addAction(eng)
             alert.addAction(french)
             alert.addAction(cancel)
             self.present(alert, animated: true, completion: nil)
    }
    
}
