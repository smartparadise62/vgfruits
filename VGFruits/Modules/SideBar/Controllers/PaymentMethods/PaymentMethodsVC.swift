//
//  PaymentMethodsVC.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 12/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class PaymentMethodsVC: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    @IBOutlet weak var tblPaymentMethod: UITableView!{
        didSet{
            tblPaymentMethod.registerXIB(CellIdentifiers.PaymentMethodCell.rawValue)
        }
    }
    
    
    var tblDataSource:TableViewDataSource?
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        configureTable()
    }
    
    func configureTable(){
        tblHeight.constant = 1 * 55
                 tblDataSource = TableViewDataSource(items: [1], tableView: tblPaymentMethod, cellIdentifier: CellIdentifiers.PaymentMethodCell.rawValue, configureCellBlock: { (cell, item, index) in
                     guard let mCell = cell as? PaymentMethodCell else { return }
                     mCell.item = item
                 }, aRowSelectedListener: { (index, item) in
                     
                 }, willDisplayCell: nil, viewforHeaderInSection: nil, scrollToTop: nil)
                 tblPaymentMethod.delegate = tblDataSource
                 tblPaymentMethod.dataSource = tblDataSource
                 tblPaymentMethod.reloadData()
             }
      
    
    //MARK: - IBAction Methods
    
    @IBAction func btnActionAddNew(_ sender: Any) {
        guard let vc = R.storyboard.sideBar.addPaymentMethodVC() else { return }
        Router.shared.pushVC(vc: vc)
    }
    
    @IBAction func btnActionBack(_ sender: Any) {
        Router.shared.popFromInitialNav()
    }
    
}
