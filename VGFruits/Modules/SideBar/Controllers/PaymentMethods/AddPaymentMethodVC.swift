//
//  AddPaymentMethodVC.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 12/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit
import Stripe

class AddPaymentMethodVC: UIViewController {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var viewNumber: UIView!
    @IBOutlet weak var viewCVV: UIView!
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var tfNumber: UITextField!
    @IBOutlet weak var tfExpiration: UITextField!
    @IBOutlet weak var tfCV: UITextField!
    @IBOutlet weak var imgErr0r: UIImageView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var cardTF: STPPaymentCardTextField!
    @IBOutlet weak var viewSuccess: UIView!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var lblAmt: UILabel!
    @IBOutlet weak var lblTransId: UILabel!
    //MARK: - Properties
    
    var stripeId = ""
    var secret = ""
    var cardNumber = ""
    var productIds = ""
    var addressID = 0
    var quantities = ""
    var shopId = 0
    
    
    var reorderOrderId = 0
    var isReorder = false
    
    var paymentDetails = ""
    private var previousTextFieldContent: String?
    private var previousSelection: UITextRange?
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
    }
    
    func setupView(){
        viewSuccess.isHidden = true
        viewBack.layer.cornerRadius = 15
        viewBack.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
    }
    
    
    //MARK: - IBAction Methods
    @IBAction func btnActionBack(_ sender: Any) {
        Router.shared.popFromInitialNav()
    }
    
    func validation() -> String{
        if  /tfNumber.text?.isEmpty || /tfCV.text?.isEmpty || /tfExpiration.text?.isEmpty{
            return AlertMessage.REQUIRED_EMPTY
        }else if cardNumber.count < 13{
            return AlertMessage.CARD_INVALID
        }else if !imgErr0r.isHidden || /tfExpiration.text?.count < 5{
            return AlertMessage.EXPIRY_INVALID
        }else if /tfCV.text?.count < 3{
            return AlertMessage.INVALID_CVV
        }
        return ""
    }
    
    @IBAction func btnActionMenu(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MoveToMenu"), object: nil)
               Router.shared.popFromInitialNav()
    }
    
    @IBAction func btnActionOrders(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MoveToOrders"), object: nil)
        Router.shared.popFromInitialNav()
      //  Toast.shared.showAlert(type: .success, message: /data.message)
        
    }
    
    @IBAction func btnActionSave(_ sender: Any) {
        if cardTF.isValid{
            self.view.endEditing(true)
            makePayment()
        }else{
            Toast.shared.showAlert(type: .validationFailure, message: "cardInvalid".localize)
        }
    }
    
    func makePayment(){
        Loader.shared.start()
        //STPAPIClient.shared().stripeAccount = stripeId
        let cardParams = cardTF.cardParams
       
        let paymentMethodParams = STPPaymentMethodParams(card: cardParams, billingDetails: nil, metadata: nil)
        
        let paymentIntentParams = STPPaymentIntentParams(clientSecret: secret)
        paymentIntentParams.paymentMethodParams = paymentMethodParams
        let paymentHandler = STPPaymentHandler.shared()
    
                paymentHandler.confirmPayment(withParams: paymentIntentParams, authenticationContext: self) { (status, paymentIntent, error) in
                    print(status)
                    Loader.shared.stop()
                    switch (status) {
                    case .failed:
                        print(/error?.localizedDescription)
                        Toast.shared.showAlert(type: .apiFailure, message: /error?.localizedDescription)
    
                        break
                    case .canceled:
                        Toast.shared.showAlert(type: .apiFailure, message: /error?.localizedDescription)
        
                        break
                    case .succeeded:
                        guard let intent = paymentIntent else { return }
                        let dict = ["stripe_id":/intent.stripeId,"payment_method_id":/intent.paymentMethodId,"source_id":/intent.sourceId,"stripe_description":/intent.stripeDescription,"amount":Double(truncating: intent.amount),"currency":/intent.currency] as [String : Any]
                        
                        self.paymentDetails = toJson(data: dict)
                        if self.isReorder{
                            self.reorder(intent:intent)
                        }else{
                            self.createOrder(intent:intent)
                        }
                        
                        break
                    @unknown default:
                        print("Failed")
                        break
                    }
                }
    }
    
    func createOrder(intent:STPPaymentIntent){
        UserEP.createOrder(productId: self.productIds, addressId: "\(self.addressID)", quantity: self.quantities,paymentDetails: paymentDetails,date: Int(Date().timeIntervalSince1970)).request(loader: true, success: { (res) in
            guard let data = res as? PlacedOrderModel else { return }
            CartManager.shared.deleteCart(shopId: self.shopId)
            self.viewSuccess.isHidden = false
            self.lblTransId.text = /intent.stripeId
            let amt = Double(truncating: intent.amount)
            let trucatingAmt = Double(round(amt * 100)/100)/100
            self.lblAmt.text = "\(trucatingAmt)"
        }) { (error) in
            Toast.shared.showAlert(type: .apiFailure, message: /error)
        }
    }
    
    
    func reorder(intent:STPPaymentIntent){
        UserEP.reorder(orderId:/reorderOrderId, paymentDetails: self.paymentDetails ,date:Int(Date().timeIntervalSince1970)).request(loader: true, success: { (res) in
                               self.viewSuccess.isHidden = false
                               self.lblTransId.text = /intent.stripeId
                               let amt = Double(truncating: intent.amount)
                               let trucatingAmt = Double(round(amt * 100)/100)/100
                               self.lblAmt.text = "\(trucatingAmt)"
                           }) { (error) in
                               Toast.shared.showAlert(type: .apiFailure, message: /error)
                           }
    }
}

extension AddPaymentMethodVC: STPAuthenticationContext {
    func authenticationPresentingViewController() -> UIViewController {
        return self
    }
}

extension AddPaymentMethodVC:UITextFieldDelegate{
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tfNumber{
            previousTextFieldContent = textField.text;
            previousSelection = textField.selectedTextRange;
        }
        if textField == tfExpiration{
            
            guard let currentText = textField.text as NSString? else {
                return false
            }
            let updatedText = currentText.replacingCharacters(in: range, with: string)
            
            if string == "" {
                if updatedText.count == 3 {
                    textField.text = "\(updatedText.prefix(1))"
                    return false
                }
                return true
            } else if updatedText.count == 5 {
                
                expDateValidation(dateStr: updatedText)
                return true
            } else if updatedText.count > 5 {
                
                return false
            } else if updatedText.count == 1 {
                
                if updatedText > "1" {
                    return false
                }
            } else if updatedText.count == 2 { //Prevent user to not enter month more than 12
                if updatedText > "12" {
                    return false
                }
            }
            if updatedText.count == 2 {
                textField.text = "\(updatedText)/" //This will add "/" when user enters 2nd digit of month
            } else {
                textField.text = updatedText
            }
            return false
        }
        
        if textField == tfCV{
            guard let currentText = textField.text as NSString? else {
                return false
            }
            let updatedText = currentText.replacingCharacters(in: range, with: string)
            if updatedText.count > 4{
                return false
            }
        }
        
        
        
        return true
    }
    
    func expDateValidation(dateStr:String) {
        
        let currentYear = Calendar.current.component(.year, from: Date()) % 100   // This will give you current year (i.e. if 2019 then it will be 19)
        let currentMonth = Calendar.current.component(.month, from: Date()) // This will give you current month (i.e if June then it will be 6)
        
        let enteredYear = Int(dateStr.suffix(2)) ?? 0 // get last two digit from entered string as year
        let enteredMonth = Int(dateStr.prefix(2)) ?? 0 // get first two digit from entered string as month
        print(dateStr) // This is MM/YY Entered by user
        
        if enteredYear > currentYear {
            if (1 ... 12).contains(enteredMonth) {
                print("Entered Date Is Right")
                imgErr0r.isHidden = true
            } else {
                print("Entered Date Is Wrong")
                imgErr0r.isHidden = false
            }
        } else if currentYear == enteredYear {
            if enteredMonth >= currentMonth {
                if (1 ... 12).contains(enteredMonth) {
                    imgErr0r.isHidden = true
                } else {
                    imgErr0r.isHidden = false
                }
            } else {
                imgErr0r.isHidden = false
            }
        } else {
            imgErr0r.isHidden = false
        }
        
    }
    
    @objc func reformatAsCardNumber(textField: UITextField) {
        var targetCursorPosition = 0
        if let startPosition = textField.selectedTextRange?.start {
            targetCursorPosition = textField.offset(from: textField.beginningOfDocument, to: startPosition)
        }
        
        var cardNumberWithoutSpaces = ""
        if let text = textField.text {
            cardNumberWithoutSpaces = self.removeNonDigits(string: text, andPreserveCursorPosition: &targetCursorPosition)
        }
        
        if cardNumberWithoutSpaces.count > 19 {
            textField.text = previousTextFieldContent
            textField.selectedTextRange = previousSelection
            return
        }
        
        let cardNumberWithSpaces = self.insertCreditCardSpaces(cardNumberWithoutSpaces, preserveCursorPosition: &targetCursorPosition)
        textField.text = cardNumberWithSpaces
        self.cardNumber = cardNumberWithoutSpaces
        if let targetPosition = textField.position(from: textField.beginningOfDocument, offset: targetCursorPosition) {
            textField.selectedTextRange = textField.textRange(from: targetPosition, to: targetPosition)
        }
    }
    
    func removeNonDigits(string: String, andPreserveCursorPosition cursorPosition: inout Int) -> String {
        var digitsOnlyString = ""
        let originalCursorPosition = cursorPosition
        
        for i in Swift.stride(from: 0, to: string.count, by: 1) {
            let characterToAdd = string[string.index(string.startIndex, offsetBy: i)]
            if characterToAdd >= "0" && characterToAdd <= "9" {
                digitsOnlyString.append(characterToAdd)
            }
            else if i < originalCursorPosition {
                cursorPosition -= 1
            }
        }
        
        return digitsOnlyString
    }
    
    func insertCreditCardSpaces(_ string: String, preserveCursorPosition cursorPosition: inout Int) -> String {
        // Mapping of card prefix to pattern is taken from
        // https://baymard.com/checkout-usability/credit-card-patterns
        
        // UATP cards have 4-5-6 (XXXX-XXXXX-XXXXXX) format
        let is456 = string.hasPrefix("1")
        
        // These prefixes reliably indicate either a 4-6-5 or 4-6-4 card. We treat all these
        // as 4-6-5-4 to err on the side of always letting the user type more digits.
        let is465 = [
            // Amex
            "34", "37",
            
            // Diners Club
            "300", "301", "302", "303", "304", "305", "309", "36", "38", "39"
            ].contains { string.hasPrefix($0) }
        
        // In all other cases, assume 4-4-4-4-3.
        // This won't always be correct; for instance, Maestro has 4-4-5 cards according
        // to https://baymard.com/checkout-usability/credit-card-patterns, but I don't
        // know what prefixes identify particular formats.
        let is4444 = !(is456 || is465)
        
        var stringWithAddedSpaces = ""
        let cursorPositionInSpacelessString = cursorPosition
        
        for i in 0..<string.count {
            let needs465Spacing = (is465 && (i == 4 || i == 10 || i == 15))
            let needs456Spacing = (is456 && (i == 4 || i == 9 || i == 15))
            let needs4444Spacing = (is4444 && i > 0 && (i % 4) == 0)
            
            if needs465Spacing || needs456Spacing || needs4444Spacing {
                stringWithAddedSpaces.append(" ")
                
                if i < cursorPositionInSpacelessString {
                    cursorPosition += 1
                }
            }
            
            let characterToAdd = string[string.index(string.startIndex, offsetBy:i)]
            stringWithAddedSpaces.append(characterToAdd)
        }
        
        return stringWithAddedSpaces
    }
    
    
}
