//
//  NotificationModel.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 04/07/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import Foundation

class NotificationModel:Codable{
    var success:Bool?
    var code:Int?
    var message:String?
    var data : NotificationResult?
}

class NotificationResult:Codable{
    var result:[NotificationData]?
    var pagination:Pagination?
}

class NotificationData:Codable{
      var created : Int?
      var order_id : Int?
      var id : Int?
      var text : String?
      var user_id : Int?
      var shop_id : Int?
      var profile : String?
      var name : String?
      var type : Int?
    
}
