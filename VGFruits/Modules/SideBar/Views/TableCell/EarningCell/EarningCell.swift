//
//  EarningCell.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 08/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class EarningCell: UITableViewCell {
    
    @IBOutlet weak var backView: UIView!
    
    var item:Any?{
        didSet{
            
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        Utility.dropShadow(mView: backView, radius: 4, color: .lightGray, size: CGSize(width: 1, height: 1))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
