//
//  NotificationCell.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 08/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblMessage: UILabel!
    
    @IBOutlet weak var viewback: UIView!
    
    
       var item:Any?{
           didSet{
            guard let data = item as? NotificationData else{ return }
            lblTitle.text = /data.name
            lblMessage.text = /data.text
            lblTime.text = Utility.getTimeFromEpoch(time: Double(/data.created), format: "hh:mm a")
           }
       }

       override func awakeFromNib() {
           super.awakeFromNib()
           Utility.dropShadow(mView: viewback, radius: 4, color: .lightGray, size: CGSize(width: 1, height: 1))
       }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
