//
//  FeaturedCell.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 11/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class FeaturedCell: UICollectionViewCell {
    
    @IBOutlet weak var widthBtn: NSLayoutConstraint!
    
    //MARK: - IBOutlets
    @IBOutlet weak var viewBorder: UIView!
    
    @IBOutlet weak var viewStepper: UIStackView!
    
    @IBOutlet weak var btnAddCart: UIButton!
    
    @IBOutlet weak var lblCount: UILabel!
    
    @IBOutlet weak var imgProduct: UIImageView!
    
    @IBOutlet weak var lblPrice: UILabel!
    
    @IBOutlet weak var lblProductName: UILabel!
    var stripeId = ""
    
    //MARK: - Properties
    var item:Any?{
           didSet{
            guard let data = item as? Product else { return }
            lblProductName.text = /data.name
            lblPrice.text = "€ \(/data.price)"
                       
            imgProduct.setImageKF(/data.image, placeholder: UIImage(named: "placeholder"))
           }
       }
    
    var cartItems:[CartProduct]?{
        didSet{
            guard let data = item as? Product else { return }
            let mArray = cartItems?.filter({$0.productId == /data.id})
            if /mArray?.count > 0{
                lblCount.text = "\(/mArray?.first?.productQuantity)"
                count = /mArray?.first?.productQuantity
                btnAddCart.isHidden = true
                viewStepper.isHidden = false
                viewBorder.isHidden = false
                
            }else{
                count = 0
                lblCount.text = "\(count)"
                viewStepper.isHidden = true
                viewBorder.isHidden = true
                btnAddCart.isHidden = false
            }
        }
    }
    
    var shopId = 0
    var count = 0
    var changed:(()->())?
    //MARK: - Cell Initialization Methods
    override func awakeFromNib() {
        super.awakeFromNib()
         viewStepper.isHidden = true
        viewBorder.isHidden = true
        Utility.dropShadow(mView: btnAddCart, radius: 2, color: .lightGray, size: CGSize(width: 0.5, height: 0.5))
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(anotherShopAdding), name: Notification.Name("AnotherRequest"), object: nil)
        if L102Language.currentAppleLanguage() == "fr"{
                 widthBtn.constant = widthBtn.constant + 20
        }
    }
           
    @objc func anotherShopAdding(_ notification:Notification){
        viewStepper.isHidden = true
        viewBorder.isHidden = true
        btnAddCart.isHidden = false
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    @IBAction func btnActionAdd(_ sender: Any) {
        guard let data = self.item as? Product else{return}
        count += 1
               lblCount.text = "\(count)"
               btnAddCart.isHidden = true
               viewStepper.isHidden = false
               viewBorder.isHidden = false
        let product = CartProduct(productId: /data.id, productName: /data.name, productCategory: /data.category_name, productDescription: /data.description, productPrice: Double(/data.price), productQuantity: count, productImage: /data.image, createdAt: Date())
        CartManager.shared.addProductToCart(product: product, addingShopId: shopId, stripeId: self.stripeId)
        changed?()
    }
    
    @IBAction func btnActionSub(_ sender: Any) {
        guard let data = self.item as? Product else{return}
        if count > 1{
                   count -= 1
                   lblCount.text = "\(count)"
               }else if count == 1{
                   viewStepper.isHidden = true
                   viewBorder.isHidden = true
                   btnAddCart.isHidden = false
               }
        let product = CartProduct(productId: /data.id, productName: /data.name, productCategory: /data.category_name, productDescription: /data.description, productPrice: Double(/data.price), productQuantity: count, productImage: /data.image, createdAt: Date())
        CartManager.shared.removeProductFromCart(product: product, addingShopId: shopId, stripeId: self.stripeId)
        changed?()
    }
    
    
    @IBAction func btnActionAddToCart(_ sender: Any) {
        guard let data = self.item as? Product else{return}
        count = 0
        count += 1
               lblCount.text = "\(count)"
               btnAddCart.isHidden = true
               viewStepper.isHidden = false
               viewBorder.isHidden = false
        let product = CartProduct(productId: /data.id, productName: /data.name, productCategory: /data.category_name, productDescription: /data.description, productPrice: Double(/data.price), productQuantity: count, productImage: /data.image, createdAt: Date())
        CartManager.shared.addProductToCart(product: product, addingShopId: shopId, stripeId: self.stripeId)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.changed?()
        }
        
    }
    
}
