//
//  FeaturedItemCVCell.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 11/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class FeaturedItemCVCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var cvFeatured: UICollectionView!{
        didSet{
            cvFeatured.registerXIB(CellIdentifiers.FeaturedCell.rawValue)
        }
    }
    
    //MARK: - Properties
    var shopId = 0
    var cvDataSource:CollectionDataSource?
    var stripeId = ""
    var item:Any?{
        didSet{
            guard let data = item as? [Product] else { return }
            configureCollection(items:data)
        }
    }
    var changed:(()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCollection(items:[Product]){
        
      cvDataSource = CollectionDataSource(_items: items, _identifier: CellIdentifiers.FeaturedCell.rawValue, _collectionView: cvFeatured, _size: CGSize(width: UIScreen.main.bounds.size.width / 1.6, height: 172), _edgeInsets: nil, _lineSpacing: nil, _interItemSpacing: nil)
        cvDataSource?.configureCell = {  (cell,item,index) in
            
        }
        
        cvDataSource?.willDisplay = {(cell,item,index) in
            guard let mCell = cell as? FeaturedCell else { return }
            mCell.shopId = self.shopId
            mCell.stripeId = self.stripeId
            mCell.item = item
            CartManager.shared.getCart { (cartList, id,stripe) in
                    mCell.cartItems  = cartList
            }
            mCell.changed = {
                self.changed?()
            }
        }
        
        
        cvDataSource?.didSelectItem = {
            (index,item) in
           guard let item = item as? Product else { return }
                      guard let vc = R.storyboard.details.productDetailVC() else { return }
                      vc.product = item
                      Router.shared.pushVC(vc: vc)
        }
        
        cvFeatured.delegate = cvDataSource
        cvFeatured.dataSource = cvDataSource
        cvFeatured.reloadData()
    }
    
}
