//
//  CouponCell.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 12/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class CouponCell: UITableViewCell {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var btnApply: UIButton!
    
    var fromSide = false
    var applied:((_ amt:Int,_ name:String)->())?
    //MARK: - Properties
    var item:Any?{
        didSet{
            guard let data = item as? CouponData else { return }
            lblDesc.text = "Get \(/data.discount)% off"
            lblCode.text = /data.name
            if fromSide{
                btnApply.isHidden = true
                
            }
        }
    }

    //MARK: - Cell Initialization Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnActionApply(_ sender: Any) {
        guard let data = item as? CouponData else { return }
        applied?(/data.discount,/data.name)
        
    }
}
