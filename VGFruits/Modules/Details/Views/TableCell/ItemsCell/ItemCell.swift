//
//  ItemCell.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 11/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class ItemCell: UITableViewCell {
    
    @IBOutlet weak var widthbtn: NSLayoutConstraint!
    //MARK: - IBOutlets
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var viewImgBack: UIView!
    @IBOutlet weak var viewBorder: UIView!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var viewStepper: UIStackView!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var btnAddToCart: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    
    
    //MARK: - Properties
    var item:Any?{
        didSet{
            if let data = item as? Product  {
                lblName.text = /data.name
                lblCategory.text = /data.category_name
                lblDescription.text = /data.description
                lblPrice.text = "€ \(/data.price)"
                
                imgItem.setImageKF(/data.image, placeholder: UIImage(named: "placeholder"))
            }
            
            if let data = item as? CartProduct{
                lblName.text = /data.productName
                lblCategory.text = /data.productCategory
                lblDescription.text = /data.productDescription
                lblPrice.text = "€ \(/data.productPrice)"
                lblCount.text = "\(/data.productQuantity)"
                count = /data.productQuantity
                imgItem.setImageKF(/data.productImage, placeholder: UIImage(named: "placeholder"))
            }
            
        }
    }
    
    var stripeId = ""
    var cartItems:[CartProduct]?{
        didSet{
            if let data = item as? Product  {
                let mArray = cartItems?.filter({$0.productId == /data.id})
                if /mArray?.count > 0{
                    lblCount.text = "\(/mArray?.first?.productQuantity)"
                    count = /mArray?.first?.productQuantity
                    btnAddToCart.isHidden = true
                    viewStepper.isHidden = false
                    viewBorder.isHidden = false
                    
                }else{
                    count = 0
                    lblCount.text = "\(count)"
                    viewStepper.isHidden = true
                    viewBorder.isHidden = true
                    btnAddToCart.isHidden = false
                }
            }
            
            if let data = item as? CartProduct{
                let mArray = cartItems?.filter({$0.productId == /data.productId})
                if /mArray?.count > 0{
                    lblCount.text = "\(/mArray?.first?.productQuantity)"
                    count = /mArray?.first?.productQuantity
                    btnAddToCart.isHidden = true
                    viewStepper.isHidden = false
                    viewBorder.isHidden = false
                    
                }else{
                    count = 0
                    lblCount.text = "\(count)"
                    viewStepper.isHidden = true
                    viewBorder.isHidden = true
                    btnAddToCart.isHidden = false
                }
            }
            
        }
    }
    
    var isFromCart = false
    var shopId = 0
    var count = 0
    var changed:(()->())?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewStepper.isHidden = true
        viewBorder.isHidden = true
        Utility.dropShadow(mView: btnAddToCart, radius: 2, color: .lightGray, size: CGSize(width: 0.5, height: 0.5))
        Utility.dropShadow(mView: viewImgBack, radius: 2, color: .lightGray, size: CGSize(width: 0.5, height: 0.5))
        Utility.dropShadow(mView: viewBack, radius: 2, color: .lightGray, size: CGSize(width: 0.5, height: 0.5))
        Utility.makeBorder(mView: viewStepper, radius: 5, width: 5, color: .lightGray)
        btnDelete.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(anotherShopAdding), name: Notification.Name("AnotherRequest"), object: nil)
        if L102Language.currentAppleLanguage() == "fr"{
                        widthbtn.constant = widthbtn.constant + 20
               }
    }
    
    @objc func anotherShopAdding(_ notification:Notification){
        viewStepper.isHidden = true
        viewBorder.isHidden = true
        btnAddToCart.isHidden = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    @IBAction func btnPlus(_ sender: Any) {
        if let data = self.item as? Product {
            
            count += 1
            lblCount.text = "\(count)"
            btnAddToCart.isHidden = true
            viewStepper.isHidden = false
            viewBorder.isHidden = false
            
            let product = CartProduct(productId: /data.id, productName: /data.name, productCategory: /data.category_name, productDescription: /data.description, productPrice: Double(/data.price), productQuantity: count, productImage: /data.image, createdAt: Date())
            CartManager.shared.addProductToCart(product: product, addingShopId: shopId, stripeId:self.stripeId)
            changed?()
        }
        if let data = self.item as? CartProduct{
            
            count += 1
            lblCount.text = "\(count)"
            btnAddToCart.isHidden = true
            viewStepper.isHidden = false
            viewBorder.isHidden = false
            let product = CartProduct(productId: /data.productId, productName: /data.productName, productCategory: /data.productCategory, productDescription: /data.productDescription, productPrice: Double(/data.productPrice), productQuantity: count, productImage: /data.productImage, createdAt: Date())
            CartManager.shared.addProductToCart(product: product, addingShopId: shopId, stripeId: self.stripeId)
            changed?()
        }
        
    }
    
    @IBAction func btnSub(_ sender: Any) {
        if let data = self.item as? Product  { if count > 1{
            count -= 1
            lblCount.text = "\(count)"
        }else if count == 1{
            viewStepper.isHidden = true
            viewBorder.isHidden = true
            btnAddToCart.isHidden = false
            }
            let product = CartProduct(productId: /data.id, productName: /data.name, productCategory: /data.category_name, productDescription: /data.description, productPrice: Double(/data.price), productQuantity: count, productImage: /data.image, createdAt: Date())
            CartManager.shared.removeProductFromCart(product: product, addingShopId: shopId, stripeId: self.stripeId)
            changed?()
        }
        
        if let data = self.item as? CartProduct  { if count > 1{
            count -= 1
            lblCount.text = "\(count)"
        }else if count == 1{
            viewStepper.isHidden = true
            viewBorder.isHidden = true
            btnAddToCart.isHidden = false
            }
            let product = CartProduct(productId: /data.productId, productName: /data.productName, productCategory: /data.productCategory, productDescription: /data.productDescription, productPrice: Double(/data.productPrice), productQuantity: count, productImage: /data.productImage, createdAt: Date())
            CartManager.shared.removeProductFromCart(product: product, addingShopId: shopId, stripeId: self.stripeId)
            changed?()
            
        }
        
        
    }
    
    @IBAction func btnActionAddToCart(_ sender: Any) {
        
        if let data = self.item as? Product {
            count = 0
            count += 1
            lblCount.text = "\(count)"
            btnAddToCart.isHidden = true
            viewStepper.isHidden = false
            viewBorder.isHidden = false
            
            let product = CartProduct(productId: /data.id, productName: /data.name, productCategory: /data.category_name, productDescription: /data.description, productPrice: Double(/data.price), productQuantity: count, productImage: /data.image, createdAt: Date())
            CartManager.shared.addProductToCart(product: product, addingShopId: shopId, stripeId: self.stripeId)
            changed?()
        }
        if let data = self.item as? CartProduct{
            count = 0
            count += 1
            lblCount.text = "\(count)"
            btnAddToCart.isHidden = true
            viewStepper.isHidden = false
            viewBorder.isHidden = false
            let product = CartProduct(productId: /data.productId, productName: /data.productName, productCategory: /data.productCategory, productDescription: /data.productDescription, productPrice: Double(/data.productPrice), productQuantity: count, productImage: /data.productImage, createdAt: Date())
            CartManager.shared.addProductToCart(product: product, addingShopId: shopId, stripeId: self.stripeId)
            changed?()
        }
        
        
        
    }
    
    @IBAction func btnActionDelete(_ sender: Any) {
        
    }
    
}
