//
//  AddressCell.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 10/07/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class AddressCell: UITableViewCell {
    
    @IBOutlet weak var lblAddress: UILabel!
    
    var item:Any?{
        didSet{
            guard let data = item as? AddressData else {return}
            lblAddress.text = /data.address
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
