//
//  TrackItemCell.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 12/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class TrackItemCell: UITableViewCell {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    var item:Any?{
        didSet{
            guard let data = item as? ProductDetails else { return }
            lblCount.text = "\(/data.qyt)"
            lblPrice.text = "€ \(/data.price * /data.qyt)"
            lblName.text = /data.name
        }
    }
    

    //MARK: - Cell Initialization Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
