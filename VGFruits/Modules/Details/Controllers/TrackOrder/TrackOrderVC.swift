//
//  TrackOrderVC.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 12/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class TrackOrderVC: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblETA: UILabel!
    @IBOutlet weak var tblItem: UITableView!{
        didSet{
            tblItem.registerXIB(CellIdentifiers.TrackItemCell.rawValue)
        }
    }
    
    @IBOutlet weak var tvComment: UITextView!
    
    @IBOutlet weak var viewComment: UIView!
    
    
    @IBOutlet weak var lblConfirmationCode: UILabel!
    
    
    @IBOutlet weak var lblShopName: UILabel!

    @IBOutlet weak var lblPrice: UILabel!
    
    @IBOutlet weak var lblOrderDate: UILabel!
    
    @IBOutlet weak var lblOrderNo: UILabel!
    
    
    //MARK: - Properties
    var tblDS:TableViewDataSource?
    var data:OrderList?
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        configureTable()
    }
    
    func setupView(){
        Utility.dropShadow(mView: viewBack, radius: 4, color: .lightGray, size: CGSize(width: 1, height: 1))
        
        lblShopName.text = /data?.shop_name
       // lblETA.text = Utility.eta(lat: /data?.shop_lat, long: /data?.shop_lng)
        
        lblETA.text = /data?.estimate_time + "min".localize
        lblPrice.text = "€ \(/data?.price)"
        lblOrderNo.text = "Order".localize + " " + "\(/data?.id)"
        lblOrderDate.text = Utility.getTimeFromEpoch(time: Double(/data?.created), format: "dd MMM YYYY 'at' hh:mm a")
        
        lblConfirmationCode.text = "confirmationCode".localize + " " + "\(/data?.order_otp)"
        
    }
    func configureTable(){
        tblHeight.constant = CGFloat(43 * /self.data?.product_details?.count)
        tblDS = TableViewDataSource(items: self.data?.product_details ?? [], tableView: tblItem, cellIdentifier: CellIdentifiers.TrackItemCell.rawValue, configureCellBlock: { (cell, item, index) in
            guard let mCell = cell as? TrackItemCell else { return }
            mCell.item = item
        }, aRowSelectedListener: { (index, item) in

        }, willDisplayCell: nil, viewforHeaderInSection: nil, scrollToTop: nil)
        tblItem.delegate = tblDS
        tblItem.dataSource = tblDS
        tblItem.reloadData()
    }
    //MARK: - IBAction Methods
    
    @IBAction func btnActionBack(_ sender: Any) {
        Router.shared.popFromInitialNav()
    }
    
    
    @IBAction func btnActionTrack(_ sender: Any) {
        guard let vc = R.storyboard.details.addRatingVC() else { return }
        vc.data = self.data
        Router.shared.pushVC(vc: vc)
    }
    
}
