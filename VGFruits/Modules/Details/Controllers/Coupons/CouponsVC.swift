//
//  CouponsVC.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 12/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class CouponsVC: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var viewApplyheight: NSLayoutConstraint!
    
    @IBOutlet weak var topMargin2: NSLayoutConstraint!
    @IBOutlet weak var topMargin1: NSLayoutConstraint!
    @IBOutlet weak var viewApply: UIView!
    @IBOutlet weak var tblCoupons: UITableView!{
        didSet{
            tblCoupons.registerXIB(CellIdentifiers.CouponCell.rawValue)
        }
    }
    @IBOutlet weak var tfCode: UITextField!
    
    
    var tblDS:TableViewDataSource?
    var fromSide = false
    var couponList = [CouponData]()
    var offset = 1
    
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
       
        getData()
        if fromSide{
            viewApply.isHidden = true
            viewApplyheight.constant = 0
            topMargin1.constant = 0
            topMargin2.constant = 0
        }
    }
    
    func getData(){
        UserEP.getCoupons.request(loader: true, success: { (res) in
            guard let data = res as? CouponsModel else { return }
            self.couponList = data.data ?? []
            self.configureTable()
        }) { (error) in
            Toast.shared.showAlert(type: .apiFailure, message: /error)
        }
    }
    
    func configureTable(){
        tblDS = TableViewDataSource(items: self.couponList, tableView: tblCoupons, cellIdentifier: CellIdentifiers.CouponCell.rawValue, configureCellBlock: { (cell, item, index) in
            guard let mCell = cell as? CouponCell else { return }
            mCell.fromSide = self.fromSide
            mCell.item = item
            mCell.applied = { (amt,name) in
                NotificationCenter.default.post(name: Notification.Name("DiscountAvailed"), object: nil, userInfo: ["discount":/amt,"name":name])
                Router.shared.popFromInitialNav()
            }
           }, aRowSelectedListener: { (index, item) in
            guard let data = item as? CouponData else {return }
            
           }, willDisplayCell: nil, viewforHeaderInSection: nil, scrollToTop: nil)
           tblCoupons.delegate = tblDS
           tblCoupons.dataSource = tblDS
           tblCoupons.reloadData()
       }
       
    
    //MARK: - IBAction Methods
    @IBAction func btnActionBack(_ sender: Any) {
        Router.shared.popFromInitialNav()
    }
    
    @IBAction func btnActionApply(_ sender: Any) {
        
    }
    
}
