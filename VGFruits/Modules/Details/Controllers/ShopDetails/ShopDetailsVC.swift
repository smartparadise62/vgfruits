//
//  ShopDetailsVC.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 11/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit
import CoreLocation

class ShopDetailsVC: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var imgShop: UIImageView!
    @IBOutlet weak var tblItems: UITableView!
    @IBOutlet weak var lblShopName: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblDeliveryStatus: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblOpenStatus: UILabel!
    @IBOutlet weak var lblETA: UILabel!
    
    @IBOutlet weak var lblItemCount: UILabel!
    
    @IBOutlet weak var lblExtra: UILabel!
    @IBOutlet weak var viewCart: UIView!
    @IBOutlet weak var bottomHEight: NSLayoutConstraint!
    //MARK: - Properties
    var data:ShopData?
    var products = [Product]()
    var featuredProducts = [Product]()
    var tblDS:ShopItemsDataSource?
    var searchData:SearchData?
    var imageShop = UIImage()
    var offset = 1
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
        NotificationCenter.default.addObserver(self, selector: #selector(anotherShopAdding), name: Notification.Name("AnotherRequest"), object: nil)
       NotificationCenter.default.addObserver(self, selector: #selector(itemCountUpdated), name: Notification.Name("CartUpdated"), object: nil)
        }
           
           @objc func itemCountUpdated(_ notification:Notification){
               guard let item = notification.userInfo as? [String:Any] else { return }
               let count = item["count"] as? Int
               if /count > 0 {
                lblItemCount.text = "\(/count)" + " " + "Items".localize
                viewCart.isHidden = false
                if Device.IS_IPHONE_X{
                    bottomHEight.constant = 75
                }else{
                    bottomHEight.constant = 55
                }
                
               }else {
                viewCart.isHidden = true
                bottomHEight.constant = 0
                   
               }
               
           }
    
    func setupData(){
        if let data = data{
            imgShop.setImageKF(/data.profile, placeholder: UIImage(named:"placeholder"))
            lblShopName.text = /data.first_name + " " + /data.last_name
            lblAddress.text = /data.address
            lblETA.text = eta(lat: /data.latitude, long: /data.longitude)
            lblRating.text = "\(/data.rating)"
            lblPhone.text = "phoneNo".localize + " " + "\(/data.phone_code)" + /data.phone
            lblOpenStatus.text = /data.is_online == 1 ? "open".localize : "close".localize
                   getProducts(shouldLoad: true)
                   footerRefresh()
             
        }
        
        if let data = searchData{
                   imgShop.setImageKF(/data.profile, placeholder: UIImage(named:"placeholder"))
                   lblShopName.text = /data.first_name + " " + /data.last_name
                   lblAddress.text = /data.address
                   lblETA.text = eta(lat: /data.latitude, long: /data.longitude)
                   lblRating.text = "\(/data.rating)"
                   lblPhone.text = "phoneNo".localize + " " + "\(/data.phone_code)" + /data.phone
                   lblOpenStatus.text = /data.is_online == 1 ? "open".localize : "close".localize
                          getProducts(shouldLoad: true)
                          footerRefresh()
                    
               }
        
        CartManager.shared.getCart { (list, id,stripe) in
                         let count = list.count
                         if /count > 0 {
                             self.lblItemCount.text = "\(/count)" + " " + "Items".localize
                             self.viewCart.isHidden = false
                          if Device.IS_IPHONE_X{
                             self.bottomHEight.constant = 75
                          }else{
                             self.bottomHEight.constant = 55
                          }
                         }else {
                             self.viewCart.isHidden = true
                             self.bottomHEight.constant = 0
                             
                         }
                     }
       
    }
    
    
    func footerRefresh() {
        
        let footer = self.tblItems.setUpFooterRefresh { [weak self] in
            
            self?.offset += 1
            self?.getProducts(shouldLoad:false)
                
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                self?.tblItems.endFooterRefreshing()
            }
        }.SetUp { (footer) in
            footer.setText("", mode: .pullToRefresh)
            footer.setText("Loading...".localize, mode: .refreshing)
            footer.setText("".localize, mode: .noMoreData)
        }
        footer.refreshMode = .scroll
    }
    
    
    
    func getProducts(shouldLoad:Bool){
        
        var shopId = 0
        if let data = searchData{
            if /data.type == 1{
                shopId = /data.id
            }else{
                shopId = /data.user_id
            }
        }
        if let data = self.data{
            shopId = /data.id
        }
        
        UserEP.getProducts(shopId: "\(shopId)",offset:offset,limit:20).request(loader: shouldLoad, success: { (res) in
            guard let data = res as? ProductModel else {
                if self.offset != 1{
                    self.offset -= 1
                }
                return
                
            }
            if /data.data?.result?.products?.count == 0{
                if self.offset != 1{
                        self.offset -= 1
                        return
                }
                
            }
            self.featuredProducts = data.data?.result?.featureProduct ?? []
            self.products = self.offset == 1 ? data.data?.result?.products ?? [] : self.products + (data.data?.result?.products ?? [])
            if self.featuredProducts.count > 0 {
                let featureProductList = Product()
                featureProductList.featureProduct = self.featuredProducts
                self.products.insert(featureProductList, at: 0)
            }
            self.configureTable()
        }) { (error) in
            Toast.shared.showAlert(type: .apiFailure, message: /error)
        }
    }
    
    func configureTable(){
        tblDS = ShopItemsDataSource(items: products, tableView: tblItems, cellIdentifier: CellIdentifiers.ShopCell.rawValue, configureCellBlock: { (cell, item, index) in
                        
        }, aRowSelectedListener: { (index, item) in
            
            guard let item = item as? Product else { return }
            guard let vc = R.storyboard.details.productDetailVC() else { return }
            vc.product = item
            Router.shared.pushVC(vc: vc)
            
        }, willDisplayCell: { (index, cell, item) in
            if let mCell = cell as? ItemCell{
                mCell.shopId = /self.data?.id
                mCell.stripeId = /self.data?.stripeId
                mCell.item = item
                CartManager.shared.getCart { (cartList, id,stripeId)  in
                    mCell.cartItems  = cartList
                    
                }
            }
            
            if let mCell = cell as? FeaturedItemCVCell{
                mCell.item = self.featuredProducts
                mCell.shopId = /self.data?.id
                mCell.stripeId = /self.data?.stripeId
                mCell.changed = {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        self.tblItems.reloadData()
                    }
                }
            }
            
        }, viewforHeaderInSection: nil, scrollToTop: nil)
        
        tblItems.dataSource = tblDS
        tblItems.delegate = tblDS
        tblItems.reloadData()
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnActionBack(_ sender: Any) {
        Router.shared.popFromInitialNav()
    }
    
    
    func eta(lat:Double,long:Double)->String{
        let myCoordinate = CLLocation(latitude: /UserPreference.shared.data?.latitude, longitude: /UserPreference.shared.data?.longitude)
        let shopCoordinate = CLLocation(latitude:lat , longitude: long)
        let distanceInMeters = shopCoordinate.distance(from: myCoordinate)
        let distanceInKms = distanceInMeters/1000
        let speed = 30.0 //km/hr
        let time = distanceInKms/speed //in Hour
        let timeInMinutes = time * 60
        return "\(Int(timeInMinutes))" + " " + "min".localize
    }
    
    @IBAction func btnActionGoToCart(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("GoToCart"), object: nil)
        Router.shared.popFromInitialNav()
    }
    
    
    @objc func anotherShopAdding(_ notification:Notification){
        if let data = notification.userInfo as? [String:Any] {
            let addingShopId = data["addingShopId"] as? Int
            let shopId = data["shopId"] as? Int
            let product = data["product"] as? CartProduct
            let controller = Router.shared.topController()!
            
            controller.showAlert(message: "deleteAlreadyAdded".localize, title: "VGFruits", actionTitle: "Ok".localize, ok: {
                NotificationCenter.default.post(name:Notification.Name(rawValue: "DeleteCart"), object: nil, userInfo: ["shopId":/shopId])
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    let product = CartProduct(productId: /product?.productId, productName: /product?.productName, productCategory: /product?.productCategory, productDescription: /product?.productDescription, productPrice: Double(/product?.productPrice), productQuantity: /product?.productQuantity, productImage: /product?.productImage, createdAt: Date())
                    CartManager.shared.addProductToCart(product: product, addingShopId: /addingShopId, stripeId: /self.data?.stripeId)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.tblItems.reloadData()
                    }
                    
                }
            }) {
                
            }
        }
    }
}
