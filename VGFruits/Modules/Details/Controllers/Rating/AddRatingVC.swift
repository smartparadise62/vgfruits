//
//  AddRatingVC.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 03/07/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit
import Cosmos

class AddRatingVC: UIViewController,UITextViewDelegate {
    
    //MARK: - IBOutlets
    @IBOutlet weak var lblShopName: UILabel!
    @IBOutlet weak var viewRating: CosmosView!
    @IBOutlet weak var viewComment: UIView!
    @IBOutlet weak var tvComment: UITextView!
    
    var data:OrderList?
    var responseData:OrderResponse?
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        tvComment.delegate = self
    }
    
    func setupView(){
        if let data = self.data{
            lblShopName.text = /data.shop_name
        }
        
        if let data = self.responseData{
            lblShopName.text = /data.shopName
        }
        
        
        Utility.dropShadow(mView: viewComment, radius: 3, color: .lightGray, size: CGSize(width: 1, height: 1))
        tvComment.textColor = .lightGray
    }
    
    @IBAction func btnActionSend(_ sender: Any) {
        var id = 0
        if let data = self.data{
            id = /data.shop_id
        }
        if let data = self.responseData{
            id = /data.shopId
        }
        if tvComment.text != ""{
            let rating = viewRating.rating
            let text = tvComment.text!
            UserEP.postRating(shopId: id, rating: Int(rating), comment: text).request(loader: true, success: { (res) in
                Router.shared.popFromInitialNav()
            }) { (error) in
                Toast.shared.showAlert(type: .apiFailure, message: /error)
            }
        }else{
            Toast.shared.showAlert(type: .validationFailure, message: AlertMessage.REQUIRED_EMPTY)
        }
    }
    
    @IBAction func btnActionBack(_ sender: Any) {
        Router.shared.popFromInitialNav()
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == tvComment{
            if textView.textColor == .lightGray{
                textView.text = ""
                textView.textColor = .darkGray
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == tvComment{
            if textView.text == ""{
                textView.text = "tellExperience".localize
                textView.textColor = .lightGray
            }
        }
    }
    
}
