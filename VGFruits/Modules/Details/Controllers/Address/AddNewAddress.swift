//
//  AddNewAddress.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 10/07/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import ACFloatingTextfield_Swift

class AddNewAddress: UIViewController,GMSMapViewDelegate ,CLLocationManagerDelegate{

    //MARK: - IBOutlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var tfHouse: ACFloatingTextfield!
    @IBOutlet weak var tfStreet: ACFloatingTextfield!
    @IBOutlet weak var tfCity: ACFloatingTextfield!
    @IBOutlet weak var tfPostalCode: ACFloatingTextfield!
    @IBOutlet weak var tfAddress: ACFloatingTextfield!
    
    //MARK:- Properties
    var locationManager:CLLocationManager!
    var delegate:SelectLocationDelegate!
    var coordinates:CLLocationCoordinate2D!
    var address = ""
    var city = ""
    var state = ""
    var country = ""
    var zip = ""
    
    var latitude = 0.0
    var longitude = 0.0
    
    var isForAdding = true
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        setupView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           locationManager = CLLocationManager()
           locationManager.delegate = self
           locationManager.desiredAccuracy = kCLLocationAccuracyBest
           locationManager.requestAlwaysAuthorization()
           locationManager.startUpdatingLocation()
       }
       
       override func viewWillDisappear(_ animated: Bool) {
           super.viewWillDisappear(animated)
           locationManager.stopUpdatingLocation()
       }
    
    func setupView(){
        viewBottom.layer.cornerRadius = 15
        viewBottom.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
    }
    
    
    //MARK: - IBAction Methods
    @IBAction func btnActionBack(_ sender: Any) {
        if isForAdding{
            Router.shared.popFromInitialNav()
        }else{
             self.dismiss(animated: true, completion: nil)
        }
       
    }
    
    @IBAction func btnActionSave(_ sender: Any) {
        if isForAdding{
              let valid = validate()
                    if valid == ""{
                        UserEP.addAddress(address: /tfAddress.text, street_address: /tfStreet.text, city: /tfCity.text, postCode: /tfPostalCode.text, latitude: "\(latitude)", longitude: "\(longitude)", is_default: 1).request(loader: true, success: { (res) in
            //                guard let data = res as? DefaultAddress else{ return }
                            Toast.shared.showAlert(type: .success, message: "addressAdded".localize)
                            Router.shared.popFromInitialNav()
                            
                        }) { (error) in
                            Toast.shared.showAlert(type: .apiFailure, message: /error)
                        }
                    }
        }else{
            self.delegate.locationSelected(address:/tfHouse.text + " " + /tfStreet.text + " " + /tfAddress.text, city: /tfCity.text, state: self.state, country: self.country, zipcode: /tfPostalCode.text, lat: self.latitude, long: self.longitude)
            self.dismiss(animated: true, completion: nil)
        }
      
    }
    
    func validate()->String{
        if tfAddress.text == ""{
            return "addressEmpty".localize
        }
        return ""
    }
    
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
          var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
          let lat: Double = Double("\(pdblLatitude)")!
          
          let lon: Double = Double("\(pdblLongitude)")!
          
          let ceo: CLGeocoder = CLGeocoder()
          center.latitude = lat
          center.longitude = lon
          
          let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
          
          ceo.reverseGeocodeLocation(loc, completionHandler:
              {(placemarks, error) in
                  DispatchQueue.main.async {
                      
                      if (error != nil)
                      {
                          print("reverse geodcode fail: \(error!.localizedDescription)")
                      }
                       guard let pm = placemarks as? [CLPlacemark] else {return}
                      
                      if pm.count > 0 {
                          let pm = placemarks![0]
                          
                          var addressString : String = ""
                          if pm.subLocality != nil {
                            self.tfStreet.text = pm.subLocality ?? ""
                            addressString = addressString + (pm.subLocality ?? "")  + ", "
                              // self.cityTF.text = pm.
                              
                          }
                          if pm.thoroughfare != nil {
                              //  addressString = addressString + pm.thoroughfare! + ", "
                          }
                          
                          if pm.locality != nil {
                              self.city = pm.locality ?? ""
                            self.tfCity.text = pm.locality ?? ""
                              addressString = addressString + pm.locality! + ", "
                          }
                          if pm.administrativeArea != nil{
                              self.state = pm.administrativeArea ?? ""
                              addressString = addressString + (pm.administrativeArea ?? "")
                          }
                          if pm.country != nil {
                              self.country = pm.country ?? ""
                              //   addressString = addressString + pm.country! + " "
                          }
                          if pm.postalCode != nil {
                            self.tfPostalCode.text = pm.postalCode ?? ""
                              // addressString = addressString + pm.postalCode! + " "
                              
                          }
                          self.address = addressString
                          self.tfAddress.text = self.address
                          
                      }
                  }
                  
          })
          
      }
      
      func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
          
          
          let coordinate = mapView.camera.target
          self.coordinates = coordinate
          latitude = coordinate.latitude
          longitude = coordinate.longitude
          getAddressFromLatLon(pdblLatitude: coordinate.latitude, withLongitude: coordinate.longitude)
          
      }
      
      
      func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
          guard let location = locations.last else {return}
          latitude = location.coordinate.latitude
          longitude = location.coordinate.longitude
          
          setMap(lat:latitude,long:longitude)
          getAddressFromLatLon(pdblLatitude: latitude, withLongitude: longitude)
          locationManager.stopUpdatingLocation()
      }
      
      func setMap(lat:Double,long:Double){
          let camera = GMSCameraPosition(latitude: lat, longitude: long, zoom: 16)
          mapView.animate(to: camera)
      }
}
