//
//  SelectAddressVC.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 10/07/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class SelectAddressVC: UIViewController {
    
    @IBOutlet weak var tblAddress: UITableView!{
        didSet{
            tblAddress.registerXIB(CellIdentifiers.AddressCell.rawValue)
        }
    }
    var shouldLoad = true
    var addressList = [AddressData]()
    var ds:TableViewDataSource?
    
    //MARK: - IBOutlets
    override func viewDidLoad() {
        super.viewDidLoad()
        tblAddress.tableFooterView = UIView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getAddress()
    }
    
    
    func getAddress(){
           UserEP.getAddress.request(loader: shouldLoad, success: { (res) in
               guard let data = res as? AddressModel else { return }
            self.shouldLoad = false
            self.addressList = data.data?.result ?? []
            self.configureTbl()
           }) { (error) in
               Toast.shared.showAlert(type: .apiFailure, message: /error)
           }
       }
    
    
    func configureTbl(){
        ds = TableViewDataSource(items: self.addressList, tableView: tblAddress, cellIdentifier: CellIdentifiers.AddressCell.rawValue, configureCellBlock: { (cell, item, index) in
            guard let mCell = cell as? AddressCell else { return }
            guard let data = item as? AddressData else { return }
            if /data.is_default == 1{
                mCell.accessoryType = .checkmark
            }else{
                mCell.accessoryType = .none
            }
            mCell.item = data
            
        }, aRowSelectedListener: { (index, item) in
            guard let data = item as? AddressData else { return }
            self.updateAddress(data:data)
        }, willDisplayCell: nil, viewforHeaderInSection: nil, scrollToTop: nil)
        tblAddress.delegate = ds
        tblAddress.dataSource = ds
        tblAddress.reloadData()
    }
    
    func updateAddress(data:AddressData){
        UserEP.updateAddress(isDefault: 1, addressId: /data.id).request(loader: true, success: { (res) in
            self.getAddress()
        }) { (error) in
            Toast.shared.showAlert(type: .apiFailure, message: /error)
        }
    }
    
    @IBAction func btnActionBack(_ sender: Any) {
        Router.shared.popFromInitialNav()
    }
    
    @IBAction func btnActionAddNew(_ sender: Any) {
        guard let vc = R.storyboard.details.addNewAddress() else { return}
        Router.shared.pushVC(vc: vc)
    }
    
    
}
