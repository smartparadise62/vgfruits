//
//  ProductListVC.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 05/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class ProductListVC: UIViewController {
    
   //MARK: - IBOutlets
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var cvProducts: UICollectionView!{
        didSet{
            cvProducts.registerXIB(CellIdentifiers.ProductCell.rawValue)
        }
    }
    
    var catDS:CollectionDataSource?

    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollection()
        Utility.dropShadow(mView: viewSearch, radius: 4, color: .lightGray, size: CGSize(width: 0.5, height: 0.5))
    }
    
    func configureCollection(){
        let width = UIScreen.main.bounds.size.width / 2
        catDS = CollectionDataSource(_items: [1,1,1,1,1,1], _identifier: CellIdentifiers.ProductCell.rawValue, _collectionView: cvProducts, _size: CGSize(width: width, height: width), _edgeInsets: nil, _lineSpacing: nil, _interItemSpacing: nil)
        catDS?.configureCell = {  (cell,item,index) in
            guard let mCell = cell as? ProductCell else { return }
            mCell.item = item
            
        }
        
        catDS?.didSelectItem = {
            (index,item) in
            guard let vc = R.storyboard.details.productDetailVC() else { return }
                       Router.shared.pushVC(vc: vc)
        }
        cvProducts.delegate = catDS
        cvProducts.dataSource = catDS
        cvProducts.reloadData()
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnActionBack(_ sender: Any) {
        Router.shared.popFromInitialNav()
    }
}
