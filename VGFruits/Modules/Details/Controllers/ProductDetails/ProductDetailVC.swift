//
//  ProductDetailVC.swift
//  VG-Farmer
//
//  Created by Pankaj Sharma on 05/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class ProductDetailVC: UIViewController {

    
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    
    @IBOutlet weak var lblQuantity: UILabel!
    
    
    @IBOutlet weak var lblDesc: UILabel!
    
    @IBOutlet weak var lblPrice: UILabel!
    
    var product:Product?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        productName.text = /product?.name
        imgProduct.setImageKF(/product?.image, placeholder: UIImage(named:"placeholder"))
        lblQuantity.text = "inStock".localize + "\(/product?.stock) KG"
        lblDesc.text = /product?.description
        lblPrice.text = "\(/product?.price)" + "perUnit".localize
        
    }
    
    
    
    @IBAction func btnActionBack(_ sender: Any) {
        Router.shared.popFromInitialNav()
    }
    

}
