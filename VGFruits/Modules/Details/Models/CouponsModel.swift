//
//  CouponsModel.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 04/07/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import Foundation

class CouponsModel: Codable {
    var success : Bool?
    var code : Int?
    var message : String?
    var data : [CouponData]?
}
      
class CouponData:Codable{
    var end_time:Int?
    var status:Int?
    var id:Int?
    var price:Int?
    var start_time:Int?
    var discount:Int?
    var created:Int?
    var total_apply:Int?
    var modified: Int?
    var name:String?
    var is_free:Int?
}
     
