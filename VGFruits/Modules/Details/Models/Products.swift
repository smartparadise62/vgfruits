//
//  Products.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 19/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import Foundation


class ProductModel:Codable{
    var success:Bool?
    var code:Int?
    var message:String?
    var data:ProductResult?
    var error_message:String?
}

class ProductResult:Codable{
    var result:Result?
    var pagination:Pagination?
}

class Result:Codable{
    var featureProduct:[Product]?
    var products:[Product]?
}


class Product:Codable{
    var description : String?
    var modified : Int?
    var shop_name : String?
    var status : Int?
    var address : String?
    var profile : String?
    var category_name : String?
    var user_id : Int?
    var image : String?
    var sub_category_id : Int?
    var stock : Int?
    var is_fav : Int?
    var service_fees : Int?
    var name : String?
    var id : Int?
    var taxes : Int?
    var delivery_charges : Int?
    var price : Int?
    var category_id : Int?
    var rating : Int?
    var created : Int?
    var is_feature : Int?
    var featureProduct:[Product]?
    var qyt:Int?
    
    
    init() {
        
    }
}
