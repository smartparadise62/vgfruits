//
//  UserEP.swift
//  MEX
//
//  Created by Pankaj Sharma on 01/04/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import Foundation
import Moya

enum UserEP{
    case checkSocialExist(socialId:String)
     case signup(firstName:String,lastName:String,email:String,password:String,phone:String,country_code:String,address:String,latitude:String,longitude:String,deviceType:String,deviceToken:String,userType:String)
    case login(email:String,password:String,deviceType:String,deviceToken:String)
    case forgotPassword(email:String)
    case socialLogin(socialId:String,token:String,socialType:Int,firstName:String,email:String,phone:String,phoneCode:String,lastName:String,latitude:String,longitude:String,address:String,deviceType:String,deviceToken:String)
    case socialLoginIn(socialId:String,token:String,socialType:Int,email:String,latitude:String,longitude:String,address:String,deviceType:String,deviceToken:String)
    case logout
    case editProfile(firstName:String,lastName:String,address:String,lat:String,long:String,profile:UIImage?)
    case setNotificationStatus(enable:Int)
    case getCategory(offset:Int,limit:Int,search:String?)
    case addProduct(name:String,categoryId:String,price:String,stock:String,description:String,image:UIImage)
    case updateProduct(productId:String,name:String,categoryId:String,price:String,stock:String,description:String,image:UIImage?)
    case deleteProduct(productId:String)
    case getProducts(shopId:String,offset:Int,limit:Int)
    case getRatings(shopId:String)
    case postRating(shopId:Int,rating:Int,comment:String)
    case getEarning
    case getNotifications(offset:Int,limit:Int)
    case getAddress
    case getDefaultAddress
    case updateAddress(isDefault:Int,addressId:Int)
    case addAddress(address:String,street_address:String,city:String,postCode:String,latitude:String,longitude:String,is_default:Int)
    case getHome(latitude:String,longitude:String)
    
    case createOrder(productId:String,addressId:String,quantity:String,paymentDetails:String,date:Int)
    case getOrders(status:Int,offset:Int,limit:Int)
    case reorder(orderId:Int,paymentDetails:String,date:Int)
    case getCoupons
    case getStripeToken(amount:Int,order_id:Int,shop_id:Int,application_fee_amount:Int,currency:String)
    
    case getAppInfo
    case getShops(offset:Int,limit:Int,lat:String,long:String,categoryId:Int?,search:String?,distance:Int)
    
    case searchHome(offset:Int,limit:Int,lat:String,long:String,search:String?,category:Int?,distance:Int)
}

extension UserEP:TargetType{
    var baseURL: URL {
        switch self{
        default: return URL(string: APIConstant.basePath)!
        }
        
    }
    
    var path: String {
        switch self {
        case .signup: return APIConstant.signup
        case .login: return APIConstant.login
        case .checkSocialExist: return APIConstant.checkSocial
        case .socialLogin,.socialLoginIn: return APIConstant.socialSignin
        case .forgotPassword: return APIConstant.forgotPassword
        case .logout : return APIConstant.logout
        case .editProfile,.setNotificationStatus: return APIConstant.editProfile
        case .getCategory: return APIConstant.category
        case .getShops: return APIConstant.getShops
        case .searchHome: return APIConstant.searchHome
        case .addProduct: return APIConstant.addProduct
        case .getProducts(let shopId,_,_): return APIConstant.getProduct + "/" + shopId
        case .updateProduct: return APIConstant.updateProduct
        case .deleteProduct: return APIConstant.deleteProduct
        case .getRatings: return APIConstant.getRating
        case .postRating: return APIConstant.postRating
        case .getNotifications: return APIConstant.getNotifications
        case .getEarning: return APIConstant.getEarnings
        case .getHome: return APIConstant.getHome
        case .getAddress: return APIConstant.getAddresses
        case .getDefaultAddress: return APIConstant.getDefaultAddress
        case .addAddress,.updateAddress: return APIConstant.addAddress
        case .createOrder: return APIConstant.createOrder
        case .getOrders: return APIConstant.getOrders
        case .reorder: return APIConstant.repeatOrder
        case .getStripeToken: return APIConstant.getStripeSecret
        case .getCoupons: return APIConstant.getCoupons
        case .getAppInfo: return APIConstant.appInfo
        }
    }
    
    var method: Moya.Method {
        switch self{
        case .editProfile,.setNotificationStatus,.updateProduct,.updateAddress: return .put
        case .getCategory,.getProducts,.getRatings,.getEarning,.getNotifications,.getHome,.getAddress,.getDefaultAddress,.getOrders, .getCoupons,.getAppInfo,.getShops,.searchHome: return .get
        case .deleteProduct: return .delete
        default: return .post
        }
    }
    
    var sampleData: Data {
        return Data("No Data found".utf8)
    }
    
    var task: Task {
        
        switch self {
        case .signup,.addProduct,.updateProduct:
            return .uploadCompositeMultipart(multipartBody ?? [], urlParameters: ["lang":/APIConstant.currentLanguage])
            
        case .editProfile:
            return .uploadMultipart(self.multipartBody ?? [])
            
        case .getCategory,.getProducts,.getHome,.getAddress,.getDefaultAddress,.getOrders,.getRatings,.getNotifications,.getEarning,.getCoupons,.getAppInfo,.getShops,.searchHome:
            
            return .requestParameters(parameters:self.parameters, encoding: URLEncoding.default)
            
        default:
            return .requestCompositeParameters(bodyParameters: self.parameters, bodyEncoding: JSONEncoding.default, urlParameters: ["lang":/APIConstant.currentLanguage])
        }
    }
    
    var headers: [String : String]? {
        switch self{
       
        case .signup,.login,.socialLogin,.socialLoginIn,.forgotPassword,.getCategory,.getRatings,.getHome,.checkSocialExist,.getShops: return nil
            
        default:
            return AuthHeader.makeAuth()
        }
        
    }
    
    var parameters:[String:Any]{
        switch self{
        case .signup(let firstName,let lastName,let email,let password,let phone,let country_code,let address,let latitude,let longitude,let deviceType,let deviceToken,let userType):
        
         return ["first_name":firstName,"last_name":lastName,"email":email,"password":password,"phone":phone,"phone_code":country_code,"address":address,"latitude":latitude,"longitude":longitude,"device_type":deviceType,"device_token":deviceToken,"user_type":userType]
            
        case .login(let email,let password,let deviceType,let deviceToken):
            return ["email":email,"password":password,"device_type":deviceType,"device_token":deviceToken,"user_type":1]
        
        case .forgotPassword(let email):
            return ["email":email]
            
        case .getProducts(_,let offset,let limit):
            return ["offset":offset,"limit":limit]
            
        case .editProfile(let firstName,let lastName,let address,let lat,let long,_):
            return ["first_name":firstName,"last_name":lastName,"address":address,"latitude":lat,"longitude":long]
        case .setNotificationStatus(let enable):
            return ["notification_on":enable]
            
        case .getNotifications(let offset,let limit):
            return ["offset":offset,"limit":limit]
            
        case .getCategory(let offset,let limit,let search):
            
            if let srch = search{
                return ["offset":offset,"limit":limit,"lang":/APIConstant.currentLanguage,"search":srch]
            }else{
                return ["offset":offset,"limit":limit,"lang":/APIConstant.currentLanguage]
            }
            
        case .getShops(let offset,let limit,let lat,let long,let categoryId,let search,let distance):
            if let category = categoryId{
                return ["lang":/APIConstant.currentLanguage,"offset":offset,"limit":limit,"latitude":lat,"longitude":long,"category_id":category,"radius":distance]
            }
            if let srch = search{
                return ["lang":/APIConstant.currentLanguage,"offset":offset,"limit":limit,"latitude":lat,"longitude":long,"search":srch,"radius":distance]
            }
            return ["lang":/APIConstant.currentLanguage,"latitude":lat,"longitude":long,"offset":offset,"limit":limit,"radius":distance]
            
        case .searchHome(let offset,let limit,let lat,let long,let search,let category,let distance):
            
            if let cat = category{
                return ["lang":/APIConstant.currentLanguage,"offset":offset,"limit":limit,"latitude":lat,"longitude":long,"category_id":cat,"radius":distance]
            }
            
            if let srch = search{
                return ["lang":/APIConstant.currentLanguage,"offset":offset,"limit":limit,"latitude":lat,"longitude":long,"search":srch,"radius":distance]
            }
            
            return ["lang":/APIConstant.currentLanguage,"offset":offset,"limit":limit,"latitude":lat,"longitude":long,"radius":distance]
        
            
        case .addProduct(let name,let categoryId,let price,let stock,let description,_):
            return ["name":name,"category_id":categoryId,"price":price,"stock":stock,"description":description]

        case .updateProduct(let productId,let name,let categoryId,let price,let stock,let description,_):

            return ["product_id":productId,"name":name,"category_id":categoryId,"price":price,"stock":stock,"description":description]
            
        case .getHome(let latitude,let longitude):
            return ["lang":/APIConstant.currentLanguage,"latitude":latitude,"longitude":longitude]
        case .getAddress,.getDefaultAddress,.getAppInfo:
            return ["lang":/APIConstant.currentLanguage]
        case .addAddress(let address,let street_address,let city,let postCode,let latitude ,let longitude,let is_default):
            return ["address":address,"street_address":street_address,"city":city,"post_code":postCode,"latitude":latitude,"longitude":longitude,"is_default":is_default]
            
        case .createOrder(let productId,let addressId,let quantity,let details,let date):
            return ["product_id":productId,"address_id":addressId,"quantity":quantity,"payment_details":details,"order_date":date]
        case .getOrders(let status,let offset,let limit):
            return ["lang":/APIConstant.currentLanguage,"order_status":status,"offset":offset,"limit":limit]
        case .reorder(let orderId,let details,let date):
            return ["order_id":orderId,"payment_details":details,"order_date":date]
        case .getRatings(let shopId):
            return ["shop_id":shopId]
            
        case .postRating(let shopId,let rating,let comment):
            return ["shop_id":shopId,"rating":rating,"comment":comment]
        case .getStripeToken(let amount,let order_id,let shop_stripe_id, let application_fee_amount,let currency):
            return ["amount":amount,"order_id":order_id,"shop_id":shop_stripe_id,"application_fee_amount":application_fee_amount,"currency":currency]
        case .updateAddress(let isDefault,let addressId):
            return ["is_default":isDefault,"address_id":addressId]
        case .socialLogin(let socialId,let token,let socialType,let firstName,let email,let phone,let phoneCode,let lastname,let latitude,let longitude,let address, let deviceType,let deviceToken):
            return ["social_id":socialId,"social_token":token,"soical_type":socialType,"first_name":firstName,"email":email,"phone":phone,"phone_code":phoneCode,"last_name":lastname,"latitude":latitude,"longitude":longitude,"address":address,"device_type":deviceType,"device_token":deviceToken,"user_type":1]
        
        case .socialLoginIn(let socialId,let token,let socialType,let email,let latitude,let longitude,let address, let deviceType,let deviceToken):
                       return ["social_id":socialId,"social_token":token,"soical_type":socialType,"email":email,"latitude":latitude,"longitude":longitude,"address":address,"device_type":deviceType,"device_token":deviceToken,"user_type":1]
        
        case .checkSocialExist(let socialId):
            return ["social_id":socialId]
            
        
         default:
            return [:]
        }
    }
    
    var multipartBody: [MultipartFormData]? {
        
        switch self {
            
        case .signup:
            var multipartData = [MultipartFormData]()
            
            parameters.forEach({ (key, value) in
                let tempValue = "\(value)"
                let data = tempValue.data(using: String.Encoding.utf8) ?? Data()
                multipartData.append(MultipartFormData.init(provider: .data(data), name: key))
            })
            
            return multipartData
            
        case .editProfile(_, _, _, _, _,let profile):
            var multipartData = [MultipartFormData]()
            
            
            if let profileImg = profile{
                let picData = profileImg.jpegData(compressionQuality: 0.3) ?? Data()
                multipartData.append(MultipartFormData.init(provider: .data(picData), name: "profile", fileName: "image.jpg", mimeType: "image/jpeg"))
            }
            
            
            
            parameters.forEach({ (key, value) in
                let tempValue = "\(value)"
                let data = tempValue.data(using: String.Encoding.utf8) ?? Data()
                multipartData.append(MultipartFormData.init(provider: .data(data), name: key))
            })
            
            return multipartData
            
            case .addProduct(_, _, _, _, _,let image):
            var multipartData = [MultipartFormData]()
            
                let picData = image.jpegData(compressionQuality: 0.3) ?? Data()
                multipartData.append(MultipartFormData.init(provider: .data(picData), name: "image", fileName: "image.jpg", mimeType: "image/jpeg"))
            
            parameters.forEach({ (key, value) in
                let tempValue = "\(value)"
                let data = tempValue.data(using: String.Encoding.utf8) ?? Data()
                multipartData.append(MultipartFormData.init(provider: .data(data), name: key))
            })
            
            return multipartData
            
            case .updateProduct(_,_, _, _, _, _,let image):
            var multipartData = [MultipartFormData]()
            
            
            if let image = image{
                let picData = image.jpegData(compressionQuality: 0.3) ?? Data()
                               multipartData.append(MultipartFormData.init(provider: .data(picData), name: "image", fileName: "image.jpg", mimeType: "image/jpeg"))
                           
                           
                           
            }
               
            
            parameters.forEach({ (key, value) in
                let tempValue = "\(value)"
                let data = tempValue.data(using: String.Encoding.utf8) ?? Data()
                multipartData.append(MultipartFormData.init(provider: .data(data), name: key))
            })
            
            return multipartData
            
        default: return nil
        }
        
    }
    
}

