//  MEX
//
//  Created by Pankaj Sharma on 03/04/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import Foundation
import Moya

extension TargetType {
    
    func parseModel(data: Data) -> Any? {
        
        switch self {
            
        case is UserEP:
            let endpoint = self as! UserEP
            switch endpoint {
            case .login,.signup,.editProfile,.setNotificationStatus,.socialLogin,.socialLoginIn:
                return JSONHelper<Login>().getCodableModel(data: data)
           
            case .forgotPassword:
                return JSONHelper<AnyData>().getCodableModel(data: data)
                
            case .getCategory:
                return JSONHelper<CategoryModel>().getCodableModel(data: data)
                
            case .getProducts:
                return JSONHelper<ProductModel>().getCodableModel(data: data)
                
            case .getHome:
                return JSONHelper<HomeModel>().getCodableModel(data: data)
                
            case .getAddress:
                return JSONHelper<AddressModel>().getCodableModel(data: data)
                
            case .getDefaultAddress,.addAddress:
                return JSONHelper<DefaultAddress>().getCodableModel(data: data)
                
            case .createOrder:
                return JSONHelper<PlacedOrderModel>().getCodableModel(data: data)
                
            case .getOrders:
                return JSONHelper<OrderModel>().getCodableModel(data:data)
                
            case .getCoupons:
                return JSONHelper<CouponsModel>().getCodableModel(data: data)
                
            case .getNotifications:
                return JSONHelper<NotificationModel>().getCodableModel(data: data)
            
            case .getStripeToken:
                return JSONHelper<StripeSecret>().getCodableModel(data: data)
            
            case .checkSocialExist:
                return JSONHelper<SocialCheck>().getCodableModel(data:data)
                
            case .getShops:
                return JSONHelper<ShopModel>().getCodableModel(data:data)
                
            case .searchHome:
                return JSONHelper<SearchModel>().getCodableModel(data:data)
            default:
                break
            }
        default:
            return nil
        }
        
        return nil
    }
}
