//  MEX
//
//  Created by Pankaj Sharma on 03/04/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//


import Foundation
import SocketIO


class SocketHandler{

    //static let testUrl = "http://3.17.254.50:4000"
      static let testUrl = "http://18.224.76.247:4000"
      
    let manager = SocketManager(socketURL: URL(string:SocketHandler.testUrl)!, config:[.log(true), .forcePolling(true),.forceNew(true)])
    let socket:SocketIOClient?

    var paramString = Int()
    var queryString = String()
    
    
    
    
    init(){
        socket = manager.defaultSocket
        NotificationCenter.default.addObserver(self, selector: #selector(joinRoom), name: Notification.Name(rawValue: "JoinRoom"), object: nil)
        self.setSocketEvents()
    }
    
    private func setSocketEvents(){
        guard let socket = self.socket else { return }
        
        socket.once(clientEvent: .connect) { (data, ack) in
            print("socket connected!")
            socket.emit(self.queryString,self.paramString)
        }
        
        socket.on(clientEvent: .disconnect) { (data, ack) in
            print("socket Disconnected")
            self.socketDisconnect()
        }
        
        socket.on("connected") { (data, ack) in
            print(data,ack)
            
        }
        
        socket.on("disconnect") { (data, ack) in
            print(data,ack)
        
        }
        

        socket.on("orderAccept"){ (data,ack) in
            let dict = data.first as! [String:Any]
            print("response array:" ,dict)
            NotificationCenter.default.post(name: Notification.Name(rawValue: "IncomingResponse"), object: nil)
            self.showResponse(data:dict)
            
        }
        
        
    }
    
    @objc func joinRoom(_ notification:Notification){
        if let dict = notification.userInfo as? [String:Any] {
            let id = dict["userId"] as? Int
            connectUser(params: /id)
        }
    }
    
    func connectUser(params:Int){
       
        
        self.queryString = "connected"
        self.paramString = params
        self.socket?.connect()
    }
    
//    func connectSocket(){
//         self.socket?.connect()
//    }
  
    
    func newOrder(params:[String:Any]){
        let parameters = self.convertDictToJsonString(params: params)
        guard let param = parameters else { return }
        self.socket?.emit("newOrder", params)
    }
    
  
 
    func disconnect(params:Int){
           self.socket?.emit("disconnect",params)
       }
    
    func socketDisconnect(){
        self.socket?.disconnect()
    }
    
    func convertDictToJsonString(params:[String:Any])->String?{
        do{
            let jsonData = try JSONSerialization.data(withJSONObject: params, options: .init(rawValue: 0))
            let jsonString = String(data: jsonData, encoding: .ascii)
            print(jsonString)
            return jsonString
        }catch{
            print("error while convert dict to json")
        }
        return nil
    }
    
    func showResponse(data:[String:Any]){
        let orderId = data["id"] as? Int
        let orderStatus = data["order_status"] as? Int
        let rejectMessage = data["reject_message"] as? String
        let shopId = data["shop_id"] as? Int
        let estimatedTime = data["estimate_time"] as? Int
        let shopName = data["shop_name"] as? String
        let response = OrderResponse(orderID: orderId, orderStatus: orderStatus, rejectMessage: rejectMessage, shopId: shopId, estimatedTime: estimatedTime, shopName: shopName)
        let view = NewResponseView(frame: (Router.shared.topController()?.view.bounds)!, data: response)
        Router.shared.topController()?.view.addSubview(view)
        Router.shared.topController()?.view.bringSubviewToFront(view)
    }
   
}

struct OrderResponse{
    var orderID:Int?
    var orderStatus:Int?
    var rejectMessage:String?
    var shopId:Int?
    var estimatedTime:Int?
    var shopName:String?
}
