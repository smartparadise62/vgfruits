//  MEX
//
//  Created by Pankaj Sharma on 03/04/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

internal struct APIConstant{
    
   // static let testUrl = "http://3.17.254.50:4000/"
    static let testUrl = "http://18.224.76.247:4000/"
    
    static let basePath = APIConstant.testUrl + "apis/v1/"
        
    //Common APIS
    static let signup = "user"
    static let login = "user/login"
    static let forgotPassword = "forgot-password"
    static let logout = "logout"
    static let socialSignin = "user/soical-login"
    static let checkSocial = "user/soical-check"
    static let editProfile = "user/edit"
    static let category = "category"
    static let addProduct = "products"
    static let getProduct = "shop/product"
    static let searchHome = "shop/search"
    static let getShops = "shop/listing"
    static let updateProduct = "products"
    static let deleteProduct = "products"
    static let getRating = "rating"
    static let postRating = "rating"
    static let getNotifications = "notifications"
    static let getEarnings = "earning"
    static let getAddresses = "user/address"
    static let getDefaultAddress = "user/address/default"
    static let addAddress = "user/address"
    static let createOrder = "order"
    static let getOrders = "order"
    static let getHome = "home"
    static let getCoupons = "coupens"
    static let getStripeSecret = "stripe-secert-key"
    static let repeatOrder = "order/repeat"
    static let appInfo = "app-information"
    
    
    static var currentLanguage:String?{
        if L102Language.currentAppleLanguage() == "fr"{
            return "fr"
        }else{
            return "en"
        }
    }
}



class AuthHeader{
    
    static func makeAuth()->[String:String]{

        let auth = ["authorization_key":/UserPreference.shared.data?.authorization_key]
          
            return auth
     }
}

class UserHeaders{
    
 static func makeAuth()->[String:String]{
         let username = "mex"
         let password = "mexapp&^GR$%@VIBD(Y#($"
         let loginString = String(format: "%@:%@", username, password)
         let loginData = loginString.data(using: String.Encoding.utf8)!
         let base64LoginString = loginData.base64EncodedString()
    
    let auth = ["security_key":"JHNlcnZpY2VAdmFsZXlvdUBwcm92aWRlciQ=","auth_key":""]
    
        return auth
     }
}
