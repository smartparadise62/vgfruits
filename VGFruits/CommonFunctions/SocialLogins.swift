//
//  SocialLogins.swift
//  SocialLoginsModule
//
//  Created by Gurpal Rajput on 04/06/20.
//  Copyright © 2020 Gurpal Rajput. All rights reserved.
//

//README:-
// 1. Add keys in info.plist

import UIKit
import GoogleSignIn
import FacebookLogin
import FacebookCore
import AuthenticationServices
import WebKit
import Alamofire


typealias FacebookCallback = (_ facebook : Facebook) -> ()
typealias GoogleCallback = (_ google : Google) -> ()
typealias AppleCallback = (_ apple : Apple,_ credentialState:ASAuthorizationAppleIDProvider.CredentialState) -> ()
typealias InstagramCallback = (_ instagramToken : String) -> ()
typealias LinkedInCallback = (_ linkedInEmail : String) -> ()


//MARK:- Facebook Login Manager
class FacebookManager: NSObject {
    
    var viewController : UIViewController?
    let permissions = ["public_profile","email"]
    var facebookCallback : FacebookCallback?
    let loginManager = LoginManager()
    static let sharedManager: FacebookManager = { FacebookManager() }()
    
    override init() {
        super.init()
    }
    
    func configureLoginManager(sender : UIViewController, success : @escaping FacebookCallback){
        
        facebookCallback = success
        self.viewController = sender
        loginManager.logOut()
        loginManager.logIn(permissions: permissions, from: viewController) { (result, error) in
            weak var weakSelf = self
            if let err = error {
                print(err.localizedDescription)
            }else if result?.isCancelled == true{
                print("Cancelled")
            }else{
                weakSelf?.sendGraphRequest()
            }
        }
    }
    
    func logout() {
        loginManager.logOut()
    }
    
    func sendGraphRequest(){
        GraphRequest(graphPath: "me", parameters: ["fields":"first_name,last_name,picture.type(large),email"]).start { (connection, result, error) in
            
            if let err = error {
                print(err.localizedDescription)
            }else if let block = self.facebookCallback {
                
                let fbProfile = Facebook(result: result)
                block(fbProfile)
            }
        }
    }
}

//MARK:- Google Manager
class GoogleManager: NSObject {
    
    var viewController : UIViewController?
    var googleCallback : GoogleCallback?
    static let sharedManager: GoogleManager = { GoogleManager() }()
    
    override init() {
        super.init()
    }
    
    func configureLoginManager(sender : UIViewController, success : @escaping GoogleCallback){
        googleCallback = success
        self.viewController = sender
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().presentingViewController = sender
        GIDSignIn.sharedInstance().signIn()
    }
    
    func googleSetup(googleKey:String) {
        GIDSignIn.sharedInstance()?.clientID = googleKey
    }
    
    ///Google Logout
    func googleLogout() {
        GIDSignIn.sharedInstance().signOut()
    }
    
    /// Restore Previous Login Session
    func googleRestorePreviousSession() {
        // Automatically sign in the user.
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
    }
}

//MARK:- Apple Manager
class AppleManager: NSObject {
    
    var viewController : UIViewController?
    var appleCallback : AppleCallback?
    static let sharedManager: AppleManager = { AppleManager() }()
    
    override init() {
        super.init()
    }
    
    func configureLoginManager(appleSignInBtnFrame:CGRect,radiusForButton:CGFloat,sender : UIViewController, success : @escaping AppleCallback){
        appleCallback = success
        self.viewController = sender
        
        let authorizationButton = ASAuthorizationAppleIDButton()
        authorizationButton.addTarget(self, action: #selector(handleAppleIdRequest), for: .touchUpInside)
        authorizationButton.layer.cornerRadius = radiusForButton
        authorizationButton.frame = appleSignInBtnFrame
        sender.view.addSubview(authorizationButton)
    }
    
    @objc func handleAppleIdRequest() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.performRequests()
    }
    
    @objc func checkAuthorization(appleIDCredential:ASAuthorizationAppleIDCredential){
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        appleIDProvider.getCredentialState(forUserID: appleIDCredential.user) {  (credentialState, error) in
            
            if let block = self.appleCallback {
                let appleProfile = Apple(result: appleIDCredential)
                block(appleProfile,credentialState)
            }
        }
    }
}

//MARK:- Instagram Manager
class InstagramManager:NSObject{
    
    var INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    var INSTAGRAM_CLIENT_ID:String?
    var INSTAGRAM_CLIENTSERCRET:String?
    var INSTAGRAM_REDIRECT_URI:String?
    var INSTAGRAM_ACCESS_TOKEN:String?
    var scopes: [InstagramScope] = [.basic]
    var viewController : UIViewController?
    private var webView: WKWebView!
    private var webViewObservation: NSKeyValueObservation?
    
    var instagramCallback : InstagramCallback?
    static let sharedManager: InstagramManager = { InstagramManager() }()
    
    enum InstagramScope: String {
        /// To read a user’s profile info and media.
        case basic
        /// To read any public profile info and media on a user’s behalf.
        case publicContent = "public_content"
        /// To read the list of followers and followed-by users.
        case followerList = "follower_list"
        /// To post and delete comments on a user’s behalf.
        case comments
        /// To follow and unfollow accounts on a user’s behalf.
        case relationships
        /// To like and unlike media on a user’s behalf.
        case likes
        /// To get all permissions.
        case all = "basic+public_content+follower_list+comments+relationships+likes"
    }
    
    override init() {
        super.init()
    }
    
    func configureLoginManager(clientSecret:String,clientId:String,redirectUri:String,sender : UIViewController, success : @escaping InstagramCallback){
        instagramCallback = success
        self.viewController = sender
        
        INSTAGRAM_CLIENT_ID = clientId
        INSTAGRAM_REDIRECT_URI = redirectUri
        INSTAGRAM_CLIENTSERCRET = clientSecret
        
        setupWebView()
        
        var components = URLComponents(string: "https://api.instagram.com/oauth/authorize/")!
        
        components.queryItems = [
            URLQueryItem(name: "client_id", value:INSTAGRAM_CLIENT_ID),
            URLQueryItem(name: "redirect_uri", value: INSTAGRAM_REDIRECT_URI),
            URLQueryItem(name: "response_type", value: "token"),
            URLQueryItem(name: "scope", value: scopes.first?.rawValue)   //.joined(separator: "+"))
        ]
        webView.load(URLRequest(url: components.url!, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData))
    }
    
    func setupWebView() {
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.websiteDataStore = .nonPersistent()
        
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.isOpaque = false
        webView.backgroundColor = UIColor(red: 0.97, green: 0.97, blue: 0.97, alpha: 1.0)
        webView.navigationDelegate = self
        self.viewController?.view.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.widthAnchor.constraint(equalTo: (self.viewController?.view.widthAnchor)!).isActive = true
        webView.heightAnchor.constraint(equalTo: (self.viewController?.view.heightAnchor)!).isActive = true
        webView.centerXAnchor.constraint(equalTo: (self.viewController?.view.centerXAnchor)!).isActive = true
        webView.centerYAnchor.constraint(equalTo: (self.viewController?.view.centerYAnchor)!).isActive = true
    }
    
    func checkRequestForCallbackURL(request: URLRequest){
        let requestURLString = (request.url?.absoluteString)! as String
        if requestURLString.hasPrefix(INSTAGRAM_REDIRECT_URI ?? "") {
            let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
            handleAuth(authToken: requestURLString.substring(from: range.upperBound))
        }
    }
    
    func handleAuth(authToken: String) {
        print("Instagram authentication token ==", authToken)
        if let block = self.instagramCallback {
            block(authToken)
        }
    }
}

//MARK:- Linkedin Manager
class LinkedinManager:NSObject{
    
    var viewController : UIViewController?
    var linkedinCallback : LinkedInCallback?
    private var webView: WKWebView!
    private var webViewObservation: NSKeyValueObservation?
    let linkedInAuthEndPoint  = "https://www.linkedin.com/uas/oauth2/authorization"
    let linkedinAccessTokenEndPoint = "https://www.linkedin.com/uas/oauth2/accessToken"
    var linkedInKey:String?
    var clientSecretKey:String?
    var linkedInRedirectUrlKey:String?
    
    
    static let sharedManager: LinkedinManager = { LinkedinManager() }()
    
    override init() {
        super.init()
    }
    
    func configureLoginManager(linkedinKey:String,clientSecret:String,linkedInRedirectUrl:String,sender: UIViewController, success : @escaping LinkedInCallback){
        linkedinCallback = success
        self.viewController = sender
        linkedInKey = linkedinKey
        clientSecretKey = clientSecret
        linkedInRedirectUrlKey = linkedInRedirectUrl
        
        setupWebView()
        startAuthorization(linkedInRedirectUrl: linkedInRedirectUrl, linkedinKey: linkedinKey)
    }
    
    func setupWebView() {
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.websiteDataStore = .nonPersistent()
        
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.isOpaque = false
        webView.backgroundColor = UIColor(red: 0.97, green: 0.97, blue: 0.97, alpha: 1.0)
        webView.navigationDelegate = self
        self.viewController?.view.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.widthAnchor.constraint(equalTo: (self.viewController?.view.widthAnchor)!).isActive = true
        webView.heightAnchor.constraint(equalTo: (self.viewController?.view.heightAnchor)!).isActive = true
        webView.centerXAnchor.constraint(equalTo: (self.viewController?.view.centerXAnchor)!).isActive = true
        webView.centerYAnchor.constraint(equalTo: (self.viewController?.view.centerYAnchor)!).isActive = true
    }
    
    func startAuthorization(linkedInRedirectUrl:String,linkedinKey:String) {
        // Specify the response type which should always be "code".
        let responseType = "code"
        
        // Set the redirect URL. Adding the percent escape characthers is necessary.
        let redirectURL = linkedInRedirectUrl.addingPercentEncoding(withAllowedCharacters: .alphanumerics) ?? ""
        
        // Create a random string based on the time interval (it will be in the form linkedin12345679).
        let state = "linkedin\(Int(Date().timeIntervalSince1970))"
        
        // Set preferred scope.
        let scope = "r_emailaddress,r_liteprofile"
        // Create the authorization URL string.
        var authorizationURL = "\(linkedInAuthEndPoint)?"
        authorizationURL += "response_type=\(responseType)&"
        authorizationURL += "client_id=\(linkedinKey)&"
        authorizationURL += "redirect_uri=\(redirectURL)&"
        authorizationURL += "state=\(state)&"
        authorizationURL += "scope=\(scope)"
        
        let url = URL(string: authorizationURL)!
        webView.load(URLRequest(url: url))
    }
   
    func requestForAccessTokens(authorizationCode: String,linkedInKey:String,linkedInSecret:String,linkedInRedirectUrl:String) {
        let grantType = "authorization_code"
        let redirectURL = linkedInRedirectUrl.addingPercentEncoding(withAllowedCharacters: .alphanumerics) ?? ""
        // Set the POST parameters.
        var postParams = "grant_type=\(grantType)&"
        postParams += "code=\(authorizationCode)&"
        postParams += "redirect_uri=\(redirectURL)&"
        postParams += "client_id=\(linkedInKey)&"
        postParams += "client_secret=\(linkedInSecret)"
        let postData = postParams.data(using: String.Encoding.utf8)
        var request = URLRequest(url: URL(string: linkedinAccessTokenEndPoint)!)
        request.httpMethod = "POST"
        
        // Set the HTTP body using the postData object created above.
        request.httpBody = postData
        request.addValue("application/x-www-form-urlencoded;", forHTTPHeaderField: "Content-Type")
        
        // Initialize a NSURLSession object.
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        // Make the request.
        let task: URLSessionDataTask = session.dataTask(with: request) { (data, response, error) -> Void in
            let statusCode = (response as! HTTPURLResponse).statusCode
            
            if statusCode == 200 {
                // Convert the received JSON data into a dictionary.
                do {
                    let dataDictionary = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String:Any]
                    
                    let accessToken = dataDictionary["access_token"] as! String
                    //                    self.delegate?.linkedInLogin(from: self, code: accessToken)
                    self.getEmailfromLinked(code: accessToken)
                    print(accessToken)
                }
                catch {
                    print("Could not convert JSON data into a dictionary.")
                    // self.showalertview(messagestring: "Could not convert JSON data into a dictionary.")
                }
            }else{
                // self.showalertview(messagestring: error?.localizedDescription ?? "")
            }
        }
        
        task.resume()
    }
    
    func getEmailfromLinked(code: String){
        
        var request = URLRequest(url: URL(string: "https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))" )!)
        request.httpMethod = "GET"
        request.httpBody = nil
        request.addValue("Authorization", forHTTPHeaderField: "Bearer \(code)")
        request.addValue("application/x-www-form-urlencoded;", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        let task: URLSessionDataTask = session.dataTask(with: request) { (data, response, error) -> Void in
            let statusCode = (response as! HTTPURLResponse).statusCode
            
            if statusCode == 200 {
                do {
                    let dataDictionary = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String:Any]
                    
                    if let block = self.linkedinCallback {
                        //   let email = dataDictionary["elements"].arrayValue.first?["handle~"]["emailAddress"].string
                        block("email parsing need to be done ,pending")
                    }
                }
                catch {
                    print("Could not convert JSON data into a dictionary.")
                }
            }else{
            }
        }
        task.resume()
    }
}

//MARK:- Delegates
extension GoogleManager:GIDSignInDelegate{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error == nil{
            if let block = self.googleCallback {
                let googleProfile = Google(result: user)
                block(googleProfile)
            }
        }
    }
}
extension InstagramManager: WKNavigationDelegate{
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    }
    
    public func webView(_ webView: WKWebView,
                        decidePolicyFor navigationAction: WKNavigationAction,
                        decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        if let url = navigationAction.request.url?.absoluteString, let _ = url.range(of: "#access_token=") {
            checkRequestForCallbackURL(request: navigationAction.request)
        }
        decisionHandler(.allow)
    }
    
    public func webView(_ webView: WKWebView,
                        decidePolicyFor navigationResponse: WKNavigationResponse,
                        decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        
        if let response = navigationResponse.response as? HTTPURLResponse, response.statusCode == 400 {
            print("Instagram Made In-valid request")
        }
        decisionHandler(.allow)
    }
}

extension AppleManager:ASAuthorizationControllerDelegate{
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
            checkAuthorization(appleIDCredential: appleIDCredential)
        }
        
        func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
            print("error")
        }
    }}



extension LinkedinManager: WKNavigationDelegate{
    
    
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        guard let url = webView.url else{
            return
        }
        //https://com.breezyapp.linkedin.oauth/oauth
        print(url.absoluteString)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard let url = webView.url else{
            decisionHandler(.cancel)
            return
        }
        print(url.absoluteString)
        if url.host == "com.breezyapp.linkedin.oauth" {
            //extract auth code from url
            let code = url.query?.components(separatedBy: "&").filter{$0.contains("code")}.first
            if let code = code{
                let authCode = code.components(separatedBy: "=")[1]
                self.requestForAccessTokens(authorizationCode: authCode, linkedInKey: self.linkedInKey ?? "", linkedInSecret: clientSecretKey ?? "", linkedInRedirectUrl: linkedInRedirectUrlKey ?? "")
            }
        }
        decisionHandler(.allow)
    }
}

//MARK:- Models
class Facebook: NSObject {
    
    var fbId : String?
    var firstName : String?
    var lastName : String?
    var imageUrl : String?
    var email : String?
    
    init(result : Any?) {
        super.init()
        guard let fbResult = result as? [String:Any] else { return }
        
        fbId = AccessToken.current?.userID
        firstName = fbResult["first_name"] as? String
        lastName = fbResult["last_name"] as? String
        imageUrl = ((fbResult["picture"] as? [String:Any])?["data"] as? [String:Any])?["url"] as? String
        email = fbResult["email"] as? String
        if email == nil {
            email = (firstName ?? "" + "@facebook.com")
        }
    }
    
    override init() {
        super.init()
    }
}

class Google: NSObject {
    
    var userId: String?
    var fullName : String?
    var givenName : String?
    var familyName : String?
    var email : String?
    
    init(result : Any?) {
        guard let googleResults = result as? GIDGoogleUser else { return }
        
        userId = googleResults.userID
        fullName = googleResults.profile.name
        givenName = googleResults.profile.givenName
        familyName = googleResults.profile.familyName
        email = googleResults.profile.email
        
    }
    override init() {
        super.init()
    }
}

class Apple : NSObject {
    
    var appleToken : String?
    var fullName : String?
    var email : String?
    var token : Data?
    
    init(result : Any?) {
        super.init()
        guard let appleResult = result as? ASAuthorizationAppleIDCredential else { return }
        
        appleToken = appleResult.user
        fullName = appleResult.fullName?.givenName ?? ""
        email = appleResult.email ?? ""
        token = appleResult.identityToken ?? Data()
    }
    
    override init() {
        super.init()
    }
}

