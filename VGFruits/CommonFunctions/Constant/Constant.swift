//  MEX
//
//  Created by Pankaj Sharma on 03/04/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit


enum Gender:String{
    case male = "Male"
    case female = "Female"
    
    var string:String{
           return NSLocalizedString(self.rawValue, comment: "")
       }
    
}

enum MaritalStatus:String{
    case married = "Married"
    case unMarried = "Unmarried"
    
    var string:String{
        return NSLocalizedString(self.rawValue, comment: "")
    }
}

enum JobType:String{
    case fullTime = "Full Time"
    case partTime = "Part Time"
    
    var string:String{
        return NSLocalizedString(self.rawValue, comment: "")
    }
}

enum PostType:String{
    case jobs = "Jobs"
    case events = "Events"
    case business = "Business"
    case ads = "Ads"
}



//Cell Identifiers
enum CellIdentifiers:String{
    case OnboardingCell = "OnboardingCell"
    case CategoryCell = "CategoryCell"
    case SideMenuCell = "SideMenuCell"
    case ReviewCell = "ReviewCell"
    case OrderCell = "OrderCell"
    case ShopCell = "ShopCell"
    case BannerCell = "BannerCell"
    case FilterCell = "FilterCell"
    case FilterCVCell = "FilterCVCell"
    case CouponCell = "CouponCell"
    case FeaturedCell = "FeaturedCell"
    case FeaturedItemCVCell = "FeaturedItemCVCell"
    case ItemsCell = "ItemCell"
    case RoundCategoryCell = "RoundCategoryCell"
    case BannerCollectionCell = "BannerCollectionCell"
    case CategoryCVCell = "CategoryCVCell"
    case ProductCell = "ProductCell"
    case NotificationCell = "NotificationCell"
    case EarningCell = "EarningCell"
    case InboxCell = "InboxCell"
    case JobPhotoCell = "JobPhotoCell"
    case TrackItemCell = "TrackItemCell"
    case PaymentMethodCell = "PaymentMethodCell"
    case AddressCell = "AddressCell"
    case SearchItemCell = "SearchItemCell"
}


enum BarItems:String{
    case home = "home"
    case bookmark = "bookmark"
    case paymentOptions = "paymentOptions"
    case offers = "offers"
    case history = "history"
    case setting = "setting"
    case help = "help"
    case privacyPolicy = "privacyPolicy"
    case aboutUs = "aboutUs"
    case logout = "logout"
    
    var string:String{
        return NSLocalizedString(self.rawValue, comment: "")
    }
}
