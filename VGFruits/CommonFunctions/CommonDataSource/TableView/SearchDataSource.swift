//
//  HomeTableDataSource.swift
//  VGFruits
//
//  Created by Pankaj Sharma on 11/06/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import Foundation
import UIKit


class SearchDataSource: TableViewDataSource  {
    

    override init (items : Array<Any>? , tableView : UITableView? , cellIdentifier : String? , configureCellBlock : ListCellConfigureBlock? , aRowSelectedListener :   DidSelectedRow?,willDisplayCell : WillDisplayCell? , viewforHeaderInSection : ViewForHeaderInSection?,scrollToTop:ScrollToTop?,scrollToBottom:ScrollToBottom? = nil) {
        
        super.init(items: items, tableView: tableView, cellIdentifier: cellIdentifier, configureCellBlock: configureCellBlock, aRowSelectedListener: aRowSelectedListener, willDisplayCell: willDisplayCell, viewforHeaderInSection: viewforHeaderInSection, scrollToTop: scrollToTop)
       
       
        
        tableView?.registerXIB(CellIdentifiers.SearchItemCell.rawValue)
       
        tableView?.registerXIB(CellIdentifiers.ShopCell.rawValue)
        
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var identifier = ""
        
        
        guard let data = self.items?[indexPath.row] as? SearchData else { fatalError() }
        
        if /data.type == 1{
            identifier = CellIdentifiers.ShopCell.rawValue
        }else if /data.type == 2{
            identifier = CellIdentifiers.SearchItemCell.rawValue
        }
      
        
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier , for: indexPath) as UITableViewCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
       
        if let block = self.configureCellBlock {
            block(cell , nil , indexPath as IndexPath?)
        }
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if let block = self.aRowSelectedListener,let item  = self.items?[indexPath.row]{
                      block(indexPath , item)
                  }
        
      
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  self.items?.count == 0 {
                        let objNoDataFound =  UINib(nibName: "EmptyList", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
                        self.tableView?.backgroundView = objNoDataFound
                    } else {
                        self.tableView?.backgroundView = nil
                    }
        return (self.items?.count ?? 0)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let block = viewforHeaderInSection else { return nil }
        return block(section)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let block = willDisplayCell, indexPath.row < /items?.count else { return }
        return block(indexPath, cell, items?[indexPath.row])
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerHeight ?? 0.0
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let block = viewforHeaderInSection else { return nil }
        return block(section)
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return footerHeight ?? 0.0
    }
    
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tableView{
            if scrollView.contentOffset.y == 0{
                scrollToTop?()
            }else{
                if scrollView.contentOffset.y + 100 >= (scrollView.contentSize.height - scrollView.frame.size.height){
                    if let block = scrollToBottom{
                        block()
                    }
                }
            }
        }
    }
    
}

